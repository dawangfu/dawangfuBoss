<?php

/**
 * 
 * @author zhouming
 *
 */
class Area_model extends MY_Model {

	public function __construct(){
		parent::__construct() ;
	}
	
	public function getAreaInfoId($id){
		$areaInfo = array();
		if($id > 0){
			$areaInfo = $this->db->select("a.`id`,a.`area_key`,a.`name`,a.`province_id`,a.`city_id`,a.`manager_id`,a.`update_flag` ,b.region_name as province_name,c.region_name as city_name,d.username")
							->from("dwf_area as a")
							->join('dwf_region as b','a.province_id=b.id','left')
							->join('dwf_region as c','a.city_id=c.id','left')
							->join('dwf_manager as d','a.manager_id=d.id','left')
							->where('a.id',$id)
							->get()
							->row_array();
		}
		return $areaInfo;
	}
	/**
	 * 数据库查询区域信息
	 * @param unknown $areaKey
	 */
	public function getAreaInfo($areaKey){
		$areaInfo = array();
		if($areaKey != ''){
			$areaInfo = $this->db->select("a.`id`,a.`area_key`,a.`name`,a.`province_id`,a.`city_id`,a.`manager_id`,a.`update_flag` ,b.region_name as province_name,c.region_name as city_name,d.username")
							->from("dwf_area as a")
							->join('dwf_region as b','a.province_id=b.id','left')
							->join('dwf_region as c','a.city_id=c.id','left')
							->join('dwf_manager as d','a.manager_id=d.id','left')
							->where('a.area_key',$areaKey)
							->get()
							->row_array();
		}
		return $areaInfo;
	}
	/**
	 * 获取区域列表
	 * @param unknown $where
	 * @param number $page
	 * @param number $limi
	 */
	public function getAreaList($where=array() , $page = 1 , $limi = 30){
		$areaList = array();
		$areaList = $this->db->select("a.`id`,a.`area_key`,a.`name`,a.`province_id`,a.`city_id`,a.`manager_id` ,b.region_name as province_name,c.region_name as city_name,d.username")
							->from("dwf_area as a")
							->join('dwf_region as b','a.province_id=b.id','left')
							->join('dwf_region as c','a.city_id=c.id','left')
							->join('dwf_manager as d','a.manager_id=d.id','left')
// 							->where()
// 							->limit()
							->get()
							->result_array();
		return $areaList;
	}
	/**
	 * 更新区域信息
	 * @param unknown $areaInfo
	 * @param number $provinceId
	 * @param number $cityId
	 */
	public function updateAreaInfo($areaInfo , $provinceId = 11 , $cityId = 110){
		$result = false;
		if(!empty($areaInfo)){
			$data = array('name'=>$areaInfo['name'],'province_id'=>$provinceId ,'city_id'=>$cityId,'update_flag'=>1);
			$this->db->where('area_key',$areaInfo['area_key'])->update('dwf_area',$data);
		}
		return $result;
	}
	/**
	 * 添加区域信息
	 * @param unknown $areaInfo
	 * @param number $provinceId
	 * @param number $cityId
	 */
	public function insertAreaInfo($areaInfo , $provinceId = 11 , $cityId = 110){
		$result = false;
		if(!empty($areaInfo)){
			$data = array('area_key'=>$areaInfo['area_key'],'name'=>$areaInfo['name'],'province_id'=>$provinceId ,'city_id'=>$cityId,'update_flag'=>1);
			print_r($data);
			$result = $this->db->insert('dwf_area',$data);
		}
		return $result;
	}
	/**
	 * 更新区域更新标识
	 * @param unknown $areaKey if $areaKey == all 更新所有数据
	 * @return boolean
	 */
	public function updateAreaFlag($areaKey , $flag){
		$result = false;
		$data = array('update_flag'=>$flag);
		if($areaKey == 'all'){
			$this->db->update('dwf_area',$data);
		}elseif($areaKey != ''){
			$this->db->where('area_key',$areaKey)->update('dwf_area',$data);
		}
		return $result;
	}
	/**
	 * 更新区域的分管经理
	 * @param unknown $areaId
	 * @param unknown $managerId
	 */
	public function setAreaManager($areaId , $managerId){
		$result = false;
		if($areaId != 0){
			$data = array('manager_id'=>$managerId);
			$result = $this->db->where('id',$areaId)->update('dwf_area',$data);
		}
		return $result;
	}
	/**
	 * 获取单个区域经理的分管区域
	 * @param unknown $managerId
	 */
	public function getAreaForManager($managerId){
		$areaList = $this->db->where('manager_id',$managerId)->get('dwf_area')->result_array();
		return $areaList;
	}
}
