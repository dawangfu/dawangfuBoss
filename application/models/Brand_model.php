<?php
/**
 *
* @author zhouming
*
*/
class Brand_model extends MY_Model {

	public function __construct() {
		parent::__construct ();
	}
	/**
	 * 查询品牌列表
	 * @param unknown $param
	 * @return unknown
	 */
	public function getBrandList($param){
		if(isset($param['where']) && !empty($param['where'])){$this->db->where($param['where']);}
		if(isset($param['where_in']) && !empty($param['where_in'])){
			foreach($param['where_in'] as $key=>$value){
				$this->db->where_in($key , $value);
			}
		}
		$brandList = $this->db->get('dwf_brand')
							->result_array();
		return $brandList;
	}
}