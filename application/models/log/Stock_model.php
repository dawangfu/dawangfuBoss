<?php
/**
 * Created by PhpStorm.
 * User: fish
 * Date: 2017/7/21
 * Time: 上午10:10
 */
class Stock_model extends MY_Model{

    public function __construct(){
        parent::__construct();
    }

    /**
     * @param $pid  产品ID
     * @param $gid  规格ID
     * @param $type 操作类型 0:加库存，1:减库存
     * @return mixed
     */
    public function insertLog($gid, $num, $type, $operate, $price){
        $data = array(
            'goods_id' => $gid,
            'goods_price' => $price,
            'opera_num' => $num,
            'opera_type' => $type,
            'opera_mode' => $operate,
            'opera_time' => date('Y-m-d H:i:s')
        );
        $result = $this->db->insert('dwf_log_stock', $data);
        return $result;
    }
}