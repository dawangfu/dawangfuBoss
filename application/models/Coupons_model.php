<?php
/**
 *
 * @author zhouming
 *
 */
class Coupons_model extends MY_Model{
    public function __construct() {
        parent::__construct ();
    }
   
	/**
	 * 获取优惠券信息
	 * @param type $couponId
	 * @return type
	 */
    public function getCoupon($couponId){
    	
    }
    /**
     * 优惠券列表
     * @param unknown $param
     * @return unknown[]
     */
	public function getCouponList($param){
		$coupon_list = array();
	
		if(isset($param['where']) && !empty($param['where'])){$this->db->where($param['where']);}
		if(isset($param['where_in']) && !empty($param['where_in'])){
			foreach($param['where_in'] as $key=>$value){
				if(!empty($value)){
					$this->db->where_in($key , $value);
				}
			}
		}
		$totalNum =  $this->db->count_all_results('`dwf_coupon`',FALSE);
		//echo $this->db->last_query();
		if(isset($param['fields'])){
			$fields = $param['fields'];
		}else{
			$fields = '*';
		}
		$this->db->select($fields);
		if(isset($param['limit']) && !empty($param['limit'])){
			if(is_array($param['limit'])){
				$this->db->limit($param['limit'][0] , $param['limit'][1]);
			}else{
				$this->db->limit($param['limit']);
			}
		}
		if(isset($param['order_by']) && $param['order_by'] != '')$this->db->order_by($param['order_by']);
		$coupons = $this->db->get()->result_array();
// 		echo $this->db->last_query();
// 		$coupons = $this->getProductExtendData($coupons);
		//echo $this->db->last_query();
		$coupon_list = array('totalNum'=>$totalNum , 'coupons'=>$coupons);
		return $coupon_list;
	}
    /**
     * 发放优惠券
     * @param unknown $couponsInfo
     * @return boolean
     */
    public function addCoupons($couponsInfo){
        $result = 0;
        if(!empty($couponsInfo)){
            try {
                $this->db->insert('`dwf_coupon`',$couponsInfo);
                $result = $this->db->insert_id();
            } catch (Exception $e) {
            }
        }
        return $result;
    }
    /**
     * 添加优惠券关联产品
     * @param unknown $couponProductData
     */
    public function addCouponProduct($couponProductData){
    	$result = false;
    	if(!empty($couponProductData)){
    		try {
    			$result = $this->db->insert_batch('`dwf_coupon_product`',$couponProductData);
    		} catch (Exception $e) {
    		}
    	}
    	return $result;
    }
    
    public function addCouponCategory($couponCategoryData){
    	$result = false;
    	if(!empty($couponCategoryData)){
    		try {
    			$result = $this->db->insert_batch('`dwf_coupon_category`',$couponCategoryData);
    		} catch (Exception $e) {
    		}
    	}
    	return $result;
    }
    
} 