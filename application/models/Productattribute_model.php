<?php

/**
 * Created by PhpStorm.
 * User: fish
 * Date: 16/10/27
 * Time: 下午6:14
 */
class ProductAttribute_model extends MY_Model{

    private static $table = '`dwf_product_attribute`';

    protected static $perPage = 5;

    public function __construct(){
        parent::__construct();
    }

    /**
     *  商品属性列表
     */
    public function getProductAttribute($param){
    	if(isset($param['where']) && !empty($param['where'])){$this->db->where($param['where']);}
    	if(isset($param['where_in']) && !empty($param['where_in'])){
    		foreach($param['where_in'] as $key=>$value){
    			if(!empty($value)){
    				$this->db->where_in($key , $value);
    			}
    		}
    	}
		$attribute = $this->db->get('dwf_product_attribute')->result_array();
		$attribute_ids = array();
		$attribute_new = array();
		foreach($attribute as $key=>$value){
			$attribute_ids[] = $value['id'];
			$value['values'] = array();
			$attribute_new[$value['id']] = $value;
            $attribute_new['ids'][] = $value['id'];
		}
		if(!empty($attribute_ids)){
			$param1['where_in'] = array('attribute_id'=>$attribute_ids);
			$attribute_value = $this->getProductAttributeValue($param1);
			if(!empty($attribute_value)){
				foreach($attribute_value as $value){
					$attribute_new[$value['attribute_id']]['values'][] = $value;
				}
			}
		}
		
// 		echo $this->db->last_query();exit;
		return $attribute_new;
    }
    
    public function getProductAttributeValue($param){
    	$attributeValue = array();
    	if(isset($param['where']) && !empty($param['where'])){$this->db->where($param['where']);}
    	if(isset($param['where_in']) && !empty($param['where_in'])){
    		foreach($param['where_in'] as $key=>$value){
    			$this->db->where_in($key , $value);
    		}
    	}
    	$attributeValue = $this->db->get('dwf_product_attribute_value')->result_array();
//     	print_r($attributeValue);exit;
//     			echo $this->db->last_query();exit;
    	return $attributeValue;
    }

    /**
     *  商品属性详情
     */
    public function info($attributeId){
        $category_info = $this -> db -> select("`id`,`attribute_name`,`category_id`,`attribute_type`,`form_type`,`f_order`")
            ->from($this::$table)
            ->where('id',$attributeId)
            ->get()
            ->row_array();
        return $category_info;
    }

    /**
     *  添加商品属性
     */
    public function productAttributeAdd($options, $id){
        $options['create_time'] = date('Y-m-d H:i:s');
        if($id == 0){
            $result['result'] = $this -> db -> insert($this::$table, $options);
            $result['id'] = $this -> db -> insert_id();
        }else{
            $result['id'] = $id;
            $this -> db -> where('id', $id);
            $result['result'] = $this -> db -> update($this::$table, $options);
        }
        return $result;
    }

    /**
     *  添加商品属性值
     */
    public function productAttributeValueAdd($options){
        $result = $this -> db -> insert_batch('`dwf_product_attribute_value`', $options);
        return $result;
    }

    /**
     *  获取属性值
     */
    public function attributeValueGet($aid, $cid){
        $result = $this -> db -> select("`id`,`attribute_id`,`category_id`,`value_name`")
                              -> from('`dwf_product_attribute_value`')
                              -> where('attribute_id', $aid)
                              -> where('category_id', $cid)
                              -> get()
                              -> result_array();
        return $result;
    }

    /**
     * 更新属性值
     */
    public function attributeValueUpdate($options){
        if(!empty($options)){
            foreach($options as $key => $value){
                $this -> db -> where('id', $key);
                $this -> db -> update('dwf_product_attribute_value',array('value_name' => $value));
            }
        }
    }
    /**
     *  删除属性值
     */
    public function attributeValueDel($id){
        $this -> db -> where('id',$id);
        $result = $this -> db -> delete('`dwf_product_attribute_value`');
        return $result;
    }

    /**
     *  获取所有属性名
     */
    public function allAttributeGet($page){
        $result = $this -> db -> select("a.`id`,a.`category_id`,a.`attribute_name`,a.`attribute_type`,a.`form_type`,c.category_name")
            -> from($this::$table.' as a')
            -> join('dwf_category as c', 'c.id = a.category_id', 'left')
            -> limit(self::$perPage, $page * self::$perPage)
            -> get()
            -> result_array();
        return $result;
    }

    /**
     *  获取所有属性的
     */
    public function allAttributeCount(){
        $total = $this -> db -> count_all($this::$table);
        $pages = ceil($total / self::$perPage);
        return $pages;
    }

    /**
     *  删除指定的商品属性
     */
    public function productAttributeDel($id){
        $attributeValueResult = $this -> productAttributeValueDel($id);
        if($attributeValueResult > 0){
            $this -> db -> where('id', $id);
            $result = $this -> db -> delete($this::$table);
        }else{
            $result = false;
        }
        return $result;
    }

    /**
     *  根据商品属性category_id删除属性值
     */
    public function productAttributeValueDel($attribute_id){
        $table = '`dwf_product_attribute_value`';
        $this -> db -> where('attribute_id', $attribute_id);
        $total = $this -> db -> count_all_results($table, false);
        $result = 2;//未找到数据
        if($total > 0){
            $result = $this -> db -> delete($table);
        }
        return $result;
    }
}