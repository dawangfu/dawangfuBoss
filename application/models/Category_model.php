<?php

class Category_model extends MY_Model {

    public function __construct(){
        parent::__construct();
    }

    /**
     *  查询商品分类列表
     */
    public function getCategoryList($type){
        $category_list = array();
        if($type > 0){
            $this->db->where('category_type', $type);
        }
        $category_all = $this->db->select('`id`,`category_name`,`category_type`,`parent_id`,`index_show`,`title`,`keywords`,`description`,`category_icon`,`f_order`')
        						->from('dwf_category')
        						->get()
        						->result_array();
        //echo $this->db->last_query();
		$category_list = PHPTree::makeTree($category_all);	
        return $category_list;
    }

    /**
     *  删除分类
     */
    public function delCategoryItem($id){
        //判断有无子分类
        $this -> db -> where('parent_id',$id);
        $children_num = $this -> db -> count_all_results('`dwf_category`', FALSE);

        if($children_num > 0){
            $result['result'] = $children_num;
            $result['msg'] = 'error';
        }else{
            $this->db->reset_query();
            $this -> db -> where('id',$id);
            $result['result'] = $this -> db -> delete('`dwf_category`');
            $result['msg'] = 'success';
        }
        return $result;
    }

    /**
     *  首页显示分类
     */
    public function showCategoryItem($options, $id){
        $result = $this -> db -> where('id', $id);
        $result = $this -> db -> update('`dwf_category`', $options);
        return $result;
    }

    /**
     *  新增&编辑分类
     *  $id大于0,更新
     */
    public function addCategoryItem($options, $id = 0, $pids = ''){
        $options['create_time'] = date('Y-m-d H:i:s');
        if($id == 0){
            $result = $this -> db -> insert('dwf_category', $options);
            $id = $this -> db -> insert_id();
        }else{
            $this -> db -> where('id', $id);
            $result = $this -> db -> update('`dwf_category`', $options);
            //$result = $this -> db -> update('`dwf_category`', $options) -> where(array('id' => $id));
        }
        if($pids != ''){
           $this -> add_category_to_product($pids, $id);
        }
        return $result;
    }


    /**
     *  分类详情
     */
    public function category_item_info($category_id){
        $category_info = array();
        if($category_id > 0){
            $category_info = $this -> db -> select("`id`,`category_name`,`category_type`,`category_icon`,`parent_id`,`index_show`,`title`,`keywords`,`description`,`f_order`")
                ->from("dwf_category")
                ->where('id',$category_id)
                ->get()
                ->row_array();
            if($category_info['parent_id'] != '0'){
                $category_parent = $this -> db -> select("`category_name`")
                    ->from("dwf_category")
                    ->where('id', $category_info['parent_id'])
                    ->where('parent_id', 0)
                    ->get()
                    ->row_array();
                if(!empty($category_parent)){
                    $category_info['parent_category_name'] = $category_parent['category_name'];
                }else{
                    $category_info['parent_category_name'] = '';
                }
            }else{
                $category_info['parent_category_name'] = '';
            }
        }
        return $category_info;
    }

    /**
     *  产品列表
     *  @ cid array
     */
    public function product_list_form_category($cid){
        $productList = [];
        $pids = $this -> get_product_id($cid);
        if(!empty($pids)){
            $productList = $this -> db -> select('`id`,`product_name`')
                -> from('dwf_product')
                -> where_in('id', $pids)
                -> order_by('id', 'DESC')
                -> get()
                -> result_array();
        }
        return $productList;
    }

    /**
     *  所属分类下产品ID
     */
    public function get_product_id($category_id){
        $pids = [];
        $pid = $this -> db -> select('`id`,`category_id`,`product_id`')
            -> from('dwf_product_category')
            -> where_in('category_id', $category_id)
            -> order_by('category_id', 'ASC')
            -> order_by('id', 'DESC')
            -> get()
            -> result_array();
        if(count($pid) >0){
            for($i = 0; $i < count($pid); $i++){
                $pids[$i] = $pid[$i]['product_id'];
            }
        }
        return $pids;
    }

    /**
     *  新增 分类与产品对应关系
     */
    public function add_category_to_product($pids, $category_id){
        if($pids == '' || $category_id == '')
            return false;
        $this -> del_category_to_product($category_id);
        $pids = explode(',', $pids);
        $options = [];
        for($i = 0; $i < count($pids); $i++){
            $options[$i]['product_id'] = $pids[$i];
            $options[$i]['category_id'] = $category_id;
        }
        $this -> db -> insert_batch('`dwf_product_category`', $options);
    }

    /**
     *  删除 分类与产品对应关系
     */
    public function del_category_to_product($category_id){
        $this -> db -> where('category_id',$category_id);
        $result['result'] = $this -> db -> delete('`dwf_product_category`');
    }
    
    public function getCategoryInfo($cagegoryId){
    	$categoryInfo = array();
    	if(is_array($cagegoryId)){
    		$this->db->where_in('id',$cagegoryId);
    	}else{
    		$this->db->where('id',$cagegoryId);
    	}
    	$categoryInfo = $this->db->select("id,category_type,parent_id")
    							->from("dwf_category")
    							->get()
    							->result_array();
    	return $categoryInfo;
    }

}