<?php

/**
 * 
 * @author zhouming
 *
 */
class User_model extends MY_Model {

	public function __construct(){
		parent::__construct() ;
        //$this -> need_login = true;
	}

	/**
	 * id查询用户信息
	 * @param unknown $userId
	 * @return Ambigous <boolean, string, multitype:>
	 */
	public function getUserInfoForId($userId){
		$userInfo = array() ;
		$sql = "SELECT *  FROM `dwf_user_basic` WHERE `id`=?" ;
		$binds = array('id'=>$userId) ;
		try{
			$userInfo = $this->queryRow($sql , $binds) ;
		}catch(Exception $e){
			
		}
		return $userInfo ;
	}


	/**
	 * 获取微信昵称
	 * @param type $openId
	 * @return type
	 */
	public function getWeChatUserInfo($openId){ 
		$user_info = array() ;
		$this->load->model('wechat_model') ;
		$access_token = $this->wechat_model->getAccessToken() ;
		$url = 'https://api.weixin.qq.com/cgi-bin/user/info' ;
		$param = array(
			'access_token'=>$access_token ,
			'openid'=>$openId ,
			'lang'=>'zh_CN'
		) ;
		$data = $this->wechat_model->httpRequest($url , $param) ;
		$user_info = json_decode($data , true) ;
		return $user_info ;
	}

	/**
	 * 更新账户余额
	 * @param unknown $userId
	 * @param unknown $opreation
	 * @param unknown $amount
	 * @return unknown
	 */
	public function updateUserCountBalance($userId , $opreation , $amount){
		$result = FALSE ;
		$result = $this->db->set('`count_balance`' , '`count_balance`' . $opreation . $amount , FALSE)->where(array('id'=>$userId))->update('`dwf_user_basic`') ;
		if($result){
			$this->sendCountChangeMessage($userId,$amount);
		}
		return $result ;
	}
	/**
	 * 发送账户余额变动消息(微信)
	 * @param unknown $userId
	 */
	private function sendCountChangeMessage($userId,$amount){
		$result = false;
		if($amount > 0){
			$userInfo = $this->getUserInfoForId($userId);
			if($userInfo['wx_openid']){
				$remark = '详情点这里!';
				$amount = number_format($amount , 2);
				$this->config->load('wechat_config', TRUE);
				$config = $this->config->item('wechat_config');
				$config = $config['mobile'];
				$this->load->model('wechat_model');
				$this->wechat_model->init($config);
				$this->wechat_model->sendCountChangeMessage($userInfo['wx_openid'], $amount, $userInfo['count_balance'], $remark);
			}
		}
		return $result;
	}
	
	/**
	 * 添加会员帐户变更纪录
	 * @param type $userId
	 * @param type $amount
	 * @param type $opreation
	 * @param type $offline_userid
	 * @param type $order_num
	 * @param type $remark
	 * @return type
	 */
	public function addUserCountRecord($userId , $amount , $opreation , $offline_userid = 0 , $order_num = '' , $remark = ''){
		$result = FALSE ;
		$data = array(
				'user_id'=>$userId ,
				'amount'=>$amount ,
				'operate'=>$opreation ,
				'order_num'=>$order_num ,
				'remark'=>$remark ,
				'offline_userid'=>$offline_userid ,
				'active_time'=>date('Y-m-d H:i:s')
		) ;
		$result = $this->db->insert('dwf_count_record' , $data) ;
		return $result ;
	}
	/**
	 * 获取用户的三级推荐人
	 * @param number $userId
	 * @param number $shopId
	 */
	public function getUserRecommend($userId = 0 , $shopId = 0 , $reload = 0){
		static $recommend_array = array();
		static $i = 0;
		if($reload == 1 && isset($recommend_array)){
		    $recommend_array = array();
		    $i = 0;
		}
		if(($userId == 0 && $shopId == 0) || $i > 2){
			return $recommend_array;
		}
		if($userId > 0){
			$userInfo = $this->getUserInfoForId($userId);
			if($userInfo['recommend_id'] > 0){
				$recommend_array[$i] = array('recommend_id'=>$userInfo['recommend_id'] , 'type'=>'user' , 'level'=>$i+1);
			}elseif($userInfo['recommend_shop_id'] > 0){
				$recommend_array[$i] = array('recommend_id'=>$userInfo['recommend_shop_id'] , 'type'=>'shop' , 'level'=>$i+1);
			}
			$i++;
			$this->getUserRecommend($userInfo['recommend_id'] , $userInfo['recommend_shop_id']);
		}elseif($shopId > 0){
			$this->load->model('shop_model');
			$shopInfo = $this->shop_model->getShopInfoForId($shopId);
			if($shopInfo['recommend_user_id'] > 0){
				$recommend_array[$i] = array('recommend_id'=>$shopInfo['recommend_user_id'] , 'type'=>'user' , 'level'=>$i+1);
			}elseif($userInfo['recommend_shop_id'] > 0){
				$recommend_array[$i] = array('recommend_id'=>$shopInfo['recommend_shop_id'] , 'type'=>'shop' , 'level'=>$i+1);
			}
			$i++;
			$this->getUserRecommend($shopInfo['recommend_user_id'] , $shopInfo['recommend_shop_id']);
		}
		return $recommend_array;
	}

    /**
     * boss后台登录
     * @param $username
     * @param $password
     * @return mixed
     */
	public function signIn($username, $password){
        $result = $this -> db -> select('`id`,`realname`')
                    -> from('`username`')
                    -> where(
                        array(
                            'username' => $username,
                            'psw' => md5($password),
                        )
                    )
                    -> get()
                    -> result_array();
        return $result;
    }
}
