<?php

/**
 *
 * @author fish
 *
 */
class Seckill_model extends MY_Model{

    public function __construct(){
        parent::__construct();
    }

    /**
     * 设置单品的秒杀状态
     * @param $gid
     * @param $seckill
     * @return mixed
     */
    public function setSkuSeckill($gid, $seckill){
        $this->db->set('seckill', $seckill);
        $this->db->where('id', $gid);
        $result = $this->db->update('dwf_product_goods');
        return $result;
    }

    /**
     * 设置商品的秒杀状态
     * @param $pid
     * @param $seckill
     * @return mixed
     */
    public function setProductSeckill($pid, $seckill){
        $this->db->set('seckill', $seckill);
        $this->db->where('id', $pid);
        $result = $this->db->update('dwf_product');
        return $result;
    }

    /**
     * 获取商品所属单品中秒杀状态的数量
     * @param $pid
     * @return mixed
     */
    public function getSkuSeckillNum($pid){
        $seckillNum = $this->db->select('count(id) as num')
                               ->from('dwf_product_goods')
                               ->where(
                                    array(
                                        'product_id' => $pid,
                                        'seckill' => 1
                                    )
                               )
                               ->get()
                               ->result_array();
        return $seckillNum[0];
    }

    /**
     * 获取单品的秒杀记录
     * @param $pid
     * @param $gid
     * @return mixed
     */
    public function getSkuSeckillRecord($pid, $gid){
        $result = array();
        $this->db->where(
            array(
                'product_id' => $pid,
                'goods_id' => $gid
            )
        );
        //$result = $this->db->count_all_results('dwf_seckill');
        $result = $this->db->select('status')
                           ->from('dwf_seckill')
                           ->get()
                           ->result_array();
        return $result;
    }

    /**
     * 新增秒杀记录
     * @param $data
     * @return mixed
     */
    public function insertSeckillRecord($data){
        $result = $this->db->insert('dwf_seckill', $data);
        return $result;
    }

    /**
     * 更新秒杀记录
     * @param $data
     * @return mixed
     */
    public function updateSeckillRecord($pid, $gid, $data){
        $this->db->where(
            array(
                'product_id' => $pid,
                'goods_id' => $gid
            )
        );
        $result = $this->db->update('dwf_seckill', $data);
        return $result;
    }

    /**
     * 更新秒杀价格
     * @param $data
     * @return mixed
     */
    public function updateSeckillPrice($pid, $gid, $price){
        $seckillExist = $this->getSkuSeckillRecord($pid, $gid);
        if(empty($seckillExist)){
            $data = array(
                'product_id' => $pid,
                'goods_id' => $gid,
                'sell_price' => $price,
                'pre_seckill' => '1'
            );
            $result = $this->insertSeckillRecord($data);
        }else{
            $this->db->where(
                array(
                    'product_id' => $pid,
                    'goods_id' => $gid
                )
            );
            if($seckillExist[0]['status'] == '0') {
                $this->db->set('pre_seckill', '1');
            }
            $this->db->set('sell_price', $price);
            $result = $this->db->update('dwf_seckill');
        }
        return $result;
    }

    /**
     * 更新秒杀库存
     * @param $data
     * @return mixed
     */
    public function updateSeckillStore($pid, $gid, $store, $storeReal){
        $seckillExist = $this->getSkuSeckillRecord($pid, $gid);
        if(empty($seckillExist)){
            $data = array(
                'product_id' => $pid,
                'goods_id' => $gid,
                'store_nums' => $store,
                'store_nums_real' => $storeReal,
                'pre_seckill' => '1'
            );
            $result = $this->insertSeckillRecord($data);
        }else{
            $this->db->where(
                array(
                    'product_id' => $pid,
                    'goods_id' => $gid
                )
            );
            if($seckillExist[0]['status'] == '0') {
                $this->db->set('pre_seckill', '1');
            }
            $this->db->set('store_nums', $store);
            $this->db->set('store_nums_real', $storeReal);
            $result = $this->db->update('dwf_seckill');
        }
        return $result;
    }

    public function getSeckillData($pid){
        $seckillData = [];
        $seckillData = $this->db->select('product_id, goods_id, store_nums, store_nums_real, sell_price')
                                ->from('dwf_seckill')
                                ->where(
                                    array(
                                        'product_id' => $pid,
                                        'type' => 0//,
                                        //'status' => 1
                                    )
                                )
                                ->get()
                                ->result_array();
        return $seckillData;
    }

    /**
     * 获取预上线秒杀商品列表
     * @return array
     */
    public function getPreSeckillProducts(){
        $preSeckillProducts = [];
        $preSeckillProducts = $this->db->select('id, product_id, goods_id, store_nums, store_nums_real, sell_price')
                                       ->from('dwf_seckill')
                                       ->where(
                                           array(
                                               'pre_seckill' => '1'
                                           )
                                       )
                                       ->get()
                                       ->result_array();
        return $preSeckillProducts;
    }


    /**
     * 获取处于所有秒杀状态下的商品记录
     * @return
     */
    public function getSeckillProducts(){
        $allSeckillList = $this->db->select('product_id, goods_id, store_nums, store_nums_real')
                                   ->from('dwf_seckill')
                                   ->where(
                                       array(
                                           'type' => '0',
                                           'status' => '1'
                                       )
                                   )
                                   ->get()
                                   ->result_array();
        return $allSeckillList;
    }
}