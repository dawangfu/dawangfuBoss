<?php

/**
 * 
 * @author zhouming
 *
 */
class Member_model extends MY_Model {

	public function __construct(){
		parent::__construct() ;
	}
	/**
	 * 获取充值列表
	 * @param unknown $param
	 */
	public function getRechargeList($page , $limit = 20){
	    $recharge_list = array();
	    $where = array('state'=>1);
	    $this->db->where($where);
	    $totalNum = $this->db->count_all_results('`dwf_recharge_order`',FALSE);
	    $this->db->select('user_id,order_num,order_num_online,amount,pay_time');
	    if($page <= 0){
	        $page = 1;
	    }
	    $start = ($page-1) * $limit;
	    $this->db->limit($limit,$start);
	    $this->db->order_by('id DESC');
	    $list = $this->db->get()->result_array();
	    $recharge_list = array('totalNum'=>$totalNum , 'list'=>$list);
	    return $recharge_list;
	}
	/**
	 * 获取会员一定时间内新推荐用户的一级推荐提成(默认时间是本周加上前三周)
	 * @param unknown $memberId
	 * @param unknown $startTime
	 * @param unknown $endTime
	 */
	public function getRecommenderFeeForLastThreeWeek($memberId , $startTime = '' , $endTime = ''){
	    $returnData = array();
	    $endTime == '' && $endTime = date('Y-m-d H:i:s');
	    $startTime == '' && $startTime = date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),date("d")-date("w")+1-28,date("Y")));
	    if(is_numeric($memberId) && $memberId > 0){
	        $returnData = $this->db->select('a.`amount` ,a.`active_time` ')
	                               ->from('`dwf_count_record` AS a ')
	                               ->join('`dwf_order` AS b','a.order_num = b.id','right')
	                               ->join('`dwf_user_basic` AS c', 'c.id = b.user_id' ,'right')
	                               ->where(array(
	                                               'a.`operate`'=>'+',
	                                               'a.`order_num` !='=>'',
                                                   'a.user_id'=>$memberId,
	                                               'c.`add_time` >='=>$startTime
	                                       )
	                                   )
                                    ->where('a.order_num IS NOT NULL')
	                                ->get()->result_array();
	    }else if(is_array($memberId) && !empty($memberId)){
	        $returnData = $this->db->select('a.`amount` ,a.`active_time`,a.`user_id` ')
                        	        ->from('`dwf_count_record` AS a ')
                        	        ->join('`dwf_order` AS b','a.order_num = b.id','right')
                        	        ->join('`dwf_user_basic` AS c', 'c.id = b.user_id' ,'right')
                        	        ->where(array(
                        	            'a.`operate`'=>'+',
                        	            'a.`order_num` !='=>'',
//                         	            'a.user_id'=>$memberId,
                        	            'c.`add_time` >='=>$startTime
                        	        )
                        	            )
                                    ->where('a.order_num IS NOT NULL')
                                    ->where_in('a.user_id',$memberId)
                                    ->get()->result_array();
	    }
// 	    echo $this->db->last_query();exit;
	    return $returnData;
	}
    
}
