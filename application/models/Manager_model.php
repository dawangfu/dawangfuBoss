<?php

/**
 * 
 * @author zhouming
 *
 */
class Manager_model extends MY_Model {

	public function __construct(){
		parent::__construct() ;
	}
	/**
	 * 区域经理列表
	 */
	public function getManagerList(){
		$manager_list = $this->db->select("*")
						->from("dwf_manager")
// 						->where()
// 						->limt()
						->get()
						->result_array();
		return $manager_list;
	}
	/**
	 * 获取区域经理详情
	 * @param unknown $id
	 * @return unknown
	 */
	public function getManagerDetail($id){
		$manager_detail = $this->db->select("*")
							->from("dwf_manager")
							->where("id",$id)
							->get()
							->row_array();
		return $manager_detail;
	}
	/**
	 * 手机号码，用户名查找区域经理
	 * @param unknown $keyword
	 * @return Ambigous <multitype:, boolean, string>
	 */
	public function getUserInfoFromKeyword($keyword){
		$userInfo = array() ;
		if(Tool::checkPhone($keyword)){
			$userInfo = $this->getUserInfoFromPhone($keyword) ;
		}else{
			$userInfo = $this->getUserInfoFromUserName($keyword) ;
		}
		return $userInfo ;
	}
	
	public function getUserInfoFromPhone($phone){
		$sql = "SELECT `id`,`username`,`password`,`tel` FROM `dwf_manager` WHERE `tel` = ? LIMIT 1" ;
		$binds = array('tel'=>$phone) ;
		$userInfo = $this->queryRow($sql , $binds) ;
		return $userInfo ;
	}
	
	public function getUserInfoFromUserName($userName){
		$sql = "SELECT `id`,`username`,`password`,`tel` FROM `dwf_manager` WHERE `username` = ? LIMIT 1" ;
		$binds = array('user_name'=>$userName) ;
		$userInfo = $this->queryRow($sql , $binds) ;
		return $userInfo ;
	}
	/**
	 * 更新区域经理数据
	 * @param unknown $id
	 * @param unknown $data
	 * @return boolean
	 */
	public function updateManagerInfo($id , $data){
	    $result = false;
	    if($id > 0 && !empty($data)){
	        $result = $this->db->where(array('id'=>$id))->update('dwf_manager', $data);
	    }
	    return $result;
	}
	/**
	 * 更新账户余额
	 * @param unknown $managerId
	 * @param unknown $opreation
	 * @param unknown $amount
	 * @return unknown
	 */
	public function updateManagerCountBalance($managerId , $opreation , $amount){
	    $result = FALSE ;
	    $result = $this->db->set('`count_balance`' , '`count_balance`' . $opreation . $amount , FALSE)->where(array('id'=>$managerId))->update('`dwf_manager`') ;
	    return $result ;
	}
	/**
	 * 添加区域经理帐户变更纪录
	 * @param type $managerId
	 * @param type $amount
	 * @param type $opreation
	 * @param type $managerId
	 * @param type $remark
	 * @return type
	 */
	public function addManagerCountRecord($managerId , $amount , $opreation, $orderId  , $remark = ''){
	    $result = FALSE ;
	    $data = array(
	        'manager_id'=>$managerId ,
	        'amount'=>$amount ,
	        'operate'=>$opreation ,
	        'order_id'=>$orderId ,
	        'remark'=>$remark ,
	        'add_time'=>date('Y-m-d H:i:s')
	    ) ;
	    $result = $this->db->insert('dwf_manager_count_recoder' , $data) ;
	    return $result ;
	}
	/**
	 * 获取区域经理提成
	 * @param number $managerId
	 * @param string $startTime
	 * @param string $endTime
	 */
	public function getManagerFee($managerId = 0 , $startTime = '' , $endTime = ''){
	    $this->db->select('*')
	           ->from('dwf_manager_count_recoder');
	    $this->db->where(
	               array(
	                   'operate'=>'+',
	                   'remark'=>'配送提成'
	               )
	        );
	    if($managerId > 0){
	        $this->db->where('manager_id',$managerId);
	    }
	    if($startTime != ''){
	        $this->db->where('add_time >=',$startTime);
	    }
	    if($endTime != ''){
	        $this->db->where('add_time <=',$endTime);
	    }
	    $this->db->order_by('id DESC');
	    $fee = $this->db->get()->result_array();
// 	    echo $this->db->last_query();exit;
	    return $fee;
	}
}
