<?php
/**
 *
 * @author zhouming
 *
 */
class Pay_model extends MY_Model {
    public function __construct() {
        parent::__construct ();
    }
    public function init(){}
    /**
     * 添加交账记录
     * @param unknown $manager_id
     * @param unknown $ammount
     * @param unknown $order_list
     * @return boolean
     */
    public function addAccountForCommpanyRecord($manager_id,$amount,$order_list){
        $result = false;
        $data = array(
            'manager_id'=>$manager_id,
            'amount'=>$amount,
            'account_time'=>date('Y-m-d H:i:s'),
            'order_aggregate'=>json_encode($order_list),
        );
        $result = $this->db->insert('dwf_manager_account_for_commpany_recorder',$data);
        return $result;
    }
    
    public function payForUserAliPay(){
    	
    }
    /**
     * 退还用户微信支付的金额
     * @param unknown $order_id
     * @param unknown $refund_fee
     * @param unknown $total_fee
     * @return boolean
     */
    public function returnForUserWechat($order_id,$refund_fee,$total_fee){
    	$result = false;
    	$this->load->model('order_model');
    	$orderOnlinePayNoInfo = $this->order_model->getOrderOnlinePayNo($order_id, 1);
    	if(!empty($orderOnlinePayNoInfo) && $orderOnlinePayNoInfo['online_pay_no'] != ''){
    	    $out_trade_no = $orderOnlinePayNoInfo['online_pay_no'];
    	    $input = new WxPayRefund();
    	    $input->SetOut_trade_no($out_trade_no);
    	    $total_fee100 = $total_fee * 100;
    	    $refund_fee100 = $refund_fee * 100;
    	    $input->SetTotal_fee($total_fee100);
    	    $input->SetRefund_fee($refund_fee100);
    	    $input->SetOut_refund_no(WxPayConfig::MCHID.date("YmdHis"));
    	    $input->SetOp_user_id(WxPayConfig::MCHID);
    	    $returnData = WxPayApi::refund($input);
    	    if($returnData['return_code'] && $returnData['result_code'] && $returnData['cash_refund_fee']){
    	        $this->addOrderOnlinePayRefundRecord($order_id, $out_trade_no, 1, $returnData['cash_refund_fee']);
    	    }else{
    	        $this->addOrderOnlinePayRefundFailRecord($order_id, 1, json_encode($returnData));
    	    }
    	}
    	return $result;
    }
    /**
     * 退还用户优惠券
     * @param unknown $user_coupon_id
     * @return boolean
     */
    public function returnForUserCoupon($user_coupon_id){
    	$result = false;
    	if($user_coupon_id > 0){
    		$data = array('state'=>0);
    		$result = $this->db->where('id',$user_coupon_id)->update('dwf_user_coupon',$data);
    	}
    	return $result;
    }
    /**
     * 支付款项到用户帐户余额
     * @param unknown $user_id
     * @param unknown $ammount
     * @return boolean
     */
    public function returnForUserCountBalance($user_id,$order_id,$amount){
    	$result = false;
    	if($user_id >0 && $amount > 0){
    		$this->load->model('user_model');
    		$opreation = '+';
    		$remark = '取消订单退款';
    		$result = $this->user_model->updateUserCountBalance($user_id, $opreation , $amount)
    		&& $this->user_model->addUserCountRecord($user_id, $amount, $opreation , 0 , $order_id,$remark);
    	}
    	
    	return $result;
    }
    /**
     * 添加在线支付订单退款记录
     * @param unknown $order_id 订单id
     * @param unknown $online_pay_no 在线支付订单号
     * @param unknown $pay_way 支付渠道 1微信 3支付宝
     * @param unknown $refund_fee 退还金额 单位（分）
     * @return boolean
     */
    public function addOrderOnlinePayRefundRecord($order_id,$online_pay_no,$pay_way,$refund_fee){
        $result = false;
        $data = array(
            'order_id'=>$order_id,
            'online_pay_no'=>$online_pay_no,
            'pay_way'=>$pay_way,
            'refund_fee'=>$refund_fee,
            'refund_time'=>date('Y-m-d H:i:s')
        );
        $result = $this->db->insert('dwf_order_online_pay_refund',$data);
        return $result;
    }
    /**
     * 在线支付订单退款失败日志
     * @param unknown $order_id
     * @param unknown $pay_way
     * @param unknown $fail_log
     * @return unknown
     */
    public function addOrderOnlinePayRefundFailRecord($order_id,$pay_way,$fail_log){
        $result = false;
        $data = array(
            'order_id'=>$order_id,
            'pay_way'=>$pay_way,
            'fail_log'=>$fail_log,
            'add_time'=>date('Y-m-d H:i:s')
        );
        $result = $this->db->insert('dwf_order_online_pay_refund_fail_log',$data);
        return $result;
    }
}