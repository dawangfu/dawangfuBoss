<?php
/**
 * 
 * @author zhouming
 *
 */
class Shop_model extends MY_Model {

    public function __construct() {
            parent::__construct ();
    }
    /**
     * id查找商户信息
     * @param unknown $shopId
     * @return Ambigous <boolean, string, multitype:>
     */
    public function getShopInfoForId($shopId){
        $shopInfo = array();
        $sql = "SELECT * FROM `dwf_shop` WHERE `id`=?";
        $binds = array('id'=>$shopId);
        try {
        	$shopInfo = $this->queryRow($sql,$binds);

        } catch (Exception $e) {
        }
        return $shopInfo;
    }
    /**
     * 获取商家列表
     * @param unknown $param
     * @return unknown
     */
    public function getShopList($param){
    	if(isset($param['where']) && !empty($param['where'])){$this->db->where($param['where']);}
    	if(isset($param['where_in']) && !empty($param['where_in'])){
    		foreach($param['where_in'] as $key=>$value){
    			$this->db->where_in($key , $value);
    		}
    	}
    	$shopList = $this->db->get('dwf_shop')
    							->result_array();
    	return $shopList;
    }
    
	/**
	 * 查询行业信息
	 * @return type
	 */
    public function getAlldIndustry(){
        $industryInfo = array();
        $sql = "SELECT * FROM  `dwf_industry` ORDER BY f_order DESC , id ASC";
        try {
            $industryInfo = $this->queryAll($sql);

        } catch (Exception $e) {
        }
        return $industryInfo;
    }
	
	/**
	 * 添商家帐户变更纪录
	 * @param type $userId
	 * @param type $amount
	 * @param type $opreation
	 * @param type $type_id 默认0;结合operate + 1充值 2返还 3提成 4推荐会员关注公众号加薪
	 * @param type $order_num 结合type_id=2存会员优惠券id =3存会员订单号 =4推荐会员的id
	 * @param type $remark
	 * @return type
	 */
	public function addShopCountRecord($shop_id, $amount, $opreation, $type_id = 0, $order_num = '', $remark = ''){
		$result = FALSE;
		$data = array(
			'shop_id'=>$shop_id,
			'amount'=>$amount,
			'operate'=>$opreation,
			'order_num'=>$order_num,
			'remark'=>$remark,
			'type_id'=>$type_id,
			'active_time'=>date('Y-m-d H:i:s')
		);
		$result = $this->db->insert('dwf_shop_account_record', $data);
		return $result;
	}
	/**
	 * 更新账户余额
	 * @param unknown $userId
	 * @param unknown $opreation
	 * @param unknown $amount
	 * @return unknown
	 */
	public function updateShopCountBalance($shop_id, $opreation, $amount){
		$result = FALSE;
		$result = $this->db->set('`count_balance`', '`count_balance`' . $opreation . $amount, FALSE)->where(array('id'=>$shop_id))->update('`dwf_shop`');
		return $result;
	}	
}