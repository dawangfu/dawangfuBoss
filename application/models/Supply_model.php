<?php

/**
 *
 *  @author fish
 *  供货
 */
class Supply_model extends MY_Model {
    public function __construct() {
        parent::__construct ();
    }

    /**
     * 获取区域经理供货清单
     * $isAll 导出所有
     */
    public function getSupplyList($manager = array(), $begainDate, $endDate, $deliverDate, $status, $isAll = false){
        $productList = [];
        if(empty($manager) && $isAll){
            if($status >= 0){
                $where = array(
                    'c.order_time >= ' => $begainDate . ' 00:00:00',
                    'c.order_time <= ' => $endDate . ' 23:59:59',
                    'c.deliver_time >= ' => $deliverDate . ' 00:00:00',
                    'c.deliver_time <= ' => $deliverDate . ' 23:59:59',
                    'c.status' => $status,
                    'f.parent_id != ' => '0',
                    'f.category_type' => 1
                );
            }else{
                $where = array(
                    'c.order_time >= ' => $begainDate . ' 00:00:00',
                    'c.order_time <= ' => $endDate . ' 23:59:59',
                    'c.deliver_time >= ' => $deliverDate . ' 00:00:00',
                    'c.deliver_time <= ' => $deliverDate . ' 23:59:59',
                    'f.parent_id != ' => '0',
                    'f.category_type' => 1
                );
            }
            $productList[0][0] = $this->db->select('a.product_id, SUM(a.product_num) as product_num, a.precise_unit, a.product_name as pname, a.sku_name, f.category_name, c.status')
                ->from('dwf_order_product as a')
                ->join('dwf_product as b', 'a.product_id = b.id', 'left')
                ->join('dwf_product_category as e', 'b.id = e.product_id', 'right')
                ->join('dwf_category as f', 'e.category_id = f.id', 'left')
                ->join('dwf_order as c', 'a.order_id = c.id', 'left')
                ->join('dwf_manager as d', 'c.manager_id = d.id', 'left')
                ->where($where)
                ->group_by('a.goods_id')
                ->order_by('f.parent_id asc, f.id asc')
                ->get()
                ->result_array();
            $productList[0][1] = '所有';
        }else{
            foreach($manager as $key => $value){
                if($status >= 0){
                    $where = array(
                        'c.order_time >= ' => $begainDate . ' 00:00:00',
                        'c.order_time <= ' => $endDate . ' 23:59:59',
                        'c.deliver_time >= ' => $deliverDate . ' 00:00:00',
                        'c.deliver_time <= ' => $deliverDate . ' 23:59:59',
                        'c.status' => $status,
                        'f.parent_id != ' => '0',
                        'c.manager_id' => $value['id'],
                        'f.category_type' => 1
                    );
                }else{
                    $where = array(
                        'c.order_time >= ' => $begainDate . ' 00:00:00',
                        'c.order_time <= ' => $endDate . ' 23:59:59',
                        'c.deliver_time >= ' => $deliverDate . ' 00:00:00',
                        'c.deliver_time <= ' => $deliverDate . ' 23:59:59',
                        'f.parent_id != ' => '0',
                        'c.manager_id' => $value['id'],
                        'f.category_type' => 1
                    );
                }
                //$productList[$key][0] = $this->db->select('a.product_id, SUM(a.product_num) as product_num, SUM(a.bargain_price) as total_count, a.precise_unit, a.product_name as pname, a.sku_name, b.product_name, f.category_name')
                $productList[$key][0] = $this->db->select('a.product_id, SUM(a.product_num) as product_num, a.precise_unit, a.product_name as pname, a.sku_name, f.category_name, c.status')
                    ->from('dwf_order_product as a')
                    ->join('dwf_product as b', 'a.product_id = b.id', 'left')
                    ->join('dwf_product_category as e', 'b.id = e.product_id', 'right')
                    ->join('dwf_category as f', 'e.category_id = f.id', 'left')
                    ->join('dwf_order as c', 'a.order_id = c.id', 'left')
                    ->join('dwf_manager as d', 'c.manager_id = d.id', 'left')
                    ->where($where)
                    ->group_by('a.goods_id')
                    ->order_by('f.parent_id asc, f.id asc')
                    ->get()
                    ->result_array();
                $productList[$key][1] = $value['name'];
            }
        }
        return $productList;
    }
}
