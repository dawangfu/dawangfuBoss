<?php
/**
 * 
 * @author zhouming
 * @property User_model $user_model
 * @property Category_model $category_model
 * @property Cismarty $cismarty
 * @property Validate_code  $validate_code
 * @property User  $user
 * @property Area_model  $area_model
 * @property Product_model  $product_model
 * @property Order_model  $order_model
 * @property Wechat_model $wechat_model
 * @property Shop_model   $shop_model
 * @property Pay_model   $pay_model
 * @property Manager_model   $manager_model
 * 
 */
class MY_Model extends CI_Model {
	public function __construct() {
		// Call the CI_Model constructor
		parent::__construct ();
		$this->init ();
	}
	public function init() {
	}
	
	public function queryRow($sql , $binds = array()) {
	    $result = array();
	    if ($sql === '')
	    {
	        log_message('error', 'Invalid query: '.$sql);
	        return ($this->db->db_debug) ? $this->db->display_error('db_invalid_query') : FALSE;
	    }else{
	        $query = $this->db->query($sql , $binds)  ;
	        if(is_object($query) && $query->num_rows() > 0){
	           $result =  $query->first_row('array');
	        }
	        $query->free_result();
	    }
	    return $result;
	}

	public function queryAll($sql , $binds = array()){
	    $result = array();
	    if ($sql === '')
	    {
	        log_message('error', 'Invalid query: '.$sql);
	        return ($this->db->db_debug) ? $this->db->display_error('db_invalid_query') : FALSE;
	    }else{
	       $query = $this->db->query($sql , $binds)  ;
	       if(is_object($query) && $query->num_rows() > 0){
	           foreach ($query->result_array() as $row)
	           {
	               array_push($result,$row);
	           }	              
	       }
	       $query->free_result();
	    }	    
	    return $result;
	}
	
	public function queryScalar($sql , $binds = array()) {
	    $result = '';
	    if ($sql === '')
	    {
	        log_message('error', 'Invalid query: '.$sql);
	        return ($this->db->db_debug) ? $this->db->display_error('db_invalid_query') : FALSE;
	    }else{
	        $query = $this->db->query($sql , $binds)  ;
	        if(is_object($query) && $query->num_rows() > 0){
	           $first_row =  $query->first_row('array');
	           $result = array_shift($first_row);
	        }
	        $query->free_result();
	    }
	    return $result;
	}
}