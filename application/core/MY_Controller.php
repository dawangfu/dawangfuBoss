<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct access allowed.' );
/**
 * 
 * @author zhouming
 * @property User_model $user_model
 * @property Category_model $category_model
 * @property Cismarty $cismarty
 * @property Validate_code  $validate_code
 * @property User  $user
 * @property Area_model  $area_model
 * @property Product_model  $product_model
 * @property Order_model  $order_model
 * @property Manager_model $manager_model
 * @property ProductAttribute_model $productAttribute_model
 * @property Shop_model $shop_model
 * @property Brand_model $brand_model
 * @property Coupons_model $coupons_model
 * @property Wechat_model $wechat_model
 * @property Member_model $member_model
 * 
 */
class MY_Controller extends CI_Controller { 

    public $menu = 2;  
    
    public $current = 1;
    
	public function __construct() {
		parent::__construct ();
		session_start();
		$this->init ();
	}
	protected function init() {
	    $this->assign('menu', $this->menu);
	    $this->assign('current', $this->current);
	    $this->checkLogin();
	}
	protected function assign($key, $value = null) {
		$this->cismarty->assign ( $key, $value );
	}
	protected function display($html) {
		$this->cismarty->display ( $html );
	}
	protected function checkLogin(){
	    $this -> load -> helper('url');
        if(!isset($_SESSION['user_id']) || $_SESSION['user_id'] == ''){
            redirect('/login/');
        }
	}
	
	public function isAjax() {
	    if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) ) {
	        if('xmlhttprequest' == strtolower($_SERVER['HTTP_X_REQUESTED_WITH']))
	            return true;
	    }
	    return false;
	}

} 