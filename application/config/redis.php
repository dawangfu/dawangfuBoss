<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

$config['socket_type'] = 'tcp'; //`tcp` or `unix`
$config['socket'] = '/var/run/redis.sock'; // in case of `unix` socket type
$config['host'] = '139.196.12.247';
$config['password'] = NULL;
$config['port'] = 6379;
$config['timeout'] = 0;