<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );

$config['upload_path'] = '../../../dawangfuImage/';
$config['allowed_types'] = 'gif|jpg|png|jpeg';
$config['max_size'] = '2048';
$config['file_ext_tolower'] = TRUE;