<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
$config['first_link'] = '首页';
$config['last_link'] = '尾页';
$config['cur_tag_open'] = '<span class="layui-laypage-curr"><em class="layui-laypage-em"></em><em>';
$config['cur_tag_close'] = '</em></span>';
$config['next_link'] = '下一页&gt;';
$config['prev_link'] = '&lt;上一页';
$config['num_tag_close'] = ' ';
$config['use_page_numbers'] = TRUE;
$config['num_links'] = 5;
$config['reuse_query_string'] = true;
$config['page_query_string'] = true;
$config['query_string_segment'] = 'page';