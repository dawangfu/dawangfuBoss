<?php

/**
 * 用户信息
 */
class User {

    public $isGuest = true;
    public $nickname = "";
    public $userid = 0;
    public $userKey = '';
    private $private_key = '';

    public function __construct(){
        $this->ci = & get_instance();
        $this->ci->load->config('project');
        $this->private_key = $this->ci->config->item('private_key');
        $this->init();
    }
    public function init() {
        $this->getUserInfoFromCookie();
    }

    public function getUserInfoFromCookie() {
        $userInfo = @$_COOKIE['MY_user'];
        $AC = base64_decode($userInfo);
        @list ($userid,  $nickname, $logintime, $C) = explode(",", $AC);
        $userid = intval($userid);
        $A = $userid . "," . $nickname . "," . $logintime;
        if (md5($A . "," . $this->private_key) != $C || $userid < 1) {
            $this->userid = 0;
            $this->nickname = '';
            $this->isGuest = true;
        } else {
            $this->userid = $userid;
            $this->nickname = $nickname;
            $this->isGuest = false;
        }
        if(isset($_COOKIE['user_key']) && $_COOKIE['user_key'] != ''){
            $this->userKey = $_COOKIE['user_key'];
        }
    }
    
    public function checkUserKey(){}
    
}
