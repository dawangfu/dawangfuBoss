<?php
if (! defined ( 'BASEPATH' )) exit ( 'No direct access allowed.' );

class MY_Upload extends CI_Upload{
    
    public function validate_upload_path($makeDir = TRUE)
    {
        if ($this->upload_path === '')
        {
            $this->set_error('upload_no_filepath');
            return FALSE;
        }
    
        if (realpath($this->upload_path) !== FALSE)
        {
            $this->upload_path = str_replace('\\', '/', realpath($this->upload_path));
        }
        
        if (! file_exists($this->upload_path))
        {
        	
           	if ($makeDir && ! mkdir($this->upload_path , 0777 , true))
           	{
	           	$this->set_error('upload_no_filepath');
	           	return FALSE;
          	}
           
        }
    
        if ( ! is_dir($this->upload_path))
        {
            $this->set_error('upload_no_filepath');
            return FALSE;
        }
    
        if ( ! is_really_writable($this->upload_path))
        {
            $this->set_error('upload_not_writable');
            return FALSE;
        }
    
        $this->upload_path = preg_replace('/(.+?)\/*$/', '\\1/',  $this->upload_path);
        return TRUE;
    }
    
    public function setDir()
    {
        if(!file_exists($this->upload_path))
        {
            $a = mkdir($this->upload_path , 0777 , true);
            if($a){
                echo 123;
            }else{
                echo 234;
            }
            exit;
        }
    }   
    
}