<?php
/**************************************************************************************************
 * 一个用于Mysql数据库的分页类
 *
 * @version				1.0
 * @lastupdate			2006-10-20
 *
 *
 * 使用实例:
 * $p = new show_page;		//建立新对像
 * $p->file="ttt.php";		//设置文件名，默认为当前页
 * $p->pvar="pagecount";	//设置页面传递的参数，默认为p
 * $p->setvar(array("a" => '1', "b" => '2'));	//设置要传递的参数,要注意的是此函数必须要在 set 前使用，否则变量传不过去
 * $p->set(20,2000,1);		//设置相关参数，共三个，分别为'页面大小'、'总记录数'、'当前页(如果为空则自动读取GET变量)'
 * $p->output(0);			//输出,为0时直接输出,否则返回一个字符串
 * echo $p->limit();		//输出Limit子句。在sql语句中用法为 "SELECT * FROM TABLE LIMIT {$p->limit()}";
 *
**************************************************************************************************/


class WPage
{
    /**
     * 页面输出结果
     *
     * @var string
     */
	public $output;

    /**
     * 使用该类的文件,默认为 PHP_SELF
     *
     * @var string
     */
	public $file;

    /**
     * 页数传递变量，默认为 'p'
     *
     * @var string
     */
	public $pvar = "p";

    /**
     * 页面大小
     *
     * @var integer
     */
	public $psize;

    /**
     * 当前页面
     *
     * @var ingeger
     */
	public $curr;

    /**
     * 要传递的变量数组
     *
     * @var array
     */
	public $varstr;

    /**
     * 总页数
     *
     * @var integer
     */
    public $tpage;

    /**
     * 分页设置
     *
     * @access public
     * @param int $pagesize 页面大小
     * @param int $total    总记录数
     * @param int $current  当前页数，默认会自动读取
     * @return void
     */
    public function set($pagesize=20,$total,$current=false) {
		global $_SERVER,$_GET;

		$this->tpage = ceil($total/$pagesize);
		if (!$current) 
		{
			if(isset($_GET[$this->pvar]))
				$current = $_GET[$this->pvar];
			else
				$current = "";
		}
		if ($current>$this->tpage) {$current = $this->tpage;}
		if ($current<1) {$current = 1;}

		$this->curr  = $current;
		$this->psize = $pagesize;

		if (!$this->file) {$this->file = $_SERVER['PHP_SELF'];}

		if ($this->tpage > 1) {
            
			if ($current>10) {
				$this->output.='<a href='.$this->file.'?'.$this->pvar.'='.($current-10).($this->varstr).' title="前十页">&lt;&lt;&lt;</a>&nbsp;';
			}
            if ($current>1) {
				$this->output.='<a href='.$this->file.'?'.$this->pvar.'='.($current-1).($this->varstr).' title="前一页">&lt;&lt;</a>&nbsp;';
			}
			if($current>5)
				$start = $current-4;
			else
            	$start	= floor($current/10)*10;
            $end	= $start+9;

            if ($start<1)			{$start=1;}
            if ($end>$this->tpage)	{$end=$this->tpage;}

            for ($i=$start; $i<=$end; $i++) {
                if ($current==$i) {
                    $this->output.='<a href="'.$this->file.'?'.$this->pvar.'='.$i.$this->varstr.'" class="num on">'.$i.'</a>&nbsp;';    //输出页数
                } else {
                    $this->output.='<a href="'.$this->file.'?'.$this->pvar.'='.$i.$this->varstr.'" class="num">'.$i.'</a>&nbsp;';    //输出页数
                }
            }

            if ($current<$this->tpage) {
				$this->output.='<a href='.$this->file.'?'.$this->pvar.'='.($current+1).($this->varstr).' title="下一页">&gt;&gt;</a>&nbsp;';
			}
            if ($this->tpage>10 && ($this->tpage-$current)>=10 ) {
				$this->output.='<a href='.$this->file.'?'.$this->pvar.'='.($current+10).($this->varstr).' title="下十页">&gt;&gt;&gt;</a>';
			}
		}
		$this->output.= "&nbsp;&nbsp;共 {$this->tpage} 页 / $total 条 ";
	}

    /**
     * 要传递的变量设置
     *
     * @access public
     * @param array $data   要传递的变量，用数组来表示，参见上面的例子
     * @return void
     */	
	public function setvar($data) {
		foreach ($data as $k=>$v) {
			$this->varstr.='&amp;'.$k.'='.urlencode($v);
		}
	}

    /**
     * 分页结果输出
     *
     * @access public
     * @param bool $return 为真时返回一个字符串，否则直接输出，默认直接输出
     * @return string
     */
	public function output($return = false) {
		if ($return) {
			return $this->output;
		} else {
			echo $this->output;
		}
	}

    /**
     * 生成Limit语句
     *
     * @access public
     * @return string
     */
    public function limit() {
		return (($this->curr-1)*$this->psize).','.$this->psize;
	}
	
	function get_strnum(){
		return ($this->curr-1)*$this->psize;
	}
	
	function get_endnum(){
		return $this->psize;
	}
	function get_page()
	{
		return $this->curr;
	}
	function __set($key,$value)
	{
		if($key == "Query")
		{
			$this->varstr ="&$value";
		}
	}

} //End Class
?>