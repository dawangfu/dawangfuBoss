<?php
/**
 * Created by fish.
 * User: fish
 * Date: 2017/7/17
 * Time: 上午10:21
 * 库存管理
 */
class Stock extends MY_Controller{

    public function __construct() {
        parent::__construct ();
    }

    public function index(){
        $this->load->model('product_model');
        $currentPage = $this->input->get('page',true);
        if($currentPage == 0){$currentPage = 1;}
        $limit = 15; //每页15条
        $start = ($currentPage - 1) * $limit;
        $where['limit'] = array($start , $limit);

        $search_array = $this->input->get('search',true);
        $search_array_str = json_encode($search_array);
        $this->assign('search_array',$search_array);
        $this->assign('search_array_str',$search_array_str);
        $productIdsCategory = array();
        $productIdsGoodsNo = array();
        $continue_flag = 1; //继续查表标识
        if($search_array['categroy_select_id'] > 0){
            $productIdsCategory = $this->product_model->getProductIdForCategory($search_array['categroy_select_id']);
            if(empty($productIdsCategory)){
                $continue_flag = 0;
            }
        }
        if($search_array['name'] == 'goods_no' && $continue_flag == 1){
            $productIdsGoodsNo = $this->product_model->getProductIdForGoodsNo($search_array['keywords']);
            if(empty($productIdsGoodsNo)){
                $continue_flag = 0;
            }
        }

        if($continue_flag == 1){

            if(!empty($productIdsCategory) && !empty($productIdsGoodsNo)){
                $productIdsArr = array_intersect($productIdsCategory ,$productIdsGoodsNo );
                if(!empty($productIdsArr)){
                    $where['where_in'] = array('id'=>$productIdsArr);
                }else{
                    $continue_flag = 0;
                }

            }elseif(!empty($productIdsCategory)){
                $where['where_in'] = array('id'=>$productIdsCategory);
            }elseif(!empty($productIdsGoodsNo)){
                $where['where_in'] = array('id'=>$productIdsGoodsNo);
            }

        }
        $productList = array();
        if($continue_flag == 1){
            if($search_array['name'] == 'name' && $search_array['keywords'] != ''){
                $where['like']['product_name'] = $search_array['keywords'];
            }
            if($search_array['shelf'] != ''){
                $where['where']['shelf'] = $search_array['shelf'];
            }
            if($search_array['shop_id'] != ''){
                $where['where']['shop_id'] = $search_array['shop_id'];
            }
            if($search_array['status'] != ''){
                $where['where']['status'] = $search_array['status'];
            }
            if($search_array['store_nums'] == 10){
                $where['where']['inventory <='] = 10;
                $where['where']['inventory >'] = 0;
            }elseif($search_array['inventory'] == 100){
                $where['where']['inventory <='] = 100;
                $where['where']['inventory >'] = 10;
            }elseif($search_array['inventory'] == 1000){
                $where['where']['inventory >'] = 100;
            }
            $where['order_by'] = 'id DESC';
            $productList = $this->product_model->getProductList($where);
            foreach($productList['products'] as $key => $value){
                $tmpSku = $this->product_model->getProductGoods($key);
                $productList['products'][$key]['goodsList'] = $tmpSku;
            }
        }
        $this->load->model('shop_model');
        $param = array (
            'where_in' => array (
                'type' => array(2,3)
            )
        );
        $shop = $this->shop_model->getShopList($param);
        $this->assign ( 'shop', $shop );
        $totalNum = 0;
        $products = array();
        if(!empty($productList)){
            $totalNum = $productList['productNum'];
            $products = $productList['products'];
        }
        $this->assign('products',$products);
        $this->load->library('pagination');
        $config['base_url'] = '/product/productList/';
        $config['total_rows'] = $totalNum;
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $pageStr =  $this->pagination->create_links();
        $totalPage = ceil($totalNum/$limit);
        $this->assign('currentPage',$currentPage);
        $this->assign('totalPage',$totalPage);
        $this->assign('pageStr',$pageStr);


        $this->assign('title', '库存管理');
        $this->assign('menu', 0);
        $this->assign('current', 4);
        $this->display('stock/index.html');
    }
}