<?php
/**
 * Created by fish.
 * Date: 2017/6/11
 * Time: 下午3:20
 */
class Supply extends MY_Controller {
    public function __construct() {
        parent::__construct ();
    }

    /**
     * 供货统计
     */
    public function supplyList(){
        $this->load->model('Manager_model');
        $managerList = $this->Manager_model->getManagerList();
        $this->assign('managerList', $managerList);
        $this->assign('title', '供货统计');
        $this->assign('menu', 0);
        $this->assign('current', 2);
        $this->display('supply/supplyList.html');
    }

    /**
     * 度量单位
     */
    public function measureList(){
        $this->assign('title', '度量统计');
        $this->assign('menu', 0);
        $this->assign('current', 3);
        $this->display('supply/measureList.html');
    }
}
