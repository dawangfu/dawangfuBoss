<?php
class Login extends MY_Controller{
    
    private $private_key = '';
    public function __construct() {
        parent::__construct ();
    }
    protected function init() {
    }
    public function index() {
    	$this->load->helper('url');
    	if(isset($_SESSION['USER']['id']) && $_SESSION['USER']['id'] > 0){
    		redirect('/wechat/member/order_list/');
    	}
    	$this->display('wechat/login.html');
    }
    
    public function check(){
        $userName = $this->input->post ( 'username' );
        $password = $this->input->post ( 'password' );
        $this->load->model('manager_model');
        $userInfo = $this->manager_model->getUserInfoFromKeyword($userName);
        $data = array(
            'success'=> FALSE,
            'errorCode'=>'service_exception',
            'errorMessage'=>'服务器异常，请稍后再试！'            
        );
        
        if(empty($userInfo)){
            $data['errorCode'] = 'unknownUsername';
            $data['errorMessage'] = '该用户不存在！';
        }else{
            if(md5($password) != $userInfo['password']){
                $data['errorCode'] = 'badPassword.msg1';
                $data['errorMessage'] = '密码错误';                
            }else{
            	$data['success'] = TRUE;
            	$data['errorCode'] = 'ok';
            	$data['errorMessage'] = '登录成功！';
            	session_start();
            	$_SESSION['USER']['username']=$userInfo['username'];
            	$_SESSION['USER']['mobile']=$userInfo['tel'];
            	$_SESSION['USER']['id']=$userInfo['id'];
            	$expire = time()+86400;
            	setcookie('userid',$_SESSION['USER']['id'],$expire,'/');
            }           
        }
        echo json_encode($data);
    }
    
    public function logout(){}
    public function forgetpsw() {
        echo 11;
    }
}