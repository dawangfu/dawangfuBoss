<?php

/**
 * 
 * @author zhouming
 * @property Wechat_model $wechat_model
 */
class WeChat extends MY_Controller {

	public function __construct(){
		
		parent::__construct() ;
	}

	public function init(){
		session_start() ;
	}

	protected function checkLogin(){
		$this->load->model('wechat_model') ;
		$this->load->model('user_model') ;
		if($this->wechat_model->is_weixin()){
			if(!isset($_SESSION['USER']['wx_open_id']) || $_SESSION['USER']['wx_open_id'] == ''){
				if(!isset($_GET['code']) || $_GET['code'] == ''){
					$this->wechat_model->getOAuthCode() ;
				}else{
					$code = $_GET['code'] ;
					$token = $this->wechat_model->getOAuthAccessToken($code) ;
					$_SESSION['USER']['wx_open_id'] = $token->openid ;
				}
			}
			$user = $this->user_model->getUserInfoFromWxOpenId($_SESSION['USER']['wx_open_id']) ;
			if(!empty($user)){
				$_SESSION['USER']['id'] = $user['id'] ;
				$_SESSION['USER']['shop_name'] = $user['shop_name'] ;
				$_SESSION['USER']['assistant_name'] = $user['assistant_name'] ;
				$_SESSION['USER']['shop_name'] = $user['shop_name'] ;
				$_SESSION['USER']['assistant_id'] = $user['assistant_id'] ;
				$_SESSION['USER']['administrator_flag'] = $user['administrator_flag'] ;
				$user_wx_info = $this->user_model->getWeChatUserInfo($_SESSION['USER']['wx_open_id']) ;
				if(isset($user_wx_info['nickname'])){
					$data = array('nick_name'=>$user_wx_info['nickname']) ;
					$this->user_model->updateShopWechatInfo($_SESSION['USER']['wx_open_id'] , $data);
				}
			}else{
				unset($_SESSION['USER']['id']) ;
				unset($_SESSION['USER']['shop_name']) ;
				unset($_SESSION['USER']['assistant_name']) ;
				unset($_SESSION['USER']['assistant_id']) ;
				unset($_SESSION['USER']['administrator_flag']) ;
				if($_SERVER['PATH_INFO'] != '/wechat/login'){
					$this->load->helper('url') ;
					redirect('/wechat/login') ;
				}
			}
			$jsapiTicket = $this->wechat_model->jsapiTicket() ;
			$nonceStr = 'dawangfu_business' ;
			$timestamp = time() ;
			$url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ;
			$string = 'jsapi_ticket=' . $jsapiTicket . '&noncestr=' . $nonceStr . '&timestamp=' . $timestamp . '&url=' . $url ;
			$stringSha1 = sha1($string) ;
			$this->assign('timestamp' , $timestamp) ;
			$this->assign('nonceStr' , $nonceStr) ;
			$this->assign('stringSha1' , $stringSha1) ;
		}else{
			//die('请在微信中访问!');
		}
	}

	public function index(){
		$this->load->model('wechat_model') ;
		if(!isset($_GET['echostr'])){
			$this->wechat_model->responseMsg() ;
		}else{
			$this->wechat_model->valid() ;
		}
	}


	public function menu(){
		error_reporting(E_ALL) ;
		$this->load->model('wechat_model') ;
		$jsonmenu = '{
                  "button":[
                      {
                            "type": "view",
                            "name": "订单查看",
                            "url":"http://boss.dawangfu.com/wechat/member/order_list/"
                      },
    				  {
                            "type": "view",
                            "name": "交账",
                            "url":"http://boss.dawangfu.com/wechat/member/account_for_commpany/"
                      },
    		          {
    			           "name":"提成▪业绩",
    			           "sub_button":[
    			            {
    				          "type":"view",
    				          "name":"配送提成",
    				          "url":"http://boss.dawangfu.com/wechat/member/manager_fee/"
    			            },
    						{
    				          "type":"view",
    				          "name":"推荐业绩",
    				          "url":"http://boss.dawangfu.com/wechat/member/week_fee_for_recommender/"
    			            }
    						]
    			      }
                  ]
             }' ;
		$access_token = $this->wechat_model->getAccessToken() ;
		$url = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=" . $access_token ;
		$result = $this->wechat_model->httpRequest($url , $jsonmenu , 'post') ;
		var_dump($result) ;
	}

}
