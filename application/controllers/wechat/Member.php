<?php
/**
 * Description of Member
 *
 * @author zhouming
 * 
 * @property Member_model $member_model
 */
class Member extends MY_Controller {
	public function __construct() {
		parent::__construct ();
	}
	public function init() {
	    if(isset($_COOKIE['userid'])){
	    	$_SESSION['USER']['id'] = $_COOKIE['userid'];
	    }
		if(!isset($_SESSION['USER']['id']) || $_SESSION['USER']['id'] == 0){
			$this->load->helper('url');
			redirect('/wechat/login/');
		}
	}
	public function index(){
		echo 1;
	}
	
	public function order_list(){
		$this->display('wechat/member/order_list.html');
	}
	
	public function order_detail($order_num){
		$data = array();
		if($order_num > 0){
			$this->load->model('order_model');
			$orderInfo = $this->order_model->getOrderInfoByNumber($order_num);
			if(!empty($orderInfo)){
				$orderProduct = $this->order_model->getOrderProducts($orderInfo['id']);
				$data['orderProduct'] = $orderProduct;
				$payRecoder = $this->order_model->getOrderPayRecoder($orderInfo['id']);
				$data['payRecoder'] = $payRecoder;
				$hasPay = 0;
				foreach($payRecoder as $value){
				    $hasPay = bcadd($hasPay , $value['money'],2);
				}
				$needPay = bcsub($orderInfo['total'], $hasPay,2);
				$orderInfo['needPay'] = number_format($needPay , 2);
			}
			$data['orderInfo'] = $orderInfo;
		}
		
// 		print_r($data);exit;
		$this->assign($data);
		$this->display('wechat/member/order_detail.html');
	}
	/**
	 * 交账
	 */
	public function account_for_commpany(){
	    $this->load->model('order_model');
	    $manager_id = $_SESSION['USER']['id'];
	    $orderList = $this->order_model->getAccountForCommpanyOrderList($manager_id);
	    $this->assign('orderList',$orderList);
	    $this->display('wechat/member/account_for_commpany.html');
	}
	/**
	 * 显示配送提成
	 */
	public function manager_fee(){
	    $manager_id = $_SESSION['USER']['id'];
	    $selectYear = $this->input->get('year',true);
	    intval($selectYear) == 0 && $selectYear = date('Y');
	    $startTime = $selectYear . '-01-01 00:00:00';
	    $endTime = $selectYear . '-12-31 23:59:59';
	    $this->load->model('manager_model');
	    $feeData = $this->manager_model->getManagerFee($manager_id ,$startTime ,$endTime);
// 	    print_r($feeData);exit;
	    $newFeeData = array();
	    for($i=1 ; $i <= 12 ;$i++){
	        $newFeeData[$selectYear][$i] = 0;
	    }
	    foreach($feeData as $value){
	        $date = DateTime::createFromFormat('Y-m-d H:i:s', $value['add_time']);
	        $year = $date->format('Y');
	        $mounth = $date->format('n');
	        $newFeeData[$year][$mounth] = bcadd($newFeeData[$year][$mounth], $value['amount'],2);
	    }
// 	    print_r($newFeeData);exit;
	    $this->assign('newFeeData',$newFeeData);
	    $this->assign('selectYear',$selectYear);
	    $this->display('wechat/member/mounth_fee.html');
	}
	/**
	 * 显示最近三周推荐用户的提成
	 */
	public function week_fee_for_recommender(){
	    $manager_id = $_SESSION['USER']['id'];
	    $this->load->model('manager_model');
	    $manger_info = $this->manager_model->getManagerDetail($manager_id);
	    if($manger_info['user_id'] <= 0){
	        $this->display('wechat/member/bind_user_code.html');
	    }else{
	        $member_id = $manger_info['user_id'];
	        //生成四周日历
	        $newfeeData = array();
	        for($i = 0 ; $i < 4 ; $i++){
	            $current = 0;
	            if($i == 0){
	                $current = 1;
	            }
	            $weekNumber = date('W') - $i;
	            $dayNumber = $weekNumber * 7;
	            $weekDayNumber = date("w", mktime(0, 0, 0, 1, $dayNumber, date("Y")));//当前周的第几天
	            $startNumber = $dayNumber - $weekDayNumber;
// 	            echo date("Y-m-d", mktime(0, 0, 0, 1, $startNumber + 1, date("Y")));//开始日期
// 	            echo date("Y-m-d", mktime(0, 0, 0, 1, $startNumber + 7, date("Y")));//结束日期
	            $newfeeData[$weekNumber] = array(
	                'range'=>date("Y-m-d", mktime(0, 0, 0, 1, $startNumber + 1, date("Y"))) . '--' . date("Y-m-d", mktime(0, 0, 0, 1, $startNumber + 7, date("Y"))),
	                'amount'=>0,
	                'current'=>$current,
	            );
	            
	        }
	        $endTime = date('Y-m-d H:i:s');
	        $startTime = date("Y-m-d H:i:s",mktime(0, 0, 0, 1, $startNumber + 1, date("Y")));
	        $this->load->model('member_model');
	        $feeData = $this->member_model->getRecommenderFeeForLastThreeWeek($member_id , $startTime , $endTime);
	        if(!empty($feeData)){
	            foreach($feeData as $value){
	                $week = date('W' , strtotime($value['active_time']));
	                $newfeeData[$week]['amount'] = bcadd($newfeeData[$week]['amount'], $value['amount'] , 2);
	            }
	        }
	        $this->assign('newfeeData',$newfeeData);
	        $this->display('wechat/member/week_fee_for_recommender.html');
	    }
	}
	
}
