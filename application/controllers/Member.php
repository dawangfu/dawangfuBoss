<?php
/**
 * 会员管理
 * @author zhouming
 *
 */
class Member extends MY_Controller {
    
    public $menu = 6;
    
	public function __construct() {
		parent::__construct ();
	}
	protected function init(){
	    parent::init();
	}
	public function index() {
	}
	
	public function base_info(){
	    
	}
	public function shop_assistant_list(){
	    
	}
	public function base_info_submit(){
	    
	}
	public function balance(){
	    
	}
	
	public function recharge_list(){
	    $this->title = '充值列表';
	    $this->assign ( 'title', $this->title );
// 	    $this->assign('current', 1);
	    $this->display('member/recharge_list.html');
	}
}