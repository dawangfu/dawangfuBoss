<?php
/**
 *
 * @author zhouming
 * @property Product_model $product_model
 */
class Product extends MY_Controller {
	public $menu = 3;
	public $title = '商品管理';
	public function __construct() {
		parent::__construct ();
	}
	protected function init() {
		parent::init ();
	}
	public function index() {
		$this->load->helper ( 'url' );
		redirect ( '/' );
	}
	/**
	 * 产品列表页
	 */
	public function productList() {
		$this->title = '商品列表';
		$this->assign ( 'title', $this->title );
		$this->load->model('product_model');
		$currentPage = $this->input->get('page',true);
		if($currentPage == 0){$currentPage = 1;}
		$limit = 15; //每页15条
		$start = ($currentPage - 1) * $limit;
		$where['limit'] = array($start , $limit);
		
		$search_array = $this->input->get('search',true);
		$search_array_str = json_encode($search_array);
		$this->assign('search_array',$search_array);
		$this->assign('search_array_str',$search_array_str);
		$productIdsCategory = array();
		$productIdsGoodsNo = array();
		$continue_flag = 1; //继续查表标识
		if($search_array['categroy_select_id'] > 0){
			$productIdsCategory = $this->product_model->getProductIdForCategory($search_array['categroy_select_id']);
			if(empty($productIdsCategory)){
				$continue_flag = 0;
			}
		}
		if($search_array['name'] == 'goods_no' && $continue_flag == 1){
			$productIdsGoodsNo = $this->product_model->getProductIdForGoodsNo($search_array['keywords']);
			if(empty($productIdsGoodsNo)){
				$continue_flag = 0;
			}
		}
		if($continue_flag == 1){
			if(!empty($productIdsCategory) && !empty($productIdsGoodsNo)){
				$productIdsArr = array_intersect($productIdsCategory ,$productIdsGoodsNo );
				if(!empty($productIdsArr)){
					$where['where_in'] = array('id'=>$productIdsArr);
				}else{
					$continue_flag = 0;
				}
			}elseif(!empty($productIdsCategory)){
				$where['where_in'] = array('id'=>$productIdsCategory);
			}elseif(!empty($productIdsGoodsNo)){
				$where['where_in'] = array('id'=>$productIdsGoodsNo);
			}
		}
		$productList = array();
		if($continue_flag == 1){
			if($search_array['name'] == 'name' && $search_array['keywords'] != ''){
				$where['like']['product_name'] = $search_array['keywords'];
			}
			if($search_array['shelf'] != ''){
				$where['where']['shelf'] = $search_array['shelf'];
			}
			if($search_array['shop_id'] != ''){
				$where['where']['shop_id'] = $search_array['shop_id'];
			}
			if($search_array['status'] != ''){
				$where['where']['status'] = $search_array['status'];
			}
			if($search_array['store_nums'] == 10){
				$where['where']['inventory <='] = 10;
				$where['where']['inventory >'] = 0;
			}elseif($search_array['inventory'] == 100){
				$where['where']['inventory <='] = 100;
				$where['where']['inventory >'] = 10;
			}elseif($search_array['inventory'] == 1000){
				$where['where']['inventory >'] = 100;
			}
            if($search_array['stock'] != ''){
                $where['where']['stock'] = $search_array['stock'];
            }
			$where['order_by'] = 'id DESC';
			$productList = $this->product_model->getProductList($where);
			foreach($productList['products'] as $key => $value){
                $tmpSku = $this->product_model->getProductGoods($key);
                $productList['products'][$key]['goodsList'] = $tmpSku;
            }
		}
		$this->load->model('shop_model');
		$param = array(
				'where_in' => array (
				    'type' => array(2,3)
				)
		);
		$shop = $this->shop_model->getShopList($param);
		$this->assign('shop', $shop);
		$totalNum = 0;
		$products = array();
		if(!empty($productList)){
			$totalNum = $productList['productNum'];
			$products = $productList['products'];
		}
		$this->assign('products',$products);
		$this->load->library('pagination');
		$config['base_url'] = '/product/productList/';
		$config['total_rows'] = $totalNum;
		$config['per_page'] = $limit;
		$this->pagination->initialize($config);
		$pageStr =  $this->pagination->create_links();
		$totalPage = ceil($totalNum/$limit);
		$this->assign('currentPage',$currentPage);
		$this->assign('totalPage',$totalPage);
		$this->assign('pageStr',$pageStr);
        $this->assign('menu', 3);
        $this->assign('current', 1);
		$this->display ( 'product/list.html' );
	}
	
	public function productListMini(){
		$this->title = '商品列表';
		$this->assign ( 'title', $this->title );
		$this->load->model('product_model');
		$currentPage = $this->input->get('page',true);
		if($currentPage == 0){$currentPage = 1;}
		$limit = 15; //每页15条
		$start = ($currentPage - 1) * $limit;
		$where['limit'] = array($start , $limit);
		
		$search_array = $this->input->get('search',true);
		$this->assign('search_array',$search_array);
		$productIdsCategory = array();
		$productIdsGoodsNo = array();
		$continue_flag = 1; //继续查表标识
		if($search_array['categroy_select_id'] > 0){
			$productIdsCategory = $this->product_model->getProductIdForCategory($search_array['categroy_select_id']);
			if(empty($productIdsCategory)){
				$continue_flag = 0;
			}
		}
		if($search_array['name'] == 'goods_no' && $continue_flag == 1){
			$productIdsGoodsNo = $this->product_model->getProductIdForGoodsNo($search_array['keywords']);
			if(empty($productIdsGoodsNo)){
				$continue_flag = 0;
			}
		}
		if($continue_flag == 1){
			if(!empty($productIdsCategory) && !empty($productIdsGoodsNo)){
				$productIdsArr = array_intersect($productIdsCategory ,$productIdsGoodsNo );
				if(!empty($productIdsArr)){
					$where['where_in'] = array('id'=>$productIdsArr);
				}else{
					$continue_flag = 0;
				}
			}elseif(!empty($productIdsCategory)){
				$where['where_in'] = array('id'=>$productIdsCategory);
			}elseif(!empty($productIdsGoodsNo)){
				$where['where_in'] = array('id'=>$productIdsGoodsNo);
			}
		}
		$productList = array();
		if($continue_flag == 1){
			if($search_array['name'] == 'name' && $search_array['keywords'] != ''){
				$where['like']['product_name'] = $search_array['keywords'];
			}
			if($search_array['shelf'] != ''){
				$where['where']['shelf'] = $search_array['shelf'];
			}
			if($search_array['shop_id'] != ''){
				$where['where']['shop_id'] = $search_array['shop_id'];
			}
			if($search_array['status'] != ''){
				$where['where']['status'] = $search_array['status'];
			}
			if($search_array['store_nums'] == 10){
				$where['where']['inventory <='] = 10;
				$where['where']['inventory >'] = 0;
			}elseif($search_array['inventory'] == 100){
				$where['where']['inventory <='] = 100;
				$where['where']['inventory >'] = 10;
			}elseif($search_array['inventory'] == 1000){
				$where['where']['inventory >'] = 100;
			}
			$where['order_by'] = 'id DESC';
			$productList = $this->product_model->getProductList($where);
		}
		$this->load->model('shop_model');
		$param = array (
				'where_in' => array (
						'type' => array(2,3)
				)
		);
		$shop = $this->shop_model->getShopList($param);
		$this->assign ('shop', $shop);
		$totalNum = 0;
		$products = array();
		if(!empty($productList)){
			$totalNum = $productList['productNum'];
			$products = $productList['products'];
		}
		$this->assign('products',$products);
		$this->load->library('pagination');
		$config['base_url'] = '/product/productListMini/';
		$config['total_rows'] = $totalNum;
		$config['per_page'] = $limit;
		$this->pagination->initialize($config);
		$pageStr =  $this->pagination->create_links();
		$totalPage = ceil($totalNum/$limit);
		$this->assign('currentPage',$currentPage);
		$this->assign('totalPage',$totalPage);
		$this->assign('pageStr',$pageStr);
		$this->display ( 'product/list_mini.html' );		
	}
	/**
	 * 商品编辑
	 */
	public function productEdit() {
		$this->title = '添加商品';
		$this->assign ( 'title', $this->title );
		$id = intval ( $this->uri->segment ( 3 ) );
		$this->load->model ( 'product_model' );
		$productBaseData = $this->product_model->getProductBaseData ( $id );
		$productBaseData ['product_info'] = $this->replaceProductInfo ( IFilter::stripSlash ( $productBaseData ['product_info'] ) );
		$this->assign ( 'productBaseData', $productBaseData );
		
		$productCategory = $this->product_model->getProductCategory($id);
		$this->assign('productCategory',$productCategory);
		$categoryids = array();
		foreach($productCategory as $key=>$value){
			$categoryids[] = $value['id'];
		}
		sort($categoryids);
		$this->assign('categoryids', implode($categoryids, ','));
		
		$this->load->model('shop_model');
		$param = array (
				'where_in' => array (
						'type' => array(2,3)
				)
		);
		
		$shop = $this->shop_model->getShopList($param);
		$this->assign ( 'shop', $shop );
		
		$this->load->model('brand_model');
		$param = array('where'=>array('shop_id'=>$productBaseData['shop_id']));
		$brand = $this->brand_model->getBrandList($param);
		$this->assign ( 'brand', $brand );
		$this -> load -> helper('url');
        $this->assign ( 'successUrl', current_url());
        $this->assign('menu', 3);
        $this->assign('current', 2);
		$this->display ( 'product/edit.html' );
	}
	/**
	 * 获取产品属性
	 * @param unknown $productId
	 */
	public function productAttr($productId) {
		$data = array (
				'success' => FALSE,
				'errorCode' => 500001,
				'errorMessage' => '服务器异常，请稍后再试！',
				'data' => array () 
		);
		$productId = IFilter::act ( $productId, 'int' );
		if ($productId > 0) {
			$this->load->model ( 'product_model' );
			$this->load->model ( 'productAttribute_model' );
			$productAttrbute = $this->product_model->getProductAttrbute ( $productId );
			$checkedAttrebuteIds = array ();
			$checkedAttrebuteValueIds = array ();
			foreach ( $productAttrbute as $value ) {
				$checkedAttrebuteIds [] = $value ['attribute_id'];
				$checkedAttrebuteValueIds [] = $value ['attribute_value_id'];
			}
			$checkedAttrebuteIds = array_unique ( $checkedAttrebuteIds );
			$checkedAttrebute = array();
			if(!empty($checkedAttrebuteIds)){
				$param = array (
						'where_in' => array (
								'id' => $checkedAttrebuteIds
						)
				);
				$checkedAttrebute = $this->productAttribute_model->getProductAttribute ( $param );
				unset ( $checkedAttrebute ['ids'] );
			}
			
			$checkedAttrebuteNew = array ();
			foreach ( $checkedAttrebute as $key => $value ) {
				$value ['name'] = $value ['attribute_name'];
				$value ['type'] = $value ['attribute_type'];
				foreach ( $value ['values'] as $k => $val ) {
					if (in_array ( $val ['id'], $checkedAttrebuteValueIds )) {
						$value ['values'] [$k] ['checekd'] = 1;
					} else {
						$value ['values'] [$k] ['checekd'] = 0;
					}
				}
				switch ($value ['attribute_type']) {
					case 1 :
						$checkedAttrebuteNew ['base'] [] = $value;
						break;
					case 2 :
						$checkedAttrebuteNew ['search'] [] = $value;
						break;
					case 3 :
						$checkedAttrebuteNew ['sku'] [] = $value;
						break;
				}
			}
			// print_r($checkedAttrebuteNew);exit;
			$data ['success'] = true;
			$data ['errorCode'] = 200001;
			$data ['errorMessage'] = '请求成功！';
			$data ['data'] = $checkedAttrebuteNew;
		}
		echo json_encode ( $data );
	}
	/**
	 * 获取货品
	 * @param unknown $productId
	 */
	public function productGoods($productId) {
		$data = array (
				'success' => FALSE,
				'errorCode' => 500001,
				'errorMessage' => '服务器异常，请稍后再试！',
				'data' => array () 
		);
		$productId = IFilter::act ( $productId, 'int' );
		if ($productId > 0) {
			$this->load->model ( 'product_model' );
			$this->load->model ( 'productAttribute_model' );
			$productGoodsData = $this->product_model->getProductGoodsData ( $productId );
			$skuAttrIds = array ();
			foreach ( $productGoodsData as $key => $value ) {
				$temp = JSON::decode ( $value ['sku_key'] );
				foreach ( $temp as $k => $val ) {
					$skuAttrIds [] = $k;
				}
			}
			$skuAttrIds = array_unique ( $skuAttrIds );
			$checkedAttrebute = array ();
			if (! empty ( $skuAttrIds )) {
				$param = array (
						'where_in' => array (
								'id' => $skuAttrIds 
						) 
				);
				$checkedAttrebute = $this->productAttribute_model->getProductAttribute ( $param );
				unset ( $checkedAttrebute ['ids'] );
			}
			foreach ( $productGoodsData as $key => $value ) {
				$temp = JSON::decode ( $value ['sku_key'] );
				$spec_array = array ();
				$spec = array ();
				foreach ( $temp as $k => $val ) {
					$spec ['id'] = $checkedAttrebute [$k] ['id'];
					$spec ['name'] = $checkedAttrebute [$k] ['attribute_name'];
					$spec ['type'] = $checkedAttrebute [$k] ['attribute_type'];
					foreach ( $checkedAttrebute [$k] ['values'] as $t ) {
						if ($t ['id'] == $val) {
							$spec ['value'] = $t;
							break;
						}
					}
					$spec_array [] = $spec;
				}
				$productGoodsData [$key] ['spec_array'] = $spec_array;
			}
			foreach ( $checkedAttrebute as $key => $value ) {
				$checkedAttrebute [$key] ['name'] = $value ['attribute_name'];
				$checkedAttrebute [$key] ['type'] = $value ['attribute_type'];
			}
			$data ['success'] = true;
			$data ['errorCode'] = 200001;
			$data ['errorMessage'] = '请求成功！';
			$data ['data'] = array (
					'specData' => $checkedAttrebute,
					'goodsList' => $productGoodsData 
			);
		}
        //print_r($data);exit;
		echo json_encode ( $data );
	}

    /**
     * 获取货品2 -- 废弃，已一直到Product_model中
     */
    /*public function getProductGoods($productId) {
        $data = array();
        $productId = IFilter::act($productId, 'int');
        if($productId > 0) {
            $this->load->model ( 'product_model' );
            $this->load->model ( 'productAttribute_model' );
            $productGoodsData = $this->product_model->getProductGoodsData ( $productId );
            $skuAttrIds = array ();
            foreach ( $productGoodsData as $key => $value ) {
                $temp = JSON::decode ( $value ['sku_key'] );
                foreach ( $temp as $k => $val ) {
                    $skuAttrIds [] = $k;
                }
            }
            $skuAttrIds = array_unique ( $skuAttrIds );
            $checkedAttrebute = array ();
            if (! empty ( $skuAttrIds )) {
                $param = array (
                    'where_in' => array (
                        'id' => $skuAttrIds
                    )
                );
                $checkedAttrebute = $this->productAttribute_model->getProductAttribute ( $param );
                unset ( $checkedAttrebute ['ids'] );
            }
            foreach ( $productGoodsData as $key => $value ) {
                $temp = JSON::decode ( $value ['sku_key'] );
                $spec_array = array ();
                $spec = array ();
                foreach ( $temp as $k => $val ) {
//                    $spec ['id'] = $checkedAttrebute [$k] ['id'];
//                    $spec ['name'] = $checkedAttrebute [$k] ['attribute_name'];
//                    $spec ['type'] = $checkedAttrebute [$k] ['attribute_type'];
                    foreach ( $checkedAttrebute [$k] ['values'] as $t ) {
                        if ($t ['id'] == $val) {
//                            $spec ['value'] = $t;
                            $spec ['value'] = $t['value_name'];
                            break;
                        }
                    }
                    $spec_array [] = $spec;
                }
                $productGoodsData [$key] ['spec_array'] = $spec_array;
            }
            foreach ( $checkedAttrebute as $key => $value ) {
                $checkedAttrebute [$key] ['name'] = $value ['attribute_name'];
                $checkedAttrebute [$key] ['type'] = $value ['attribute_type'];
            }
            $data = $productGoodsData;
        }
        return $data;
    }*/
	/**
	 * 获取产品图片
	 * @param unknown $productId
	 */
	public function productImages($productId) {
		$data = array (
				'success' => FALSE,
				'errorCode' => 500001,
				'errorMessage' => '服务器异常，请稍后再试！',
				'data' => array () 
		);
		$productId = IFilter::act ( $productId, 'int' );
		if ($productId > 0) {
			$this->load->model ( 'product_model' );
			$productImages = $this->product_model->getProductImages ( $productId );
			$productBaseData = $this->product_model->getProductBaseData ( $productId );
			$productImagesNew = array ();
			foreach ( $productImages as $value ) {
				$imgUlr = $this->config->item ( 'img_host' ) . $value ['image'];
				$imgPath = $value ['image'];
				$temp = array (
						'img_url' => $imgUlr,
						'img_path' => $imgPath 
				);
				if ($imgPath == $productBaseData ['product_image']) {
					$temp ['current'] = 1;
				} else {
					$temp ['current'] = 0;
				}
				$productImagesNew [] = $temp;
			}
			$data ['success'] = true;
			$data ['errorCode'] = 200001;
			$data ['errorMessage'] = '请求成功！';
			$data ['data'] = $productImagesNew;
		}
		// print_r($data);exit;
		echo json_encode ( $data );
	}
    /**
     * 获取指定图片ID的产品图片
     */

	/**
	 * 获取商家品牌数据
	 * @param unknown $shopId
	 */
	public function shopBrand($shopId){
		$data = array (
				'success' => FALSE,
				'errorCode' => 500001,
				'errorMessage' => '服务器异常，请稍后再试！',
				'data' => array ()
		);
		$shopId = IFilter::act ( $shopId, 'int' );
		if ($shopId > 0) {
			$this->load->model('brand_model');
			$param = array('where'=>array('shop_id'=>$shopId));
			$brand = $this->brand_model->getBrandList($param);
			$data ['success'] = true;
			$data ['errorCode'] = 200001;
			$data ['errorMessage'] = '请求成功！';
			$data ['data'] = $brand;
		}
		echo json_encode ( $data );
	}
	
	private function replaceProductInfo($product_info) {
		if ($product_info) {
			$pattern = "/(<img.*?src=[\"|\']?)(((?!http).)*?)([\"|\']?\s.*?>)/i";
			
			$replacement = "$1http://img.dawangfu.com$2$4";
			$product_info = preg_replace ( $pattern, $replacement, $product_info );
		}
		return $product_info;
	}
	/**
	 * 保存修改商品信息
	 */
	public function productUpdate() {
		// 检查表单提交状态
		if (! $_POST) {
			die ( '请确认表单提交正确' );
		}
		$successUrl = $this -> input -> get('successUrl', true);
		$id = IFilter::act ( $this->input->post ( 'id', true ), 'int' );
		$this->load->model ( 'product_model' );
		$this->product_model->update ( $id, $this->input->post () );
		$this->load->helper('url');
		redirect($successUrl);
	}
	/**
	 * 上传产品图片
	 */
	public function uploadProductImage() {
		$returnData = array (
				'status' => 0,
				'msg' => '服务器内部错误!' 
		);
		$file_paht = '/product/' . date ( 'Y/m/d/' );
		$config = array (
				'upload_path' => FCPATH . '../dawangfuImage/' . $file_paht,
				'allowed_types' => 'gif|jpg|png|jpeg',
				'max_size' => '2048',
				'file_ext_tolower' => TRUE,
				'encrypt_name' => TRUE 
		);
		$this->load->library ( 'upload', $config );
		if (! $this->upload->do_upload ( 'file' )) {
			$returnData = array (
					'status' => 1,
					'msg' => $this->upload->display_errors () 
			);
		} else {
			$filename = $this->upload->data ( 'file_name' );
			$imgUlr = $this->config->item ( 'img_host' ) . $file_paht . $filename;
			$imgPath = $file_paht . $filename;
			$data = array (
					'img_url' => $imgUlr,
					'img_path' => $imgPath 
			);
			$returnData = array (
					'status' => 1,
					'msg' => 'success!',
					'data' => $data 
			);
		}
		echo json_encode ( $returnData );
	}
	/**
	 * 编辑器上传图片
	 */
	public function uploadEditImage() {
		$returnData = array (
				'code' => 1,
				'msg' => '服务器内部错误!' 
		);
		$file_paht = '/edit/' . date ( 'Y/m/d/' );
		$config = array (
				'upload_path' => FCPATH . '../dawangfuImage/' . $file_paht,
				'allowed_types' => 'gif|jpg|png|jpeg',
				'max_size' => '2048',
				'file_ext_tolower' => TRUE,
				'encrypt_name' => TRUE 
		);
		$this->load->library ( 'upload', $config );
		if (! $this->upload->do_upload ( 'file' )) {
			$returnData = array (
					'msg' => $this->upload->display_errors () 
			);
		}else{
			$filename = $this->upload->data ( 'file_name' );
			$imgUlr = $this->config->item ( 'img_host' ) . $file_paht . $filename;
			$imgPath = $file_paht . $filename;
			$data = array (
					'src' => $imgUlr,
					'title' => $imgPath 
			);
			$returnData = array (
					'code' => 0,
					'msg' => '上传成功!',
					'data' => $data 
			);
		}
		echo json_encode ( $returnData );
	}

    /**
     * 设置产品是否为备货商品
     */
    public function changeStock(){
        $id = $this->input->get('id',true);
        $status = $this->input->get('status',true);
        $this->load->model('product_model');
        $this->product_model->updateProductStock($id, $status);
        $returnData = array (
            'code' => 1,
            'msg' => '更新成功!'
        );
        echo json_encode ( $returnData );
    }

	/**
	 * 修改产品上下架
	 */
	public function changeShelf(){
		$id = $this->input->get('id',true);
		$type = $this->input->get('type',true);
		$this->load->model('product_model');
		$this->product_model->updateProductShelf($id, $type);
		$returnData = array (
				'code' => 1,
				'msg' => '更新成功!'
		);
		echo json_encode ($returnData);
	}

    /**
     * 修改SKU上下架
     */
    public function changeGoodsShelf(){
        $id = $this->input->get('id',true);
        $type = $this->input->get('type',true);
        $this->load->model('product_model');
        $this->product_model->updateGoodsShelf($id, $type);
        $returnData = array (
            'code' => 1,
            'msg' => '更新成功!'
        );
        echo json_encode ( $returnData );
    }

    /**
     * 修改SKU库存
     */
    public function changeGoodsStore(){
        $id = $this->input->get('id',true);
        $store = $this->input->get('store',true);
        $this->load->model('product_model');
        $this->product_model->updateGoodsStore($id, $store);
        $returnData = array (
            'code' => 1,
            'msg' => '更新成功!'
        );
        echo json_encode ( $returnData );
    }

    /**
     * 修改SKU售价
     */
    public function changeGoodsPrice(){
        $id = $this->input->get('id',true);
        $price = $this->input->get('price',true);
        $this->load->model('product_model');
        $this->product_model->updateGoodsPrice($id, $price);
        $returnData = array (
            'code' => 1,
            'msg' => '更新成功!'
        );
        echo json_encode ( $returnData );
    }
    
    public function changeGoodsVipPrice(){

        $id = $this->input->get('id',true);
        $price = $this->input->get('price',true);
        $this->load->model('product_model');
        $this->product_model->updateGoodsVipPrice($id, $price);
        $returnData = array (
            'code' => 1,
            'msg' => '更新成功!'
        );
        echo json_encode ( $returnData );
        
    }

    /**
     * 修改SKU计量单位
     */
    public function changeGoodsPrecise(){
        $id = $this->input->get('id',true);
        $precise = $this->input->get('precise',true);
        $this->load->model('product_model');
        $this->product_model->updateGoodsPrecise($id, $precise);
        $returnData = array (
            'code' => 1,
            'msg' => '更新成功!'
        );
        echo json_encode ( $returnData );
    }

	/**
	 * 修改产品排序
	 */
	public function changeSort(){
		$id = $this->input->get('id',true);
		$sort = $this->input->get('sort',true);
		$this->load->model('product_model');
		$this->product_model->updateProductSort($id, $sort);
		$returnData = array (
				'code' => 1,
				'msg' => '更新成功!'
		);
		echo json_encode ( $returnData );
	}
	/**
	 * 更新库存
	 */
	public function update_store(){
		$productId = $this->input->get('product_id' ,true);
		$data = $this->input->get('data' , true);
		$param = array();
		$total_num = 0;
		foreach($data as $key=>$value){
			$total_num +=$value;
			$param[] = array('id'=>$key , 'store_nums'=>$value);
		}
		$this->load->model('product_model');
		$this->product_model->updateProductStore($productId, $total_num, $param);
		$returnData = array (
				'code' => 1,
				'data'=>$total_num,
				'msg' => '更新成功!'
		);
		echo json_encode ( $returnData );
	}
	/**
	 * 更新价格
	 */
	public function update_price(){
		$productId = $this->input->get('product_id' ,true);
		$data = $this->input->get('data' , true);
		$param = array();
		$product_price = array();
		$total_num = 0;
		foreach($data as $key=>$value){
			$param[] = array(
						'id'=>$key , 
						'market_price'=>$value['market_price'] , 
						'sell_price'=>$value['sell_price'] , 
						'cost_price'=>$value['cost_price'] , 
						'cash_back'=>$value['cash_back'] , 
						'proportion'=>$value['proportion'] , 
						'area_proportion'=>$value['area_proportion'] , 
					);
			if(empty($product_price)){
				$product_price = array(
						'market_price'=>$value['market_price'] ,
						'per_price'=>$value['sell_price'] ,
						'member_price'=>$value['sell_price'] ,
						'cost_price'=>$value['cost_price'] ,
						'cash_back'=>$value['cash_back'] ,
						'proportion'=>$value['proportion'] ,
						'area_proportion'=>$value['area_proportion'] ,
				);
			}
		}
		$this->load->model('product_model');
		$this->product_model->updateProductPrice($productId, $product_price, $param);
		$returnData = array (
				'code' => 1,
				'data'=>number_format($product_price['per_price'],2),
				'msg' => '更新成功!'
		);
		echo json_encode ( $returnData );		
	}
	/**
	 * 删除产品
	 */
	public function deleteProduct(){
		$id = $this->input->get('id',true);
		$this->load->model('product_model');
		$this->product_model->deleteProduct($id);
		$returnData = array (
				'code' => 0,
				'msg' => '更新成功!'
		);
		echo json_encode ( $returnData );
	}

	/**
     * 设置产品默认sku
     */
	public function setDefatultSku(){
        $pid = $this->input->get('pid', true);
	    $gid = $this->input->get('gid', true);
        $this->load->model('product_model');
	    $this->product_model->setDefaultSku($pid, $gid);
        $returnData = array (
            'code' => 0,
            'msg' => '更新成功!'
        );
        echo json_encode ($returnData);
    }

    /**
     * 产品真实库存入库
     */
    function setRealStoreNum(){
        $gid = $this->input->get('gid', true);
        $num = $this->input->get('num', true);
        $type = $this->input->get('type', true);
        $operate = $this->input->get('operate', true);
        $price = $this->input->get('price', true);
        $this->load->model('product_model');
        $this->product_model->setRealStoreNum($gid, $num, $type, $operate, $price);
        $returnData = array(
            'code' => 0,
            'msg' => '入库成功'
        );
        echo json_encode($returnData);
    }
}