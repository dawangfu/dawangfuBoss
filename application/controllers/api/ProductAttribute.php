<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
/**
 * Created by PhpStorm.
 * User: fish
 * Date: 16/10/27
 * Time: 下午6:08
 * @property ProductAttribute_model $Productattribute_model
 */
class ProductAttribute extends REST_Controller{

    public function __construct(){
        parent::__construct();
    }

    private function response_code($total_num = 0,
                                   $list = array(),
                                   $errorMessage = '请求成功',
                                   $errorCode = 200001,
                                   $success = TRUE){
        return [
            'success' => $success,
            'errorCode' => $errorCode,
            'errorMessage' => $errorMessage,
            'data' => [
                'total_num' => $total_num,
                'list' => $list
            ]
        ];
    }

    //获取所有属性名
    public function product_attribute_list_get(){
    	$data = array(
    			'success'=> FALSE,
    			'errorCode'=>500001,
    			'errorMessage'=>'服务器异常，请稍后再试！',
    	);
		$attribute_type = parent::get('attribute_type');
		$category_ids = parent::get('category_ids');
		if($category_ids != 0){
			$this ->load->model('Productattribute_model');
            $param['where'] = $attribute_type == ''?[]:array('attribute_type'=>$attribute_type);
			$param['where_in'] = array('category_id'=>explode(',', $category_ids));
			$attribute = $this->Productattribute_model->getProductAttribute($param);
			$data['data'] = $attribute;
			$data['success'] = TRUE;
			$data['errorCode'] = 200001;
			$data['errorMessage'] = '请求成功!';
		}else{
			$data['errorMessage'] = '参数错误!';
		}
		$this->response($data, REST_Controller::HTTP_OK);
    }

    //获取某分类下所有属性名
    public function  product_attribute_category_list_post(){
        $categoryID = parent::post('categoryID');
        $this -> load -> model('Productattribute_model');

    }

    /**
     *  产品属性详情
     */
    public function product_attribute_info($id){

    }

    /**
     *  添加属性
     */
    public function product_attribute_add_post(){
        $param = parent::post();
        if(!isset($param['id'])){
            $param['id'] = 0;
        }
        $options = [
            'f_order' => $param['f_order'],
            'form_type' => $param['form_type'],
            'category_id' => $param['category_id'],
            'attribute_name' => $param['attribute_name'],
            'attribute_type' => $param['attribute_type']
            ];
        $attributeValue = $param['attribute_value'];//属性值
        $this -> load -> model('Productattribute_model');

        $result = $this -> Productattribute_model -> productAttributeAdd($options, $param['id']);
        //更新商品属性值
        $this -> Productattribute_model -> attributeValueUpdate($param['attribute_value2']);

        if($result['result']){
            if($param['id'] == '0'){
                $msg = '添加成功';
                $options['id'] = $result['id'];
            }else{
                $msg = '保存成功';
                //$data = $this->response_code(1, $options, $msg, 200001, TRUE);
            }
            if(count($attributeValue) > 0){
                //保存属性
                $create_time = date('Y-m-d H:i:s');
                $valueParam = [];
                for($i = 0;$i < count($attributeValue);$i++){
                    $valueParam[$i] = [
                        'attribute_id' => $result['id'],
                        'category_id' => $param['category_id'],
                        'value_name' => $attributeValue[$i],
                        'create_time' => $create_time,
                        'shop_id' => 0,//商家ID,后台默认为0
                    ];
                }
                $result = $this -> Productattribute_model -> productAttributeValueAdd($valueParam);
                $data = $result?$this->response_code(1, $valueParam, $msg):$this->response_code(0, [],'接口错误', 200002, FALSE);
            }else{
                $data = $this->response_code(1, $options, $msg, 200001, TRUE);
            }
        }else{
            $data = $this->response_code(0, [],'接口错误', 200002, FALSE);
        }
        $this->response($data, REST_Controller::HTTP_OK);
    }

    /**
     *  添加属性值
     */
    public function product_attribute_value_add($options){
        if(count($options) > 0){
            $this -> load -> model('Productattribute_model');
            $result = $this -> Productattribute_model -> productAttributeValueAdd($options);
            return $result;
        }else{
            return false;
        }
    }

    /**
     *  根据属性值ID删除属性值(单个)
     */
    public function del_attribute_value_post(){
        $param = parent::post();
        $id = $param['id'];
        if($id == ''){
            $data = $this -> response_code(0, [], '接口错误', 200002, FALSE);
        }else{
            $this -> load -> model('Productattribute_model');
            $result = $this -> Productattribute_model -> attributeValueDel($id);
            if($result){
                $data = $this->response_code(1, [$id],'删除成功', 200001, TRUE);
            }else{
                $data = $this->response_code(0, [],'接口错误', 200002, FALSE);
            }
        }
        $this -> response($data, REST_Controller::HTTP_OK);
    }

    /**
     * 获取属性值
     */
    public function attribute_values_post(){
        $param = parent::post();
        $aid = $param['aid'];
        $cid = $param['cid'];
        $this -> load -> model('Productattribute_model');
        $result = $this -> Productattribute_model -> attributeValueGet($aid, $cid);
        if($result){
            $data = $this->response_code(count($result), $result,'请求成功', 200001, TRUE);
        }else{
            $data = $this->response_code(0, [],'接口错误', 200002, FALSE);
        }
        $this -> response($data, REST_Controller::HTTP_OK);
    }
    /**
     *  获取所有属性名
     */
    public function all_attribue_get(){
        $page = parent::get('page');
        $page = ($page - 1 >= 0)?$page - 1:0;
        $this -> load -> model('Productattribute_model');
        $list = $this -> Productattribute_model -> allAttributeGet($page);
        if($list){
            $data = $this -> response_code(count($list), $list, '请求成功', 200001, TRUE);
        }else{
            $data = $this -> response_code(0, [], '请求失败', 200002, FALSE);
        }
        $this -> response($data, REST_Controller::HTTP_OK);
    }

    /**
     *  根据属性值所属的属性名ID(attribute_id)删除属性值
     */
    public function product_attribute_del_post(){
        $param = parent::post();
        $id = $param['id'];
        $this -> load -> model('Productattribute_model');
        $result = $this -> Productattribute_model -> productAttributeDel($id);
        if($result){
            $data = $this -> response_code(1, [$id], '删除成功', 200001, TRUE);
        }else{
            $data = $this -> response_code(0, [], '删除失败', 200002, FALSE);
        }
        $this -> response($data, REST_Controller::HTTP_OK);
    }
}