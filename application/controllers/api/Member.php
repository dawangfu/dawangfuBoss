<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Member extends REST_Controller{
  
    public function __construct() {
        parent::__construct ();
        $this->init();
    }
    protected function init(){
    	session_start();
    }
    public function get(){
        $param = parent::get();
        return $param;
    }
    public function post(){
    	$param = parent::post();
    	return $param;
    }
    
    public function recharge_list_get(){
        $data = array(
            'success'=> true,
            'errorCode'=>200001,
            'errorMessage'=>'请求成功！',
            'data' => array()
        );
        $param = $this->get();
        $page = (isset($param['page']) && $param['page'] > 0) ? $param['page'] : 1;
        $limit = (isset($param['limit']) && $param['limit'] > 0) ? $param['limit'] : 20;
        $this->load->model('member_model');
        $recharge_list = $this->member_model->getRechargeList($page,$limit);
        $data['data'] = $recharge_list;
        $this->response($data, REST_Controller::HTTP_OK);
    }
    
}