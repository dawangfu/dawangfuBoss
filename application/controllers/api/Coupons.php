<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Coupons extends REST_Controller{
  
    public function __construct() {
        parent::__construct ();
        $this->init();
    }
    protected function init(){
    	session_start();
    }
    public function index_get() {
    	$data = array(
    			'success'=> FALSE,
    			'errorCode'=>500001,
    			'errorMessage'=>'服务器异常，请稍后再试！',
    			'data' => array()
    	);
        $param = $this->get();
        
        $data['data'] = $this->coupons_list($param);
        
        $this->response($data, REST_Controller::HTTP_OK);
    }
    public function index_post() {
    	$param = $this->get();
    	print_r($param);
    }
    public function get(){
        $param = parent::get();
        return $param;
    }
    public function post(){
    	$param = parent::post();
    	return $param;
    }
    
    private function coupons_list($param){
    	$this->load->model('coupons_model');
    	$page = (isset($param['page']) && $param['page'] > 0) ? $param['page'] : 1;
    	$limit = (isset($param['limit']) && $param['limit'] > 0) ? $param['limit'] : 15;
    	$where['limit'] = array($limit , ($page-1) * $limit);
    	if(isset($param['id'])){
    		if(is_array($param['id'])){
    			$where['where_in'] = array('id'=>$param['id']);
    		}elseif(is_numeric($param['id'])){
    			$where['where'] = array('id'=>$param['id']);
    		}
    	}
    	if(isset($param['online']) && is_numeric($param['online'])){
    		$where['where']['online'] = $param['online'];
    	}
    	if(isset($param['type']) && is_numeric($param['type'])){
    		$where['where']['category_type'] = $param['type'];
    	}
    	$where['fields'] = "`id`,`title`,`val`,`val_pay`,`minimum_charge`,`num`,`received_num`,`num_exchanged`,`num_used`,`st_date`,`end_date`";
    	$where['order_by'] = "id DESC";
    	$coupons = $this->coupons_model->getCouponList($where);
//     	print_r($coupons);exit;
    	return $coupons;
    }
    
    private function coupon_info($couponId){
    	
    }
    
    
    
}