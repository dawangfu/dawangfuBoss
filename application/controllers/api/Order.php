<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
/**
 *
 * @author zhouming
 * @property User_model $user_model
 * @property Category_model $category_model
 * @property Cismarty $cismarty
 * @property Validate_code  $validate_code
 * @property User  $user
 * @property Area_model  $area_model
 * @property Product_model  $product_model
 * @property Order_model  $order_model
 * @property Manager_model $manager_model
 *
 */
class Order extends REST_Controller{
  
    public function __construct() {
        parent::__construct ();
        $this->init();
    }
    protected function init(){
    	session_start();
    }
    public function index() {
        $this->load->helper('url');
        redirect('/');
    }
    public function get(){
        $param = json_decode(parent::get('d'),true);
        return $param;
    }
    public function post(){
    	$param = parent::post();
    	return $param;
    }
    public function order_list_get(){
    	$data = array(
    			'success'=> FALSE,
    			'errorCode'=>500001,
    			'errorMessage'=>'服务器异常，请稍后再试！',
    			'data' => array()
    	);
        $param = $this->get();
        if(isset($_SESSION['USER']['id'])){
        	$param['manager_id'] = $_SESSION['USER']['id'];
	        $this->load->model('order_model');
	        $order_list = array();
	        if(empty($param)){
	        	$data['errorMessage'] = '参数错误';
	        }elseif(isset($param['order_id']) && $param['order_id'] > 0 && strlen($param['order_id']) > 6){
	        	$orderNumber = $param['order_id'];
	        	$order_list[0] = $this->order_model->getOrderInfoByNumber($orderNumber);
	        	$data['success'] = TRUE;
	        	$data['errorCode'] = 200001;
	        	$data['errorMessage'] = '请求成功！';
	        	$data['data']['total_page'] = 1;
	        	$data['data']['total_num'] = 1;
	        	$data['data']['list'] = $order_list;
	        }elseif(isset($param['order_id']) && $param['order_id'] > 0 && strlen($param['order_id']) <= 6){
	            $page = $param['page'] ? $param['page'] : 1;
	            $limit = $param['limit'] ? $param['limit'] : 10;
	            $order_list = $this->order_model->getOrderList($param , $page , $limit);
	            $data['success'] = TRUE;
	            $data['errorCode'] = 200001;
	            $data['errorMessage'] = '请求成功！';
	            $data['data'] = $order_list;
	        }else{
	        	$page = $param['page'] ? $param['page'] : 1;
	        	$limit = $param['limit'] ? $param['limit'] : 10;
	        	unset($param['order_id']);
	        	
// 	        	(!isset($param['start_date']) || $param['start_date'] == '') && $param['start_date'] = '2017-06-01'; //6月之前的订单不显示 
	        	if(!isset($param['end_date']) || $param['end_date'] == ''){
	        	    if(!isset($param['start_date']) || $param['start_date'] == ''){
	        	        $currentTime = time();
	        	        $compareTime = strtotime(date('Y-m-d') . ' 15:00:00');
	        	        if($currentTime >= $compareTime){
	        	            $param['end_date'] = date('Y-m-d',strtotime('+1 day'));
	        	        }else{
	        	            $param['end_date'] = date('Y-m-d');
	        	        }
	        	        $param['start_date'] = '2017-06-01'; //6月之前的订单不显示
	        	    }else{
	        	        $param['end_date'] = $param['start_date'];
// 	        	        $currentTime = time();
// 	        	        $compareTime = strtotime(date('Y-m-d') . ' 19:00:00');
// 	        	        if($currentTime >= $compareTime){
// 	        	            $param['end_date'] = date('Y-m-d',(strtotime($param['start_date'])));
// 	        	        }else{
// 	        	            $param['end_date'] = $param['start_date'];
// 	        	        }
	        	    }
	        	    
	        	}
	        	$order_list = $this->order_model->getOrderList($param , $page , $limit);
	        	$data['success'] = TRUE;
	        	$data['errorCode'] = 200001;
	        	$data['errorMessage'] = '请求成功！';
	        	$data['data'] = $order_list;
	        }
        }
//         print_r($param);exit;
        $this->response($data, REST_Controller::HTTP_OK);
    }
    
    public function order_patch(){
    	$data = array(
    			'success'=> FALSE,
    			'errorCode'=>500001,
    			'errorMessage'=>'服务器异常，请稍后再试！',
    	);
    	$param = $this->patch();
    	if(isset($param['operation']) && isset($param['order_num'])){
    		$method = $param['operation'] . "Order";
    		if(method_exists($this, $method)){
    			$result = call_user_func(array($this , $method) ,$param);
    			if($result){
    				$data['success'] = TRUE;
    				$data['errorCode'] = 200001;
    				$data['errorMessage'] = "更新成功！";
    			}else{
    				$data['errorCode'] = 200004;
    				$data['errorMessage'] = "更新失败！";
    			}
    		}else{
    			$data['errorMessage'] = "参数异常！";
    		}
    	}else{
    		$data['errorMessage'] = "参数异常！";
    	}
    	
    	$this->response($data, REST_Controller::HTTP_OK);
    }
    /**
     * 订单配货
     * @param unknown $order_num
     */
    private function distributOrder($param){
    	$result = false;
    	if($param['order_num'] > 0){
    	    $order_num = $param['order_num'];
    		$this->load->model('order_model');
    		$orderInfo = $this->order_model->getOrderInfoByNumber($order_num);
    		if($orderInfo['status'] == 0){
    			$data = array('status'=>2);
    			$result = $this->order_model->updateOrderByOrderNum($order_num, $data);
    		}
    	}
    	return $result;
    }
    
    private function completeOrder($param){
    	$result = false;
    	if($param['order_num'] > 0){
    	    $order_num = $param['order_num'];
    		$this->load->model('order_model');
    		$orderInfo = $this->order_model->getOrderInfoByNumber($order_num);
    		if($orderInfo['status'] == 0 || $orderInfo['status'] == 2){
    			$data = array('status'=>1 , 'complete_time'=>date('Y-m-d H:i:s'));
    			$result = $this->order_model->updateOrderByOrderNum($order_num, $data);
//     			$this->order_model->handedOutCommission($orderInfo['id']);
                if($result){
                    $user_id = 0;
                    $user_type = 0;
                    if(isset($_SESSION['USER']['id'])){
                        $user_id = $_SESSION['USER']['id'];
                        $user_type = 2;
                    }elseif(isset($_SESSION['user_id'])){
                        $user_id = $_SESSION['user_id'];
                        $user_type = 3;
                    }
                    $this->order_model->addOrderCompletedRecord($orderInfo['id'], $user_id, $user_type);
                }
    		}
    	}
    	return $result;
    }
    
    private function cancelOrder($param){
    	$result = false;
    	if($param['order_num'] > 0){
    	    $order_num = $param['order_num'];
    		$this->load->model('order_model');
    		$orderInfo = $this->order_model->getOrderInfoByNumber($order_num);
    		if($orderInfo['status'] == 0 || $orderInfo['status'] == 2){
    			$data = array('status'=>3);
    			$result = $this->order_model->updateOrderByOrderNum($order_num, $data);
    			if($result){
    			    $user_id = 0;
    			    $user_type = 0;
    			    if(isset($_SESSION['USER']['id'])){
    			        $user_id = $_SESSION['USER']['id'];
    			        $user_type = 2;
    			    }elseif(isset($_SESSION['user_id'])){
    			        $user_id = $_SESSION['user_id'];
    			        $user_type = 3;
    			    }
    			    $this->order_model->addOrderCancelledRecord($orderInfo['id'], $user_id, $user_type,$param['remark']);
    			}
    		}
    	}
    	return $result;
    	
    }
}