<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
/**
 *
 * @author zhouming
 * @property Product_model $product_model
 * @property Cart_model  $cart_model
 */
class Product extends REST_Controller{

    public function __construct() {
        parent::__construct ();
    }
    public function get(){
        $param = parent::get();
        return $param;
    }
    /**
     * 产品列表页
     */
    public function productList_get(){
        $param = $this->get();
        $secondCatId = isset($param['cateId']) ? $param['cateId'] : 0;
        $keywords = isset($param['keywords']) ? $param['keywords'] : '';
        $this->load->model('product_model');
        $this->load->model('category_model');
        $where = array();
        
        if($category_id > 0){
            $where['where']['second_cat_id'] = $secondCatId;
        }elseif($keywords != ''){
            $where['like']['product_name'] = $keywords;
        }
        $currentPage = $param['page'];       
        $limit = 10;
        $start = ($currentPage - 1) * $limit;
        $where['limit'] = array($start , $limit);
        $brandId = isset($param['b']) ? $param['b'] : NULL;
        if($brandId != NULL ){
            $where['where']['brand_id'] = $brandId;            
        }
        
        $price = isset($param['p']) ? $param['p'] : NUll;        
        if($price != NULL){
            $priceRange = explode('_', $price);
            $where['where']['per_price >='] = floatval($priceRange[0]);
            if(isset($priceRange[1]) && floatval($priceRange[1]) > 0){
                $where['where']['per_price <='] = floatval($priceRange[1]);               
            }
        }
        $sort = isset($param['sort']) ? $param['sort'] : NUll;     
        
        $where['where']['status'] = 2;
        $where['where']['shelf'] = 1;
        $where['order_by'] = 'f_order DESC';
        if($sort != NULL){$where['order_by'] = self::$SORT_TYPE_VALUE[$sort];}
//         print_r($where);
//         exit;
        $productList = $this->product_model->getProductList($where);
        $productList['products'] = $this->mergeCartProduct($productList['products']);
        $productList['totalPage'] = ceil($productList['productNum']/$limit);
        $this->response($productList, REST_Controller::HTTP_OK);
 
    }
    public function searchList_get(){
        $param = $this->get();
        $keywords = $param['keywords'];
        $this->load->model('product_model');
        $this->load->model('category_model');
        $where = array();
//        $where['where']['second_cat_id'] = $secondCatId;
        $where['like']['product_name'] = $keywords;
        $currentPage = $param['page'];       
        $limit = 10;
        $start = ($currentPage - 1) * $limit;
        $where['limit'] = array($start , $limit);
        $brandId = isset($param['b']) ? $param['b'] : NULL;
        if($brandId != NULL ){
            $where['where']['brand_id'] = $brandId;            
        }
        
        $price = isset($param['p']) ? $param['p'] : NUll;        
        if($price != NULL){
            $priceRange = explode('_', $price);
            $where['where']['per_price >='] = floatval($priceRange[0]);
            if(isset($priceRange[1]) && floatval($priceRange[1]) > 0){
                $where['where']['per_price <='] = floatval($priceRange[1]);               
            }
        }
        $sort = isset($param['sort']) ? $param['sort'] : NUll;     
        
        $where['where']['status'] = 2;
        $where['where']['shelf'] = 1;
        $where['order_by'] = 'f_order DESC';
        if($sort != NULL){$where['order_by'] = self::$SORT_TYPE_VALUE[$sort];}
//         print_r($where);
//         exit;
        $productList = $this->product_model->getProductList($where);
        $productList['totalPage'] = ceil($productList['productNum']/$limit);
        $this->response($productList, REST_Controller::HTTP_OK);
 
    }
    /**
     * 合并购物车产品
     * @param unknown $productList
     * @return number
     */
    private function mergeCartProduct($productList){
    	$this->load->model('cart_model');
    	$cartProducts = $this->cart_model->getCartProduct();
    	$temp = array();
    	if(!empty($cartProducts)){
    		foreach($cartProducts as $value){
    			$temp[$value['product_id']] = $value['product_num'];
    		}
    	}
    	foreach($productList as $key=>$value){
    		if(!empty($temp) && array_key_exists($value['id'], $temp)){
    			$productList[$key]['cart_num'] = $temp[$value['id']];
    		}else{
    			$productList[$key]['cart_num'] = 0;
    		}
    	}
    	return $productList;
    }
    
    /**
     * 产品详情页
     * @param unknown $productId
     */
    public function productDetail($productId){
        if(intval($productId) <= 0){
            $this->load->helper('url');
            redirect('/');
        }
        $this->load->model('product_model');
        $this->load->model('category_model');
        $productInfo = $this->product_model->getProductInfo($productId);
        $productInfo['first_cat'] = $this->category_model->getOneFirstCategory($productInfo['first_cat_id']);
        $productInfo['second_cat'] = $this->category_model->getOneSecondCategory($productInfo['second_cat_id']);
//         print_r($productInfo);
//         exit;
        $this->assign('productInfo',$productInfo);
        $this->assign('title','aaaaa');
        $viewList = $this->product_model->updateUserViewRecord($productId);
        $param = array();
        $param['where'] = 'id IN(' . implode(',', $viewList). ')';       
        $viewListProduct = $this->product_model->getProductList($param);
        $this->assign('viewListProduct',$viewListProduct['products']);
        
        /*热卖推荐*/
        $param = array();
        $param['where'] = array('first_cat_id'=>$productInfo['first_cat_id']);
//         if($productInfo['first_cat_id'] > 0){
//             $param['where']['second_cat_id'] = $productInfo['first_cat_id'];
//         }
        $param['where']['status'] = 2;
        $param['where']['shelf'] = 1;
        $param['order_by'] = 'count DESC';
        $param['limit'] = 3;
        $recommend = $this->product_model->getProductList($param);
        $this->assign('recommend',$recommend['products']);
//         print_r($viewListProduct);
//         exit;
//         print_r($productInfo);
//         exit;
        $this->display('product/index.html');        
    }
    
}