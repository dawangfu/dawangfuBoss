<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';
class Pay extends REST_Controller{

    public function __construct() {
        parent::__construct ();
        $this->init();
    }
    protected function init(){
        session_start();
    }
    
    /**
     * 交账
     */
    public function account_for_commpany_get(){
        $data = array(
            'success'=> false,
            'errorCode'=>500001,
            'errorMessage'=>'服务器异常，请稍后再试！',
        );
        $this->load->model('order_model');
        $manager_id = $_SESSION['USER']['id'];
        $orderList = $this->order_model->getAccountForCommpanyOrderList($manager_id);
        $orderListId = array();
        if(!empty($orderList['orderList'])){
            foreach($orderList['orderList'] as $value){
                $orderListId[] = $value['id'];
            }
        }
        if(!empty($orderListId)&&$this->order_model->changeOrderAccountForCommpanyStatus($orderListId)){
            $this->load->model('pay_model');
            $result = $this->pay_model->addAccountForCommpanyRecord($manager_id, $orderList['total'], $orderList['orderList']);
            if($result){
                $data['success'] = true;
                $data['errorCode'] = 200001;
                $data['errorMessage'] = '交账成功！';
            }
            
        }else{
            $data['errorCode'] = 400001;
            $data['errorMessage'] = '交账失败！';
        }
        $this->response($data, REST_Controller::HTTP_OK);
        
    }
    
}