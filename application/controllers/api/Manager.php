<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
require APPPATH . '/libraries/REST_Controller.php';

class Manager extends REST_Controller{
  
    public function __construct() {
        parent::__construct ();
        $this->init();
    }
    protected function init(){
    	session_start();
    }
    public function get(){
        $param = parent::get();
        return $param;
    }
    public function post(){
    	$param = parent::post();
    	return $param;
    }
    
    public function bind_user_code_patch(){
        $data = array(
            'success'=> FALSE,
            'errorCode'=>500001,
            'errorMessage'=>'服务器异常，请稍后再试！',
        );
        $param = $this->patch();
        if(isset($_SESSION['USER']['id'])){
            $manager_id = $_SESSION['USER']['id'];
            if(is_numeric($param['userCode']) && $param['userCode'] > 0){
                $this->load->model('manager_model');
                $managerInfo = array('user_id'=>$param['userCode']);
                if($this->manager_model->updateManagerInfo($manager_id, $managerInfo)){
                    $data['success'] = TRUE;
                    $data['errorCode'] = 200001;
                    $data['errorMessage'] = '更新成功！';
                }
            }else{
                $data['errorMessage'] = '输入数据异常！';
            }
        }else{
            $data['errorCode'] = 400001;
            $data['errorMessage'] = '用户未登录！';
        }
        $this->response($data, REST_Controller::HTTP_OK);
    }
    
}