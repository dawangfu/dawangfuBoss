<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
/**
 *
 * @author zhouming
 * @property User_model $user_model
 * @property Category_model $category_model
 * @property Cismarty $cismarty
 * @property Validate_code  $validate_code
 * @property User  $user
 * @property Area_model  $area_model
 * @property Product_model  $product_model
 * @property Order_model  $order_model
 * @property Manager_model $manager_model
 *
 */
class Category extends REST_Controller{

    public function __construct(){
        parent::__construct();
    }

    private function response_code($total_num = 0, $list = array(), $errorMessage = '请求成功',$errorCode = 200001, $success = TRUE){
        return [
            'success' => $success,
            'errorCode' => $errorCode,
            'errorMessage' => $errorMessage,
            'data' => [
                'total_num' => $total_num,
                'list' => $list
            ]
        ];
    }

    /**
     *  商品分类列表
     */
    public function category_list_get(){
        $type = parent::get('type')?parent::get('type'):0;
        $this->load->model('category_model');
        $category_list = $this->category_model->getCategoryList($type);
        $category_list = PHPTree::makeTree($category_list);
        $data = $this -> response_code(count($category_list), $category_list);
        $this->response($data, REST_Controller::HTTP_OK);
    }

    /**
     *  删除分类
     */
    public function category_item_del_get(){
        $id = parent::get('id');
        $this -> load -> model('category_model');
        $result = $this -> category_model -> delCategoryItem($id);
        if($result['msg'] == 'success'){
            if($result['result']){
                $data = $this -> response_code(1, [], '删除成功', 200001, TRUE);
            }else{
                $data = $this -> response_code(0, [], '接口错误', 200002, FALSE);
            }
        }else{
            $data = $this -> response_code(0, [], '该分类下子分类不为空,无法删除', 200002, FALSE);
        }
        $this -> response($data, REST_Controller::HTTP_OK);
    }

    /**
     *  新增&更新分类
     */
    public function category_item_add_get(){
        //error_reporting(E_ALL);
        $id = parent::get('id');//所需更新记录的ID
        $parent_id = parent::get('parent_id');
        $product_ids = parent::get('product_ids');
        $category_name = parent::get('category_name');
        if($parent_id == '' || $category_name == ''){
            $data = $this -> response_code(0, array(), '缺少参数', 200002, FALSE);
        }else{
            $this -> load -> model('category_model');
            $options = [
                'parent_id' => $parent_id,
                'category_name' => $category_name,
                'category_type' => parent::get('category_type'),
                'category_icon' => parent::get('category_icon'),
                'index_show' => parent::get('index_show'),
                'f_order' => parent::get('f_order'),
                'title' => parent::get('title'),
                'keywords' => parent::get('keywords'),
                'description' => parent::get('description')
            ];
            $result = $this -> category_model -> addCategoryItem($options, $id, $product_ids);
            $data = $result?$this->response_code(1, $options):$this->response_code(0, [],'接口错误', 200002, FALSE);
        }
        $this->response($data, REST_Controller::HTTP_OK);
    }

    /**
     *  分类首页显示
     */
    public function category_item_show_get(){
        $id = parent::get('id');
        $options = [
            'index_show' => parent::get('status')
        ];
        $this -> load -> model('category_model');
        $result = $this -> category_model -> showCategoryItem($options, $id);
        $data = $result?$this -> response_code(1, $options, '操作成功'):$this -> response_code(0, [], '接口错误', 200002, FALSE);
        $this -> response($data, REST_Controller::HTTP_OK);
    }

    /**
     *  查找分类下所有产品
     */
    public function product_list_from_category_get(){
        //error_reporting(E_ALL);
        $cids = parent::get('cids');
        $cids = explode(',', $cids);
        $this -> load -> model('category_model');
        $result = $this -> category_model -> product_list_form_category($cids);
        $data = $result?$this -> response_code(count($result), $result, '操作成功'):$this -> response_code(0, [], '接口错误', 200002, FALSE);
        $this -> response($data, REST_Controller::HTTP_OK);
    }

}