<?php
/**
 * Created by fish.
 * User: fish
 * Date: 2017/8/22
 * Time: 下午2:03
 */
class Seckill extends MY_Controller {

    public function __construct() {
        parent::__construct ();
    }

    public $data = array (
            'success' => FALSE,
            'errorCode' => 500001,
            'errorMessage' => '服务器异常，请稍后再试！',
            'data' => array ()
        );

    /**
     * 设置单品的秒杀状态
     */
    public function setSkuSeckill(){
        $gid = $this->input->post('gid');
        $pid = $this->input->post('pid');
        $isSeckill = $this->input->post('isSeckill');
        if($gid > 0 && $pid > 0){
            $this->load->model('seckill_model');
            $result = $this->seckill_model->setSkuSeckill($gid, $isSeckill);
            if ($result) {
                $num = $this->seckill_model->getSkuSeckillNum($pid);
                $this->seckill_model->setProductSeckill($pid, ($num['num'] == 0) ? 0 : 1);
                if($isSeckill == 0){
                    //释放库存
                    $skuSeckillStore = $this->getSeckillStore($pid, $gid);
                    if($skuSeckillStore['store_nums_real'] > 0 || $skuSeckillStore['store_nums'] > 0){
                        $this->releaseSeckillStore($pid, $gid, $skuSeckillStore['store_nums'], $skuSeckillStore['store_nums_real']);
                    }
                }
            }
            $this->data['success'] = TRUE;
            $this->data['errorMessage'] = 'success';
            $this->insertSeckillRecord($pid, $gid, $isSeckill);
        }
        echo json_encode($this->data);
    }

    /**
     * 更新秒杀价格
     * @return bool
     */
    public function updateSeckillPrice(){
        $gid = $this->input->post('gid');
        $pid = $this->input->post('pid');
        $price = $this->input->post('num');
        $result = false;
        if($gid > 0 && $pid > 0){
            $this->load->model('seckill_model');
            $result = $this->seckill_model->updateSeckillPrice($pid, $gid, $price);
        }
        return $result;
    }

    /**
     * 更新秒杀库存
     * @return bool
     */
    public function updateSeckillStore(){
        $gid = $this->input->post('gid');
        $pid = $this->input->post('pid');
        $num = $this->input->post('num');
        $origin = $this->input->post('origin');//上次修改生效的库存(虚-实 如：3-9)
        $origin = explode("-",$origin);
        $result = false;
        if($gid > 0 && $pid > 0){
            $this->load->model('product_model');
            $storeNum = $this->product_model->getStoreNum($gid);
            if($num <= ($storeNum['store_nums'] + $storeNum['store_nums_real'] + $origin[0] + $origin[1])){
                if($num <= ($storeNum['store_nums_real'] + $origin[1])){
                    $store = 0;
                    $storeReal = $num;
                }else{
                    $store = $num - ($storeNum['store_nums_real'] + $origin[1]);
                    $storeReal = $storeNum['store_nums_real'] + $origin[1];
                }
                $this->product_model->updateStoreNum($gid,
                    $storeNum['store_nums'] + $origin[0] - $store,
                    $storeNum['store_nums_real']  + $origin[1] - $storeReal);
                $this->load->model('seckill_model');
                $result = $this->seckill_model->updateSeckillStore($pid, $gid, $store, $storeReal);
            }
            $this->data['success'] = $result;
            $this->data['errorMessage'] = 'success';
        }
        echo json_encode($this->data);
    }

    /**
     * 获取&插入秒杀&秒杀状态
     * @param $pid
     * @param $gid
     */
    private function insertSeckillRecord($pid, $gid, $isSeckill){
        $this->load->model('seckill_model');
        $seckillRecord = $this->seckill_model->getSkuSeckillRecord($pid, $gid);
        if($seckillRecord == 0){
            $data = array(
                'product_id' => $pid,
                'goods_id' => $gid,
                'status' => $isSeckill,
                'pre_seckill' => '0'
            );
            $this->seckill_model->insertSeckillRecord($data);
        }else{
            $data = array(
                'status' => $isSeckill,
                'pre_seckill' => '0'
            );
            $this->seckill_model->updateSeckillRecord($pid, $gid, $data);
        }
    }

    /**
     * 获取某秒杀规格商品的库存
     * @param $pid
     * @param $gid
     */
    private function getSeckillStore($pid, $gid){
        $seckillStore = [];
        $seckillStore = $this->db->select('store_nums,store_nums_real')
                                 ->from('dwf_seckill')
                                 ->where(
                                     array(
                                         'product_id' => $pid,
                                         'goods_id' => $gid
                                     )
                                 )
                                 ->get()
                                 ->result_array();
        return $seckillStore[0];
    }
    /**
     * 释放秒杀所占库存
     */
    private function releaseSeckillStore($pid, $gid, $store, $storeReal){
        $result = false;
        $this->load->model('product_model');
        $this->load->model('seckill_model');
        $storeNum = $this->product_model->getStoreNum($gid);
        $newStoreNum = $storeNum['store_nums'] + $store;
        $newRealStoreNum = $storeNum['store_nums_real'] + $storeReal;
        $this->seckill_model->updateSeckillStore($pid, $gid, 0, 0);
        $result = $this->product_model->updateStoreNum($gid, $newStoreNum, $newRealStoreNum);
        return $result;
    }

    /**
     * 批量将预秒杀商品上线
     */
    public function setSkuPreSeckill(){
        $ids = $this->input->post('ids');
        $this->load->model('seckill_model');
        foreach($ids as $key => $value){
            $this->seckill_model->setSkuSeckill($value['gid'], 1);
            $this->seckill_model->setProductSeckill($value['pid'], 1);
            $this->insertSeckillRecord($value['pid'], $value['gid'], 1);
            $this->data['success'] = TRUE;
            $this->data['errorMessage'] = 'success';
        }
        echo json_encode($this->data);
    }

    /**
     * 秒杀商品一键下线
     */
    public function setSeckillOffLine(){
        $this->load->model('seckill_model');
        $allSeckillProducts = $this->seckill_model->getSeckillProducts();
        if(!empty($allSeckillProducts)) {
            foreach ($allSeckillProducts as $key => $value) {
                $this->seckill_model->setSkuSeckill($value['goods_id'], 0);
                $this->seckill_model->setProductSeckill($value['product_id'], 0);
                $this->releaseSeckillStore($value['product_id'], $value['goods_id'], $value['store_nums'], $value['store_nums_real']);
                $this->insertSeckillRecord($value['product_id'], $value['goods_id'], 0);
            }
            $this->data['success'] = TRUE;
            $this->data['errorMessage'] = 'success';
        }
        echo json_encode($this->data);
    }
}