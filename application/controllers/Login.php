<?php
class Login extends MY_Controller{
    
    private $private_key = '';
    public function __construct() {
        parent::__construct ();
        $this -> load -> helper('url');
    }
    protected function init() {
        $this->load->config('project');
        $this->private_key = $this->config->item('private_key');
    }
    public function index() {
        $this -> display('login.html') ;
    }
    
    public function check(){
        session_start();
        $userName = $this->input->post ( 'username' );
        $password = $this->input->post ( 'password' );
        $this->load->model('user_model');
        $userInfo = $this->user_model->getUserInfoFromKeyword($userName);
        $data = array(
            'success'=> FALSE,
            'errorCode'=>'service_exception',
            'errorMessage'=>'服务器异常，请稍后再试！'            
        );
        if(empty($userInfo)){
            $data['errorCode'] = 'unknownUsername';
            $data['errorMessage'] = '该用户不存在！';
        }else{
            if(md5($password) != $userInfo['shop_pwd']){
                $data['errorCode'] = 'badPassword.msg1';
                $data['errorMessage'] = '密码错误';                
            }else if ($userInfo['status'] == 0 || $userInfo['status'] == 2){
                $data['errorCode'] = 'Did not pass the audit';
                $data['errorMessage'] = '您的账户还没有通过审核,请联系大旺福工作人员';
            }else{
                if(isset($_SESSION['USER']['wx_open_id']) 
                    && $this->user_model->bindWeChat($_SESSION['USER']['wx_open_id'], $userInfo['id'])){
                    $data['success'] = TRUE;
                    $data['errorCode'] = 'ok';
                    $data['errorMessage'] = '';
                    session_start();
                    $_SESSION['USER']['username']=$userInfo['user_name'];
                    $_SESSION['USER']['mobile']=$userInfo['mobile'];
                    $_SESSION['USER']['id']=$userInfo['id'];
                    $expire = time()+30*86400;
                    setcookie('userid',$_SESSION['USER']['id'],$expire,'/');
                }else{
                    $data['errorMessage'] = '绑定失败，请稍后再试！';
                }
                
            }           
        }
        echo json_encode($data);
    }
    
    public function logout(){
        setcookie('MY_user' , '' , time() , '/');
        session_start();
        session_destroy();
        header("Location: /login");
    }
    public function forgetpsw() {
        echo 11;
    }
    
    private function moveUserKeyCartProductToUser($userId) {
        $userKey = @$_COOKIE['user_key'];
        
        if($userKey){
           $this->load->model('cart_model');
           $this->cart_model->getUserKeyCart($userKey);
           $products = $this->cart_model->getCartProduct();
           $this->cart_model->emptyCartProduct();
           if(!empty($products)){
               $this->cart_model->checkUserIdCart($userId);
               $this->cart_model->updateCartBatch($products);               
           }           
        }
               
    }
    
    public function isLogin(){
        $this->load->library('user');
        if($this->user->userid > 0){
            echo 1;
        }else{
            echo 0;
        }       
    }

    /**
     * boss后台登录
     */
    public function signIn(){
        session_start();
        $userName = $this -> input -> post('username');
        $password = $this -> input -> post('password');
        $this -> load -> model('user_model');
        $this -> load -> helper('url');
        $result = $this -> user_model -> signIn($userName, $password);
        $url = '';
        if(!empty($result)){
            $_SESSION['user_id'] = $result[0]['id'];
            $_SESSION['user_name'] = $result[0]['realname'];
            $url = '/';
        }
        echo $url;
    }
}