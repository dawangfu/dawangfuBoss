<?php
class Ajax extends MY_Controller {
	public function __construct() {
		parent::__construct ();
	}
	public function init() {
	    
	}
	public function index() {
		session_start ();
		print_r ( $_SESSION );
	}
	public function captcha() {
		$this->load->library ( 'validate_code' );
		$this->validate_code->entry ();
	}
	public function checkCaptcha($code) {

		$success = FALSE;
		$errno = 0;
		$this->load->library ( 'validate_code' );
		$flag = $this->validate_code->check ( $code );
		$errno = $flag ? 1 : 0;
		$data = array (
				'success' => TRUE,
				'errno' => $errno
		);
		echo json_encode ( $data );
		
	}
	
	public function sendMobileCode(){
		$errno = 0;
		$phone = $this->input->post ( 'tel' );
		$identify_code = $this->input->post ( 'identify_code' );
		$this->load->library ( 'validate_code' );
		$flag = $this->validate_code->check ( $identify_code );
		if($flag){
		    $Client = new HttpClient("mssms.cn:8000");
		    $url = "http://mssms.cn:8000/msm/sdk/http/sendsms.jsp";
		    $rnd = rand(100000,999999);
		    $params = array('username'=>"JSMB260667",'scode'=>"740557",'mobile'=>$phone,'content'=>"@1@=".$rnd,'tempid'=>"MB-2013102300");
		    $pageContents = $Client->quickPost($url, $params);
		    if($pageContents){
		        $_SESSION['smscode'] = $rnd;
		        $_SESSION['checktel'] = $phone;
		        $this->validate_code->changeCodeValue();
		        $errno = 1;
		    }else{
		        $errno = -3;
		    }

		}else{
		    $errno = -2;
		}
		$data = array (
		    'success' => TRUE,
		    'errno' => $errno
		);
		echo json_encode ( $data );		
	}
	
	public function setAddress(){}
	
	public function locationToAddress(){
	    $this->checkLogin();
	    $returnData = array('status'=>0 , 'msg'=>'服务器内部错误!');
	    if(isset($_SESSION['supplierId']) && $_SESSION['supplierId'] > 0){
	        $location = $this->input->post('location',true);
	        if($location != ''){
	            $ele = new TMapLocationToAddress();
	            $ele->location = $location;
	            $service = new TMapService();
	            $address = $service->locationAddressQuery($ele);
	            $data = array(
	                'lat'=>$address[''][''],
	                'lng'=>$address[''][''],
	                'address'=>$address['address'],
	                'recommend'=>$address['formatted_addresses']['recommend'],
	            );
	            $returnData = array('status'=>1 , 'msg'=>'success!' , 'data'=>$data);
	        }
	    }else{
	        $returnData = array('status'=>-1 , 'msg'=>'请先登录!');
	    }
	    echo json_encode($returnData);
	    
	}
	
	public function autoSuggestion(){
	    $this->checkLogin();
	    $returnData = array('status'=>0 , 'msg'=>'服务器内部错误!');
	    if(isset($_SESSION['supplierId']) && $_SESSION['supplierId'] > 0){
	        $keywords = $this->input->get('keywords',true);
	        if($keywords != ''){
	            $ele = new TMapSuggestion();
	            $ele->keyword = $keywords;
	            $service = new TMapService();
	            $address = $service->autoSuggestionQuery($ele);
	            $str = '';
	            foreach ($address as $key => $val) {
	                $str .= "<a data-lat='".$val['location']['lat']."' data-lng='".$val['location']['lng']."'><span>".$val['title']."</span>（".$val['address']."）</a>";
	            }
	            $returnData = array('status'=>1 , 'msg'=>'success!' , 'data'=>$str);
	        }
	    }else{
	        $returnData = array('status'=>-1 , 'msg'=>'请先登录!');
	    }
	    echo json_encode($returnData);
	}
	
	public function uploadImage(){
	    $this->checkLogin();
	    $returnData = array('status'=>0 , 'msg'=>'服务器内部错误!');
	    if(isset($_SESSION['supplierId']) && $_SESSION['supplierId'] > 0){
	        $folder = $this->input->post('folder' ,true);
	        if($folder){
	            $config = array(
	                'upload_path'=>FCPATH . '../dawangfuImage/' .$folder . '/' ,
	                'allowed_types'=>'gif|jpg|png|jpeg',
	                'max_size'=>'2048',
	                'file_ext_tolower'=>TRUE,
	                'encrypt_name'=>TRUE
	            );
	            $this->load->library('upload',$config);
	            if ( ! $this->upload->do_upload('file')){
	                $returnData = array('status'=>1 , 'msg'=>$this->upload->display_errors());
	            }else{
	                $filename = $this->upload->data('file_name');
	                $imgUlr = $this->config->item('img_host') . '/' . $folder . '/' . $filename;
	                $imgPath = '/' . $folder .'/' . $filename;
	                $data = array('img_url'=>$imgUlr,'img_path'=>$imgPath);
	                $returnData = array('status'=>1 , 'msg'=>'success!' , 'data'=>$data);
	            } 
	        }else{

	            $config = array(
	                'upload_path'=>FCPATH . '../dawangfuImage/shop_logo/',
	                'allowed_types'=>'gif|jpg|png|jpeg',
	                'max_size'=>'2048',
	                'file_ext_tolower'=>TRUE,
	                'encrypt_name'=>TRUE
	            );
	            $this->load->library('upload',$config);
	            if ( ! $this->upload->do_upload('Filedata')){
	                $returnData = array('status'=>1 , 'msg'=>$this->upload->display_errors());
	            }else{
	                $filename = $this->upload->data('file_name');
	                $imgUlr = $this->config->item('img_host') . "/shop_logo/" . $filename;
	                $imgPath = '/shop_logo/' . $filename;
	                $data = array('img_url'=>$imgUlr,'img_path'=>$imgPath);
	                $returnData = array('status'=>1 , 'msg'=>'success!' , 'data'=>$data);
	            }
	             
	        }
	    }else{
	        $returnData = array('status'=>-1 , 'msg'=>'请先登录!');
	    }
	    echo json_encode($returnData);
	}
}