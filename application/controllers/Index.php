<?php
/**
 * 
 * @author zhouming
 * @property Index_model $index_model
 */
class Index extends MY_Controller{ 
    public $pageType = 1;  //定义页面类型 :1首页,2普通页面
	public function __construct() {
		parent::__construct ();
	}
	protected function init(){
	    parent::init();
	}
	public function index() {
        $this -> assign('title', '添加商品分类');
        $this -> assign('menu', 1);
        $this -> assign('current', 2);
		$this -> display('index/index-new.html');
	}
	public function category_list(){
	    $this -> assign('title', '商品分类列表');
        $this -> assign('menu', 1);
        $this -> assign('current', 1);
	    $this -> display('index/category_list.html');
    }
    public function editor(){
        $this -> assign('title', '编辑器');
        $this -> assign('menu', 3);
        $this -> assign('current', 1);
        $this -> display('index/editor.html');
    }

    /**
     *  编辑产品分类
    */
    public function category_edit()
    {
        $this->load->model('category_model');

        $category_id = $this->input->get('category_id');
        $result = $this->category_model->category_item_info($category_id);
        if (!$result) {
            $this->load->helper('url');
            redirect('/index/');
        }
        //关联产品
        $pids = '';
        if ($result['category_type'] == 2) {
            $productList = $this->category_model->product_list_form_category($category_id);
            for ($i = 0; $i < count($productList); $i++) {
                $pids .= $productList[$i]['id'] . ',';
            }
            if ($pids != '') {
                $pids = $this->removeLastStr($pids);
            }
        }
        $this -> assign('category_id', $category_id);
        $this -> assign('category_info', $result);
        $this -> assign('pids', $pids);
        $this -> assign('title', '编辑商品分类');
        $this -> assign('menu', 1);
        $this -> assign('current', 2);
        $this -> display('index/category_update.html');
    }

    /**
     *  去除字符串最后一位
     */
    public function removeLastStr($str){
        return substr($str, 0, strlen($str) - 1);
    }


    /**
     * 上传分类图标
     */
    public function uploadCategoryImage() {
        $returnData = array (
            'status' => 0,
            'msg' => '服务器内部错误!'
        );
        $file_paht = '/category/' . date ( 'Y/m/d/' );
        $config = array (
            'upload_path' => FCPATH . '../dawangfuImage' . $file_paht,
            'allowed_types' => 'gif|jpg|png|jpeg',
            'max_size' => '2048',
            'file_ext_tolower' => TRUE,
            'encrypt_name' => TRUE
        );
        $this->load->library ( 'upload', $config );
        if (! $this->upload->do_upload ( 'file' )) {
            $returnData = array (
                'status' => 1,
                'msg' => $this->upload->display_errors ()
            );
        } else {
            $filename = $this->upload->data ( 'file_name' );
            $imgUlr = $this->config->item ( 'img_host' ) . $file_paht . $filename;
            $imgPath = $file_paht . $filename;
            $data = array (
                'img_url' => $imgUlr,
                'img_path' => $imgPath
            );
            $returnData = array (
                'status' => 1,
                'msg' => 'success!',
                'data' => $data
            );
        }
        echo json_encode ( $returnData );
    }

    public function missionList(){
        $this->title = '低库存商品列表';
        $inventoryLimit = 5; //最低库存
        $this->assign ('title', $this->title);
        $this->load->model('product_model');
        $currentPage = $this->input->get('page',true);
        if($currentPage == 0){$currentPage = 1;}
        $limit = 15; // 每页十五条
        $start = ($currentPage - 1) * $limit;
        $where['limit'] = array($start , $limit);

        $search_array = $this->input->get('search',true);
        $search_array_str = json_encode($search_array);
        $this->assign('search_array',$search_array);
        $this->assign('search_array_str',$search_array_str);
        $productIdsCategory = array();
        $productIdsGoodsNo = array();
        $continue_flag = 1; //继续查表标识
        if($search_array['categroy_select_id'] > 0){
            $productIdsCategory = $this->product_model->getProductIdForCategory($search_array['categroy_select_id']);
            if(empty($productIdsCategory)){
                $continue_flag = 0;
            }
        }
        if($search_array['name'] == 'goods_no' && $continue_flag == 1){
            $productIdsGoodsNo = $this->product_model->getProductIdForGoodsNo($search_array['keywords']);
            if(empty($productIdsGoodsNo)){
                $continue_flag = 0;
            }
        }
        if($search_array['stock'] == '1'){
            $where2['where']['store_nums_real<='] = $inventoryLimit;
        }else{
            $where2['where']['store_nums<='] = $inventoryLimit;
        }

        $productsId = $this->product_model->getAllProductId($where2);
        if($continue_flag == 1){
            if(!empty($productIdsCategory) && !empty($productIdsGoodsNo)){
                $productIdsArr = array_intersect($productIdsCategory, $productIdsGoodsNo, $productsId);
                if(!empty($productIdsArr)){
                    $where['where_in'] = array('id'=>$productIdsArr);
                }else{
                    $continue_flag = 0;
                }
            }elseif(!empty($productIdsCategory)){
                $productIdsArr = array_intersect($productIdsCategory, $productsId);
                $where['where_in'] = array('id' => $productIdsArr);
            }elseif(!empty($productIdsGoodsNo)){
                $productIdsArr = array_intersect($productIdsGoodsNo, $productsId);
                $where['where_in'] = array('id' => $productIdsArr);
            }else{
                $where['where_in'] = array('id' => $productsId);
            }
        }
        $productList = array();
        if($continue_flag == 1){
            if($search_array['name'] == 'name' && $search_array['keywords'] != ''){
                $where['like']['product_name'] = $search_array['keywords'];
            }
            if($search_array['shelf'] != ''){
                $where['where']['shelf'] = $search_array['shelf'];
            }
            if($search_array['shop_id'] != ''){
                $where['where']['shop_id'] = $search_array['shop_id'];
            }
            if($search_array['status'] != ''){
                $where['where']['status'] = $search_array['status'];
            }
            if($search_array['stock'] != ''){
                $where['where']['stock'] = $search_array['stock'];
            }

            /*if($search_array['store_nums'] == 10){
                $where2['where']['store_nums <='] = 10;
                $where2['where']['store_nums >'] = 0;
            }elseif($search_array['store_nums'] == 100){
                $where2['where']['store_nums <='] = 100;
                $where2['where']['store_nums >'] = 10;
            }elseif($search_array['store_nums'] == 1000){
                $where2['where']['store_nums >'] = 100;
            }else{
                $where2['where']['store_nums<='] = $inventoryLimit;
            }*/
            $where['order_by'] = 'id DESC';
            if(!empty($where['where_in']['id'])){
                $productList = $this->product_model->getProductList($where);
                foreach($productList['products'] as $key => $value){
                    $tmpSku = $this->product_model->getProductGoods($key);
                    $productList['products'][$key]['goodsList'] = $tmpSku;
                }
            }else{
                $productList = [];
            }
        }
        $this->load->model('shop_model');
        $param = array (
            'where_in' => array (
                'type' => array(2,3)
            )
        );
        $shop = $this->shop_model->getShopList($param);
        $this->assign ( 'shop', $shop );
        $totalNum = 0;
        $products = array();
        if(!empty($productList)){
            $totalNum = $productList['productNum'];
            $products = $productList['products'];
        }
        $this->assign('products',$products);
        $this->load->library('pagination');
        $config['base_url'] = '/index/missionList/';
        $config['total_rows'] = $totalNum;
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $pageStr =  $this->pagination->create_links();
        $totalPage = ceil($totalNum/$limit);
        $this->assign('currentPage',$currentPage);
        $this->assign('totalPage',$totalPage);
        $this->assign('pageStr',$pageStr);

        $this->assign('menu', 0);
        $this->assign('current', 1);
        $this -> display('mission/list.html');
    }

    public function seckill(){
        $this->assign('title', '秒杀管理');
        $this->assign('menu', 0);
        $this->assign('current', 5);

        $this->load->model('product_model');
        $currentPage = $this->input->get('page',true);
        if($currentPage == 0){$currentPage = 1;}
        $limit = 15; //每页15条
        $start = ($currentPage - 1) * $limit;
        $where['limit'] = array($start , $limit);

        $search_array = $this->input->get('search',true);
        $search_array_str = json_encode($search_array);
        $this->assign('search_array',$search_array);
        $this->assign('search_array_str',$search_array_str);
        $productIdsCategory = array();
        $productIdsGoodsNo = array();
        $continue_flag = 1; //继续查表标识
        if($search_array['categroy_select_id'] > 0){
            $productIdsCategory = $this->product_model->getProductIdForCategory($search_array['categroy_select_id']);
            if(empty($productIdsCategory)){
                $continue_flag = 0;
            }
        }
        if($search_array['name'] == 'goods_no' && $continue_flag == 1){
            $productIdsGoodsNo = $this->product_model->getProductIdForGoodsNo($search_array['keywords']);
            if(empty($productIdsGoodsNo)){
                $continue_flag = 0;
            }
        }
        if($continue_flag == 1){
            if(!empty($productIdsCategory) && !empty($productIdsGoodsNo)){
                $productIdsArr = array_intersect($productIdsCategory ,$productIdsGoodsNo );
                if(!empty($productIdsArr)){
                    $where['where_in'] = array('id'=>$productIdsArr);
                }else{
                    $continue_flag = 0;
                }
            }elseif(!empty($productIdsCategory)){
                $where['where_in'] = array('id'=>$productIdsCategory);
            }elseif(!empty($productIdsGoodsNo)){
                $where['where_in'] = array('id'=>$productIdsGoodsNo);
            }
        }
        $productList = array();
        if($continue_flag == 1){
            if($search_array['name'] == 'name' && $search_array['keywords'] != ''){
                $where['like']['product_name'] = $search_array['keywords'];
            }
            if($search_array['shelf'] != ''){
                $where['where']['shelf'] = $search_array['shelf'];
            }
            if($search_array['shop_id'] != ''){
                $where['where']['shop_id'] = $search_array['shop_id'];
            }
            if($search_array['seckill'] != ''){
                $where['where']['seckill'] = $search_array['seckill'];
            }
            if($search_array['store_nums'] == 10){
                $where['where']['inventory <='] = 10;
                $where['where']['inventory >'] = 0;
            }elseif($search_array['inventory'] == 100){
                $where['where']['inventory <='] = 100;
                $where['where']['inventory >'] = 10;
            }elseif($search_array['inventory'] == 1000){
                $where['where']['inventory >'] = 100;
            }
            if($search_array['stock'] != ''){
                $where['where']['stock'] = $search_array['stock'];
            }
            $where['order_by'] = 'seckill DESC,id DESC';
            $productList = $this->product_model->getProductList($where);
            foreach($productList['products'] as $key => $value){
                $tmpSku = $this->product_model->getProductGoods($key, true);
                $productList['products'][$key]['goodsList'] = $tmpSku;
            }
        }
        $this->load->model('shop_model');
        $param = array(
            'where_in' => array (
                'type' => array(2,3)
            )
        );
        $shop = $this->shop_model->getShopList($param);
        $this->assign('shop', $shop);
        $totalNum = 0;
        $products = array();
        if(!empty($productList)){
            $totalNum = $productList['productNum'];
            $products = $productList['products'];
        }
        $this->assign('products',$products);
        $this->load->library('pagination');
        $config['base_url'] = '/index/seckill/';
        $config['total_rows'] = $totalNum;
        $config['per_page'] = $limit;
        $this->pagination->initialize($config);
        $pageStr =  $this->pagination->create_links();
        $totalPage = ceil($totalNum/$limit);
        $this->assign('currentPage',$currentPage);
        $this->assign('totalPage',$totalPage);
        $this->assign('pageStr',$pageStr);

        /* 预上线秒杀商品 */
        $this->load->model('seckill_model');
        $preSeckillProducts = $this->seckill_model->getPreSeckillProducts();
        foreach($preSeckillProducts as $key => $value){
            $productName = $this->product_model->getProductBaseData($value['product_id']);
            $preSeckillProducts[$key]['product_name'] = $productName['product_name'];
            $preSeckillProducts[$key]['sku_name'] = $this->removeLastStr($this->product_model->getGoodsSkuNameByGid($value['goods_id']));
        }
        $this->assign('preSeckillProducts',$preSeckillProducts);
        $this->display('mission/seckill.html');
    }
}