<?php
/**
 * Created by PhpStorm.
 * User: fish
 * Date: 16/10/27
 * Time: 下午4:06
 */
class ProductAttribute extends MY_Controller{

    public function __construct(){
        parent::__construct();
    }

    public function init(){
        parent::init();
    }

    /**
     *  商品属性列表
     */
    public function index(){
        $this -> load -> model('productAttribute_model');
        $total = $this -> productAttribute_model -> allAttributeCount();

        $this -> assign('total', $total);
        $this -> assign('title', '商品属性列表');
        $this->assign('menu', 2);
        $this->assign('current', 1);
        $this -> display('productAttribute/index.html');
    }

    /**
     *  添加商品属性
     */
    public function add(){
        $this -> assign('title', '添加商品属性');
        $this->assign('menu', 2);
        $this->assign('current', 2);
        $this -> display('productAttribute/product_attribute_add.html');
    }

    /**
     *  编辑商品属性
     */
    public function edit(){
        $attributeID = $this -> input -> get('attribute_id');
        if(!$attributeID){
            $this -> load -> helper('url');
            redirect('/productAttribute/add');
        }
        $this -> load -> model('productAttribute_model');
        $this -> load -> model('category_model');

        $info = $this -> productAttribute_model -> info($attributeID);
        $category = $this->category_model->category_item_info($info['category_id']);

        $this -> assign('title', '编辑商品属性');
        $this->assign('menu', 2);
        $this->assign('current', 2);
        $this -> assign('info', $info);
        $this -> assign('category', $category);
        $this -> display('productAttribute/product_attribute_edit.html');
    }
}