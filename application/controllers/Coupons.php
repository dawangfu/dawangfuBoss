<?php
class Coupons extends MY_Controller {
	public function __construct() {
		parent::__construct ();
	}
	public function init() {
	    parent::init();
	}
	public function index() {
		
	}
	/**
	 * 优惠券发放页
	 */
	public function issuing() {
		$this->title = '发放优惠券';
		$this->assign ( 'title', $this->title );
        $this->assign('menu', 4);
        $this->assign('current', 2);
	    $this->display('coupons/issuing.html');
	}

	/**
	 * 优惠券发放提交
	 */
	public function submit(){
// 	    print_r($_POST);
// 	    exit;
	    $this->load->model('coupons_model');
	    $this->load->model('user_model');
	    $title = $this->input->post('title',true);
	    $face_value = $this->input->post('face-value',true);
	    $use_range = $this->input->post('use-range' ,true);
	    $minimum_charge = $this->input->post('minimum-charge',true);
	    $exchangeable_amount = $this->input->post('exchangeable-amount',true);
	    $issue_number = $this->input->post('issue-number',true);
	    $content = $this->input->post('content',true);
	    $coupon_icon = $this->input->post('coupon_icon',true);
	    $start_date = $this->input->post('start-date',true);
	    $end_date= $this->input->post('end-date',true);
	    $handoutWay= $this->input->post('handout-way',true);
	    $data = array(
	        'title'=>$title,
	        'shop_id'=>0,  //没有商家
	        'industry_id'=>3,//网站都是消费品
	        'val'=>$face_value,
	    	'online'=>1,
	    	'category_type'=>$use_range,
	        'minimum_charge'=>$minimum_charge,
	        'val_pay'=>$exchangeable_amount,
	        'num'=>$issue_number,
	        'st_date'=>$start_date,
	        'end_date'=>$end_date,
	        'coupon_icon'=>$coupon_icon,
	        'content'=>$content,
	        'point_jd'=>0,
	        'point_wd'=>0,
	        'active_time'=>date('Y-m-d H:i:s'),
	    	'handout_way'=>$handoutWay	    	
	    ); 
	    $coupon_id = $this->coupons_model->addCoupons($data);
	    if($coupon_id > 0){
	    	switch ($use_range){
	    		case 2 :	//多品类券 添加优惠券关联分类
	    			$categroy_id = $this->input->post('categroy_id' ,true);
	    			$categroy_id_arr = explode(',', $categroy_id);
	    			$this->load->model('category_model');
	    			$category_info = $this->category_model->getCategoryInfo($categroy_id_arr);
	    			$coupon_categroy_data = array();
	    			foreach($category_info as $value){
	    				if(($value['category_type'] == 1 && $value['parent_id'] != 0) || $value['category_type'] == 2){
	    					$coupon_categroy_data[] = array('coupon_id'=>$coupon_id,'category_id'=>$value['id']);
	    				}
	    			}
	    			$this->coupons_model->addCouponCategory($coupon_categroy_data);
	    			break;
	    		case 3 :	//多产品券 添加优惠券关联产品信息
	    			$product_id = $this->input->post('product_id' ,true);
	    			$product_id_arr = explode(',', $product_id);
	    			$coupon_product_data = array();
	    			foreach($product_id_arr as $value){
	    				$coupon_product_data[] = array('coupon_id'=>$coupon_id,'product_id'=>$value);
	    			}
	    			$this->coupons_model->addCouponProduct($coupon_product_data);
	    			break;
	    		default:
	    	}
	    }
	    $this->load->helper('url');
	    redirect('/coupons/couponList/');
        
	}
	public function couponList(){
		$this->title = '优惠券列表';
		$this->assign ( 'title', $this->title );
		$this->load->model('coupons_model');
        $this->assign('menu', 4);
        $this->assign('current', 1);
		$this->display('coupons/list.html');
	}
	/**
	 * 上传优惠券图标
	 */
	public function uploadCouponIcon() {
	    error_reporting(E_ALL);
		$returnData = array (
				'status' => 0,
				'msg' => '服务器内部错误!'
		);
		$file_paht = '/coupon_icon/' . date ( 'Y/m/d/' );
		$config = array (
				'upload_path' => FCPATH . '../dawangfuImage/' . $file_paht,
				'allowed_types' => 'gif|jpg|png|jpeg',
				'max_size' => '2048',
				'file_ext_tolower' => TRUE,
				'encrypt_name' => TRUE
		);
		$this->load->library ( 'upload', $config );
		if (! $this->upload->do_upload ( 'file' )) {
			$returnData = array (
					'status' => 1,
					'msg' => $this->upload->display_errors ()
			);
		} else {
			$filename = $this->upload->data ( 'file_name' );
			$imgUlr = $this->config->item ( 'img_host' ) . $file_paht . $filename;
			$imgPath = $file_paht . $filename;
			$data = array (
					'img_url' => $imgUlr,
					'img_path' => $imgPath
			);
			$returnData = array (
					'status' => 1,
					'msg' => 'success!',
					'data' => $data
			);
		}
		echo json_encode ( $returnData );
	}	
	/**
	 * 优惠券详情
	 */
	public function coupon_info(){
	    $returnData = array('status'=>0 , 'msg'=>'服务器内部错误!');
	    if(isset($_SESSION['supplierId']) && $_SESSION['supplierId'] > 0){
	        $couponId = $this->input->post('coupon_id',true);
	        if($couponId > 0){
	            $this->load->model('coupons_model');
	            $coupon = $this->coupons_model->getCoupon($couponId);
	            $returnData = array('status'=>1 , 'msg'=>'success!' , 'data'=>$coupon);
	        }
	    }else{
	        $returnData = array('status'=>-1 , 'msg'=>'请先登录!');
	    }
	    echo json_encode($returnData);
	}
	
	public function coupon_used_list($coupon_id){
	    error_reporting(E_ALL);
		$shop_id = $_SESSION['supplierId'];
		$this->load->model('coupons_model');
	    $coupon = $this->coupons_model->getCoupon($coupon_id);
		$this->assign('coupon' , $coupon);
		$coupon_used_list = $this->coupons_model->getCouponUsedList($shop_id,$coupon_id);
		$this->load->model('user_model');
		foreach($coupon_used_list as $key=>$value){
			if($value['used_shop_wechat_id'] > 0){
				$assistant_info = $this->user_model->getAssistantInfo($value['used_shop_wechat_id']);
				$coupon_used_list[$key]['assistant_name'] = $assistant_info['true_name'];
			}else{
				$coupon_used_list[$key]['assistant_name'] = '暂无营业员信息';
			}
			
		}
		$this->assign('coupon_used_list', $coupon_used_list);
		$this->display('coupons/used_list.html');
	}
}