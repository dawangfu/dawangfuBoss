<?php
class Area extends MY_Controller {
	public function __construct() {
		parent::__construct ();
	}
	public function init() {
	    
	}
	public function index() {
		$this->display ( 'area/index.html' );
	}
	
	public function area_list(){
		$this->load->model('area_model');
		$area_info = $this->area_model->getAreaList();
// 		print_r($area_info);exit;
		$this->assign('area_info',$area_info);
		$this->display ( 'area/area.html' );
	}
	public function area_detail(){
		$this->load->model('area_model');
		$area_id = $this->uri->segment(3,0);
		$this->assign('area_id',$area_id);
		$area_info = $this->area_model->getAreaInfoId($area_id);
		$this->assign('area_info',$area_info);
		$area_list = $this->area_model->getAreaList();
		$this->assign('area_list',$area_list);
		$this->load->model('manager_model');
		$manager_list = $this->manager_model->getManagerList();
		$this->assign('manager_list',$manager_list);
// 		print_r($manager_list);exit;
		$this->display ( 'area/area_detail.html' );
	}
	/**
	 * 区域经理列表
	 */
	public function manager_list(){
		$this->load->model('manager_model');
		$this->load->model('area_model');
		$manager_list = $this->manager_model->getManagerList();
		$area_list = $this->area_model->getAreaList();
// 		print_r($area_list);exit;
		$area_list_temp = array();
		foreach($area_list as $key=>$value){
			$area_list_temp[$value['manager_id']][] = $value;
		}
		foreach($manager_list as $key=>$value){
			$manager_list[$key]['area_list'] = $area_list_temp[$value['id']];
		}
		$this->assign('manager_list',$manager_list);
// 		print_r($manager_list);exit;
		$this->display('area/manager_list.html');
	}
	/**
	 * 区域经理详情
	 */
	public function manager_detail(){
		$this->load->model('area_model');
		$this->load->model('manager_model');
		$manager_id = $this->uri->segment(3,0);
		$manager_detail = $this->manager_model->getManagerDetail($manager_id);
		$this->assign('manager_detail',$manager_detail);
		$this->assign('manager_id',$manager_id);
		$manager_list = $this->manager_model->getManagerList();
		$area_list = $this->area_model->getAreaList();
		$this->assign('manager_list',$manager_list);
		$this->assign('area_list',$area_list);
		$this->display('area/manager_detail.html');
	}
	/**
	 * 更新区域数据(从地图无忧接口处调)
	 */
	public function update_area(){
		$this->load->model('area_model');
		$this->area_model->updateAreaFlag('all', 0);//将所有区域更新标识设置0更新完毕后删除没有更新的区域数据
		$province_id = 11;//暂时默认省份江苏
		$city_id = 110;//暂时默认城市南京
		$ele = new WMapArea();
		$ele->mid='irXHKOyB3kKasgHp1dKfkA';//后期在数据库中查找地图mid
		$client = new WMapService();
		$result = $client->getDistributeArea($ele);
		if(isset($result['layers'][0]['regions'])){
			$areaInfo = $result['layers'][0]['regions'];
			foreach($areaInfo as $key=>$value){
				$area_key = $value['DTWY_ID'];
				$area_info = $this->area_model->getAreaInfo($area_key);
				$data = array(
						'area_key'=>$value['DTWY_ID'],
						'name'=>$value['标题']
				);
// 				print_r($data);exit;
				if(empty($area_info)){
					$this->area_model->insertAreaInfo($data,$province_id,$city_id);
				}else{
					$this->area_model->updateAreaInfo($data,$province_id,$city_id);
				}
			}
		}
		
		$data = array (
				'success' => TRUE,
				'errno' => 1
		);
		echo json_encode ( $data );
	}
	/**
	 * 设置区域分管经理
	 */
	public function set_area_manager(){
		$errno = 0;
		$msg = '服务器异常,请稍后再试!';
		$area_id = $this->input->post('area_id',true);
		$manager_id = $this->input->post('manager_id',true);
		if($area_id != 0 && $manager_id != 0){
			$this->load->model('area_model');
			$result = $this->area_model->setAreaManager($area_id, $manager_id);
			if($result){
				$errno = 1;
				$msg = "更新成功!";
			}
		}
	    $data = array (
	        'success' => TRUE,
    		'msg' => $msg,
	        'errno' => $errno
	    );
	    echo json_encode ( $data );
	    exit;
	}
	/**
	 * 设置经理分管区域
	 */
	public function set_manager_area(){
		$area_ids = $this->input->post('area_ids',true);
		$manager_id = $this->input->post('manager_id',true);
		if(!empty($area_ids) && $manager_id != 0){
			$this->load->model('area_model');
			$area_for_manager = $this->area_model->getAreaForManager($manager_id);
			$del_ids_temp = array();
			foreach($area_for_manager as $key=>$value){
				$k = array_search($value['id'], $area_ids);
				if($k === false){
					$del_ids_temp[] = $value['id'];
				}else{
					unset($area_ids[$k]);
				}
			}
			if(!empty($del_ids_temp)){
				foreach($del_ids_temp as $value){
					$this->area_model->setAreaManager($value, 0);
				}
			}
			if(!empty($area_ids)){
				foreach($area_ids as $value){
					$result = $this->area_model->setAreaManager($value, $manager_id);
				}
			}
			
		}elseif(empty($area_ids) && $manager_id != 0){
			$this->load->model('area_model');
			$area_for_manager = $this->area_model->getAreaForManager($manager_id);
			foreach($area_for_manager as $value){
				$this->area_model->setAreaManager($value['id'], 0);
			}
		}
		$errno = 1;
		$msg = "更新成功!";
	    $data = array (
	        'success' => TRUE,
    		'msg' => $msg,
	        'errno' => $errno
	    );
	    echo json_encode ( $data );
	    exit;
	}
}