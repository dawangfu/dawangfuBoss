<?php
class Manager extends MY_Controller {
    public $menu = 5;
    public $title = '区域经理管理';
	public function __construct() {
		parent::__construct ();
	}
	public function index() {
		
	}
	/**
	 * 显示配送提成
	 */
	public function manager_fee(){
	    $this->title = '配送提成管理';
	    $this->assign ( 'title', $this->title );
	    $this->assign('current', 3);
	    $this->load->model('manager_model');
	    $managerList = $this->manager_model->getManagerList();
	    $this->assign('managerList',$managerList);
	    $manager_id = $this->input->get('manager',true);;
	    $selectYear = $this->input->get('year',true);
	    $currentYear = date('Y');
	    intval($selectYear) == 0 && $selectYear = $currentYear;
        if($selectYear <= $currentYear){
            $startTime = $selectYear . '-01-01 00:00:00';
            $endTime = $selectYear . '-12-31 23:59:59';
            $this->load->model('manager_model');
            $feeData = $this->manager_model->getManagerFee($manager_id ,$startTime ,$endTime);
            $newFeeData = array();
            for($i=1 ; $i <= 12 ;$i++){
                if(($selectYear == $currentYear) && ($i > date('n'))){
                    break;
                }
                foreach($managerList as $value){
                    if(intval($manager_id) > 0 && $value['id'] != $manager_id){
                        continue;
                    }
                    $newFeeData[$selectYear][$i][$value['id']] = array('realname'=>$value['realname'],'amount'=>0);
                }
                 
            }
            // 	    print_r($feeData);exit;
            foreach($feeData as $value){
                $date = DateTime::createFromFormat('Y-m-d H:i:s', $value['add_time']);
                $year = $date->format('Y');
                $mounth = $date->format('n');
                $newFeeData[$year][$mounth][$value['manager_id']]['amount'] = bcadd($newFeeData[$year][$mounth][$value['manager_id']]['amount'], $value['amount'],2);
            }
            $newFeeData = $newFeeData[$selectYear];
            krsort($newFeeData);
//             print_r($newFeeData);exit;
            $this->assign('newFeeData',$newFeeData);
            $this->assign('selectYear',$selectYear);
            $this->assign('manager_id',$manager_id);
        }
	    $this->display('manager/fee.html');
	}
	/**
	 * 区域经理推荐业绩
	 */
	public function week_fee_for_recommender(){
	    $this->title = '最近三周推荐业绩';
	    $this->assign ( 'title', $this->title );
	    $this->assign('current', 4);
	    $this->load->model('manager_model');
	    $managerList = $this->manager_model->getManagerList();
	    $endTime = date('Y-m-d H:i:s');
	    $startTime = date("Y-m-d H:i:s",mktime(0, 0 , 0,date("m"),date("d")-date("w")+1-28,date("Y")));
	    $this->load->model('member_model');
	    $managerMemberIds = array();
	    $newManagerList = array();
	    foreach($managerList as $value){
	        $managerMemberIds[] = $value['user_id'];
	        $data = array('realname'=>$value['realname'],'user_id'=>$value['user_id'],'amount'=>0);
	        $newManagerList[$value['id']] = $data;
	    }
	    unset($managerList);
	    unset($data);
// 	    print_r($newManagerList);exit;
	    $feeData = $this->member_model->getRecommenderFeeForLastThreeWeek($managerMemberIds , $startTime , $endTime);
	    $feeDataNew = array();
	    foreach($feeData as $value){
	        $week = date('W' , strtotime($value['active_time']));
            if(isset($feeDataNew[$week][$value['user_id']])){
                $feeDataNew[$week][$value['user_id']] = bcadd($feeDataNew[$week][$value['user_id']], $value['amount'],2);
            }else{
                $feeDataNew[$week][$value['user_id']] = $value['amount'];
            }
	        
	    }
// 	    print_r($feeDataNew);exit;
	    //生成四周日历
	    $newfeeData = array();
	    for($i = 0 ; $i < 4 ; $i++){
	        $current = 0;
	        if($i == 0){
	            $current = 1;
	        }
	        $weekNumber = date('W') - $i;
	        $dayNumber = $weekNumber * 7;
	        $weekDayNumber = date("w", mktime(0, 0, 0, 1, $dayNumber, date("Y")));//当前周的第几天
	        $startNumber = $dayNumber - $weekDayNumber;
	        $amountThisWeek = $feeDataNew[$weekNumber];
	        $amountManagerList = $newManagerList;
	       
	        foreach($amountManagerList as $key=>$value){
	            $amountManagerList[$key]['amount'] = $amountThisWeek[$value['user_id']];
	        }
	        $newfeeData[$weekNumber] = array(
	            'range'=>date("Y-m-d", mktime(0, 0, 0, 1, $startNumber + 1, date("Y"))) . '--' . date("Y-m-d", mktime(0, 0, 0, 1, $startNumber + 7, date("Y"))),
	            'current'=>$current,
	            'manager'=>$amountManagerList
	        );
	    
	    }
// 	    print_r($newfeeData);exit;
	    $this->assign('newfeeData',$newfeeData);
	    $this->display('manager/week_fee_for_recommender.html');
	     
	}
}