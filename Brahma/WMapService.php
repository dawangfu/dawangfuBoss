<?php
/**
 * 地图无忧分单接口
 *
 */
class WMapScenic implements MapService{
	
    public $mid = 'irXHKOyB3kKasgHp1dKfkA';    //必须 地图唯一标识,地图 url 中的标识符,比如: 3wFaJb5m4wL9lf70pxeTKw
    
    public $key = 'HqFzcf3NtT5QR6czes0Ev02ojdzvtIBUcJ0';   //必须 x用户令牌
    
    public $location = '';    //当使用 location 经纬度参数时,address 和 city 参数无 效。格式要求,经度,纬度:location=lng,lat【分隔符是 英文逗号】
    
    public $coord_type = 1;  //1 代表百度地图坐标, 2 代表高德腾讯谷歌地图坐标,3 代表 GPS 坐标;默认为 1
    
    public $city = '南京'; //搜索地址所在的城市,不填写默认是全国。建议填写, 增加地址匹配准确度
    
    public $address = ''; //要判断的地址
    
    public function __construct(){}
    
    public function build_http_param(){
        $query_data = array();
    
        foreach (get_class_vars(__CLASS__) as $field => $value) {
            $this->{$field} && $query_data[$field] = $this->{$field};
        }
    
        return http_build_query($query_data);
    }
}
/**
 * 地图无忧分单接口
 *
 */
class WMapArea implements MapService{

	public $mid = 'irXHKOyB3kKasgHp1dKfkA';    //必须 地图唯一标识,地图 url 中的标识符,比如: 3wFaJb5m4wL9lf70pxeTKw

	public $token = 'D-VFOfeTRv7G-_RHS8cg23XvhavcQnyekG4';   //必须 用户令牌

	public function __construct(){}

	public function build_http_param(){
		$query_data = array();

		foreach (get_class_vars(__CLASS__) as $field => $value) {
			$this->{$field} && $query_data[$field] = $this->{$field};
		}

		return http_build_query($query_data);
	}
}
/**
 * <code>
 * $service = new TMapService(); //新建service
 * $ele = new TMapScenic(); //新建参数对象
 * $ele->ip = 3922; //入参
 * $response = $service->ipAddressQuery($ele); //调用方法
 * </code>
 *
 */
class WMapService extends ServiceInterface{
    const DISTRIBUTE_ORDER_QUERY = "/service/v1"; //分单
    
    const DISTRIBUTE_AREA_QUERY = '/service/dawangfu/regions';
    
    public $service_url = 'http://fendan.dituwuyou.com';
    
    public function __construct()
    {
        parent::__construct();
        $this->params = array();
        $this->response = array();
        $this->debug = true;
    }
    /**
     * 调用分单接口
     * @param TMapScenic $ele
     * @return Ambigous <boolean, unknown>
     */
    public function orderDistributeQuery(BMapScenic $ele)
    {
        $this->setCommand(self::DISTRIBUTE_ORDER_QUERY);
        $this->addParam('params', $ele->build_http_param());
    
        unset($ele);
    
        return $this->process();
    }
    public function getDistributeArea(BMapScenic $ele){
    	$this->service_url = 'https://www.dituwuyou.com';
    	$this->setCommand(self::DISTRIBUTE_AREA_QUERY);
    	$this->addParam('params', $ele->build_http_param());
    	
    	unset($ele);
    	
    	return $this->process();
    }
    protected function _is_success($result)
    {
    	if ( ! $result
    			|| $result['status'] != 0)
    	{
    		if (isset($result['message']))
    		{
    			$this->addError($result['message']);
    			return $this->returnError();
    		}
    		else
    		{
    			$this->addError('接口通信失败');
    			return $this->returnError();
    		}
    	}
    
    	//直接返回接口值
    	return $result;
    
    }
    
}