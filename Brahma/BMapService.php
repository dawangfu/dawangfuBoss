<?php
/**
 * 百度地图
 * IP地址查询地址信息
 * @author zhouming
 *
 */
class BMapScenic implements MapService{
    
    public $ip = '';    //IP地址，缺省时会使用请求端的IP 如：ip=202.106.0.20
    
    public $key = '8f0b144f8f292feab6f0bcf3ab173663';   //开发密钥（key）
    
    public $output = 'json';    //返回格式：支持json/jsonp，默认json
    
    public $callback = '';  //jsonp方式回调函数 callback=function1
    
    public function __construct(){}
    
    public function build_http_param(){
        $query_data = array();
    
        foreach (get_class_vars(__CLASS__) as $field => $value) {
            $this->{$field} && $query_data[$field] = $this->{$field};
        }
    
        return http_build_query($query_data);
    }
}

/**
 * 关键词输入提示
 * @author zhouming
 *
 */
class BMapSuggestion implements MapService{
    
    public $query = '';    //输入建议关键字（支持拼音）	keyword=南方
    
    public $region = '南京市';   //所属城市/区域名称或代号，如，仅获取“广州市”范围内的提示内容	region=广州市
    
    public $location = '';   //传入location参数后，返回结果将以距离进行排序 40.047857537164,116.31353434477
      
    public $ak = '8f0b144f8f292feab6f0bcf3ab173663';   //开发者访问密钥，必选。
    
    public $output = 'json';    //返回数据格式，可选json、xml两种
    
    public $sn = '';   //用户的权限签名
    
    public $timestamp = '';  //设置sn后该值必选
    
    public function __construct(){}
    
    public function build_http_param(){
        $query_data = '';
    
        foreach (get_class_vars(__CLASS__) as $field => $value) {
            $this->{$field} && $query_data .= $field . '=' . $this->{$field} . '&';
        }
        
        return $query_data;
    }  
}
/**
 * 坐标转地址<=>地址转换坐标
 * @author zhouming
 *
 */
class BMapLocationToAddress implements MapService{

	public $address = '';  //北京市海淀区上地十街10号
	
	public $city = ''; //地址所在的城市名
	
	public $location = '';    //位置坐标，格式：location=lat<纬度>,lng<经度>	location= 39.984154,116.307490
    
    public $coordtype = '';   //
    
    public $pois = 0;   //是否返回周边POI列表：1.返回；0不返回(默认)	get_poi=1
    
    
    public $ak = '8f0b144f8f292feab6f0bcf3ab173663';   //开发者访问密钥，必选。
    
    public $output = 'json';    //返回数据格式，可选json、xml两种
    
    public $sn = '';   //用户的权限签名
    
    public $callback = '';  //jsonp方式回调函数 callback=function1
    
    public function __construct(){}
    
    public function build_http_param(){
        $query_data = '';
    
        foreach (get_class_vars(__CLASS__) as $field => $value) {
            $this->{$field} && $query_data .= $field . '=' . $this->{$field} . '&';
        }
        
        return $query_data;
    }
}

/**
 * <code>
 * $service = new TMapService(); //新建service
 * $ele = new TMapScenic(); //新建参数对象
 * $ele->ip = 3922; //入参
 * $response = $service->ipAddressQuery($ele); //调用方法
 * </code>
 *
 */
class BMapService extends ServiceInterface{
    
    const AUTO_SUGGESTION_QUERY ="/place/v2/suggestion"; //关键词输入提示
    
    const LOCATION_ADDRESS_QUERY = "/geocoder/v2/"; //坐标查询地址
    
    public $service_url = 'http://api.map.baidu.com';
    
    public function __construct()
    {
        parent::__construct();
        $this->params = array();
        $this->response = array();
        $this->debug = true;
    }
    /**
     * IP查询地址
     * @param TMapScenic $ele
     * @return Ambigous <boolean, unknown>
     */
    public function ipAddressQuery(BMapScenic $ele)
    {
        $this->setCommand(self::IP_ADDRESS_QUERY);
        $this->addParam('params', $ele->build_http_param());
    
        unset($ele);
    
        return $this->process();
    }
    /**
     * 关键词提示
     * @param TMapSuggestion $ele
     * @return Ambigous <boolean, unknown>
     */
    public function autoSuggestionQuery(BMapSuggestion $ele)
    {
        $this->setCommand(self::AUTO_SUGGESTION_QUERY);
        $this->addParam('params', $ele->build_http_param());
        
        unset($ele);
        
        return $this->process();
    }
    /**
     * 坐标查询地址<=>地址查询坐标
     * @param TMapLocationToAddress $ele
     * @return Ambigous <boolean, unknown>
     */
    public function locationAddressQuery(BMapLocationToAddress $ele)
    {
        $this->setCommand(self::LOCATION_ADDRESS_QUERY);
        $this->addParam('params', $ele->build_http_param());

        unset($ele);
        
        return $this->process();        
    }
    
}