<?php
Interface BaseService
{
    public function addParam($name, $value);

    public function setCommand($command);

    public static function addError($error);

    public function returnError();

    public function process();
}


class ServiceInterface implements BaseService
{
    protected $params;

    protected $resquestHost;

    protected static $resquestErr = array();

    protected $response;

    protected $debug = FALSE;

    public $service_url = null;

    public function __construct()
    {
    }

    public static function addError($error)
    {
        self::$resquestErr['Err'][] = $error;
    }

    public static function getCurrentError()
    {
        if (isset(self::$resquestErr['Err']))
        {
            return current(self::$resquestErr['Err']);
        }
    }

    public function returnError()
    {
        if ($this->debug) {
            $error = $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'];
            $error .= "<pre>{$this->service_url}执行command: <i>{$this->params['command']}</i>\r\n\r\n<b><u>参数</u></b>\r\n";
            $error .= print_r(trim($this->params['params']), TRUE);
            $error .= "\r\n<b><u>捕捉到的错误:</u></b>\r\n";
            $error .= '<ul>';
            for($i=0; $i<count(self::$resquestErr['Err']); $i++)
            {
                $error .= '<li>' . self::$resquestErr['Err'][$i] . '</li>';
            }
            $error .= '</ul>';
            $error .= "\r\n<b><u>服务器返回:</u></b>\r\n";
            $error .= print_r(json_decode(trim($this->response), TRUE), TRUE);;
            $error .= '</pre>';
            echo $error;
        }

        return FALSE;
    }

    public function addParam($name, $value)
    {
        $this->params[$name] = $value;
    }

    public function setCommand($command)
    {
        $this->params = array();
        $this->addParam('command', $command);
    }

    public function process()
    {
        if ( ! count($this->params) ) {
            $this->addError("没有设置process()的参数");
            return $this->returnError();
        }

        if ( ! function_exists('curl_init') ) {
            $this->addError('Could not connect to Server though SSL - libcurl is not supported');
            return $this->returnError();
        }

        $this->resquestHost = $this->service_url . $this->params['command'];
        $ch = curl_init();

        $timeout = 5;
        curl_setopt_array(
        $ch,
        array(
	        CURLOPT_URL => $this->resquestHost . '?' . $this->params['params'],
	        CURLOPT_RETURNTRANSFER => TRUE,
	        CURLOPT_TIMEOUT => $timeout,
        	CURLOPT_SSL_VERIFYPEER => FALSE,
        	CURLOPT_SSL_VERIFYHOST => FALSE
        )
        );
        $this->response = curl_exec($ch);
        if (curl_errno($ch)) {
            $errno = curl_errno($ch);
            $errstr = curl_error($ch);
            $this->addError("Could not connect to Server - Error($errno) $errstr");
            return $this->returnError();
        }

        return $this->_is_success(json_decode(trim($this->response), TRUE));
    }
    
    protected function _is_success($result)
    {
        if ( ! $result
            || $result['status'] != 0)
        {
            if (isset($result['message'])) 
            {
                $this->addError($result['message']);
                return $this->returnError();
            } 
            else 
            {
                $this->addError('接口通信失败');
                return $this->returnError();
            }
        }

        //直接返回接口值
        if(isset($result['result']))
        {
            return $result['result'];
            
        }
        elseif(isset($result['data']))
        {
            return $result['data'];
        }
        else 
        {
            return array();
        }    
        
    }
}


