<?php
/**
 * 腾讯地图
 * IP地址查询地址信息
 * @author zhouming
 *
 */
class TMapScenic implements MapService{
    
    public $ip = '';    //IP地址，缺省时会使用请求端的IP 如：ip=202.106.0.20
    
    public $key = 'BJPBZ-OQERX-4VK44-7LPM3-7KEPS-N3FD5';   //开发密钥（key）
    
    public $output = 'json';    //返回格式：支持json/jsonp，默认json
    
    public $callback = '';  //jsonp方式回调函数 callback=function1
    
    public function __construct(){}
    
    public function build_http_param(){
        $query_data = array();
    
        foreach (get_class_vars(__CLASS__) as $field => $value) {
            $this->{$field} && $query_data[$field] = $this->{$field};
        }
    
        return http_build_query($query_data);
    }
}

/**
 * 关键词输入提示
 * @author zhouming
 *
 */
class TMapSuggestion implements MapService{
    
    public $keyword = '';    //用户输入的关键词（希望获取后续提示的关键词）	keyword=南方
    
    public $region = '南京';   //设置城市名，限制关键词所示的地域范围，如，仅获取“广州市”范围内的提示内容	region=广州
    
    public $region_fix = 1;   //取值： 0：[默认]当前城市无结果时，自动扩大范围到全国匹配 1：固定在当前城市	region_fix=1
    
    public $policy = 1;   //检索策略，目前支持：policy=0：默认，常规策略 
                          //policy=1：本策略主要用于收货地址、上门服务地址的填写,提高了小区类、商务楼宇、大学等分类的排序，过滤行政区、道路等分类（如海淀大街、朝阳区等），排序策略引入真实用户对输入提示的点击热度，使之更为符合此类应用场景，体验更为舒适	policy=1
    
    public $filter = '';   //筛选条件：基本语法：columnName<筛选列>=value<列值>；目前支持按POI分类筛选（例：category=分类词），若指定多个分类用英文逗号分隔，最多支持五个分类,支持的分类词可参考：
                           //搜索指定分类 filter=category=公交站 搜索多个分类 filter=category=大学,中学 （注意参数值要进行url编码）

    public $orderby = '';   //排序方式： 基本语法：orderby=<排序条件>[ asc|desc] 注：目前仅支持按指定坐标到搜索结果的距离升序排序（由近到远） 本参数与policy=1为互斥关系
                           //例：按指定坐标到搜索结果的距离升序排序 orderby=distance(39,116)
    
    public $key = 'BJPBZ-OQERX-4VK44-7LPM3-7KEPS-N3FD5';   //开发密钥（key）
    
    public $output = 'json';    //返回格式：支持json/jsonp，默认json
    
    public $callback = '';  //jsonp方式回调函数 callback=function1
    
    public function __construct(){}
    
    public function build_http_param(){
        $query_data = '';
    
        foreach (get_class_vars(__CLASS__) as $field => $value) {
            $this->{$field} && $query_data .= $field . '=' . $this->{$field} . '&';
        }
        
        return $query_data;
    }  
}
/**
 * 坐标转地址
 * @author zhouming
 *
 */
class TMapLocationToAddress implements MapService{

    public $location = '';    //位置坐标，格式：location=lat<纬度>,lng<经度>	location= 39.984154,116.307490
    
    public $coord_type = 5;   //输入的locations的坐标类型 可选值为[1,6]之间的整数，每个数字代表的类型说明：1 GPS坐标 ,2 sogou经纬度 ,3 baidu经纬度 ,4 mapbar经纬度,5 [默认]腾讯、google、高德坐标,6 sogou墨卡托
    
    public $get_poi = 0;   //是否返回周边POI列表：1.返回；0不返回(默认)	get_poi=1
    
    public $key = 'BJPBZ-OQERX-4VK44-7LPM3-7KEPS-N3FD5';   //开发密钥（key）
    
    public $output = 'json';    //返回格式：支持json/jsonp，默认json
    
    public $callback = '';  //jsonp方式回调函数 callback=function1
    
    public function __construct(){}
    
    public function build_http_param(){
        $query_data = '';
    
        foreach (get_class_vars(__CLASS__) as $field => $value) {
            $this->{$field} && $query_data .= $field . '=' . $this->{$field} . '&';
        }
        
        return $query_data;
    }
}

/**
 * <code>
 * $service = new TMapService(); //新建service
 * $ele = new TMapScenic(); //新建参数对象
 * $ele->ip = 3922; //入参
 * $response = $service->ipAddressQuery($ele); //调用方法
 * </code>
 *
 */
class TMapService extends ServiceInterface{
    const IP_ADDRESS_QUERY = "/ws/location/v1/ip"; //ip查询地址
    
    const AUTO_SUGGESTION_QUERY ="/ws/place/v1/suggestion"; //关键词输入提示
    
    const LOCATION_ADDRESS_QUERY = "/ws/geocoder/v1"; //坐标查询地址
    
    public $service_url = 'http://apis.map.qq.com';
    
    public function __construct()
    {
        parent::__construct();
        $this->params = array();
        $this->response = array();
        $this->debug = true;
    }
    /**
     * IP查询地址
     * @param TMapScenic $ele
     * @return Ambigous <boolean, unknown>
     */
    public function ipAddressQuery(TMapScenic $ele)
    {
        $this->setCommand(self::IP_ADDRESS_QUERY);
        $this->addParam('params', $ele->build_http_param());
    
        unset($ele);
    
        return $this->process();
    }
    /**
     * 关键词提示
     * @param TMapSuggestion $ele
     * @return Ambigous <boolean, unknown>
     */
    public function autoSuggestionQuery(TMapSuggestion $ele)
    {
        $this->setCommand(self::AUTO_SUGGESTION_QUERY);
        $this->addParam('params', $ele->build_http_param());
        
        unset($ele);
        
        return $this->process();
    }
    /**
     * 坐标查询地址
     * @param TMapLocationToAddress $ele
     * @return Ambigous <boolean, unknown>
     */
    public function locationAddressQuery(TMapLocationToAddress $ele)
    {
        $this->setCommand(self::LOCATION_ADDRESS_QUERY);
        $this->addParam('params', $ele->build_http_param());

        unset($ele);
        
        return $this->process();        
    }
    
}