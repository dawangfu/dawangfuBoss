<?php
/**
 * 
 * @author zhouming
 *
 */
class Tool {
	
	/**
	 * 手机号码检验
	 */
    static public function checkPhone($phone) {
        
        $result = FALSE;
        $reg = "/^((13[0-9])|(15[0-9])|(18[0-9])|14[0-9]|17[0-9])[0-9]{8,8}$/" ;
        if($phone && preg_match($reg, $phone)){
            $result = TRUE;
        }
        return $result;
    }
    /**
     * 邮箱检验
     */
    static public function checkEmail($email){
        $result = FALSE;
        $reg = "/\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/" ;
        if($email && preg_match($reg, $email)){
            $result = TRUE;
        }
        return $result;        
    }
    
    static public function checkRange($num) {
        $result = 0;
        if($num != 0){
            $result = floor($num/100);            
        }
        return $result;
    }
    /**
     * 获取客户端ip
     * @return unknown
     */
    static public function getClientIp(){
    	$realip = $_SERVER["REMOTE_ADDR"];
    	return  $realip;
    }
    /**
     * 生成cachekey
     * @param unknown $string
     */
    static public function getCacheKey($string = ''){
    	
    	if($string != ''){
    		$key = md5($string);
    	}else{
    		$backtrace = debug_backtrace();
    		$key = md5($backtrace[0]['file'] . $backtrace[0]['file'] . $backtrace[1]['class'] . $backtrace[1]['function']);
    	}
    	return $key;
    }
    /**
     * 检验5分钟内发送次数
     * @param unknown $phone
     */
    static public function isAvailablePhoneSendSms($phone){
    	$result = false;
    	$key = self::getCacheKey($phone);
    	$ci = &get_instance();
    	$ci->load->driver('cache' , array('adapter' => 'redis'));
    	$phone_send_sms_num = $ci->cache->redis->get($key);
    	if(empty($phone_send_sms_num) || $phone_send_sms_num['num'] < 5){
    		$result = true;
    	}
    	
    	return $result;
    }
    /**
     * 检验单个ip发送次数
     * @param unknown $ip
     */
    static public function isAvailableIPSendSms($ip){
    	$result = false;
    	$key = self::getCacheKey($ip);
    	$ci = &get_instance();
    	$ci->load->driver('cache' , array('adapter' => 'redis'));
    	$ip_send_sms_num = $ci->cache->redis->get($key);
    	if(empty($ip_send_sms_num) || intval($ip_send_sms_num['num']) < 20){
    		$result = true;
    	}
    	return $result;
    }
    /**
     * 发送手机验证码检验代码
     * @param unknown $phone
     * @return number
     */
    static public function sendMobileCode($phone){
    	$result = 0;
    	$ip = self::getClientIp();
    	if(!self::checkPhone($phone)){
    		$result = 2;  //手机号码格式不正确
    	}elseif(!self::isAvailablePhoneSendSms($phone)){
    		$result = 3;//手机号码5分钟内达到最大发送次数
    	}elseif(!self::isAvailableIPSendSms($ip)){
    		$result = 4;//单个ip地址达到当日最大发送量
    	}else{
    		$ci = &get_instance();
    		$ci->load->driver('cache' , array('adapter' => 'redis'));
    		$code = rand(100000,999999);
    		file_put_contents(FCPATH . '1.txt', $code);
    		$key_phone_code = self::getCacheKey($phone.$code);
    		$errno = self::sendCode($phone , $code);
    		$ci->cache->redis->save($key_phone_code , $code , 900);
    		$key_phone_send_num = self::getCacheKey($phone); //单个手机号码发送短信的缓存 key
    		$key_phone_send_num_time = 300; //单个手机号码发送短信5分钟内不超过5条
    		$phone_send_num = $ci->cache->redis->get($key_phone_send_num);
    		$current_time = time();
    		if(empty($phone_send_num)){
    			$phone_send_num = array('timestamp'=>time() , 'num'=>1);
    		}else{
    			$key_phone_send_num_time = $key_phone_send_num_time - ($current_time - $phone_send_num['timestamp']);
    			$phone_send_num = array('timestamp'=>$current_time , 'num'=>$phone_send_num['num'] + 1);
    		}
    		$ci->cache->redis->save($key_phone_send_num  , $phone_send_num  ,$key_phone_send_num_time);
    		$key_ip_send_num = self::getCacheKey($ip);
    		$key_ip_send_num_time = 86400 ;//单个ip发送短信间隔1天
    		$ip_send_num = $ci->cache->redis->get($key_ip_send_num);
    		if(empty($ip_send_num)){
    			$ip_send_num = array('timestamp'=>time() , 'num'=>1);
    		}else{
    			$key_ip_send_num_time = $key_ip_send_num_time - ($current_time - $ip_send_num['timestamp']);
    			$ip_send_num = array('timestamp'=>$current_time , 'num'=>$ip_send_num['num'] + 1);
    		}
    		$ci->cache->redis->save($key_ip_send_num  , $ip_send_num  ,$key_ip_send_num_time);
    		$result = 1;
    		 
    	}
    	return $result;
    }
    
    static public function checkSmsCode($phone,$smscode){
    	$result = false;
    	$ci = &get_instance();
    	$ci->load->driver('cache' , array('adapter' => 'redis'));
    	$key_phone_code = self::getCacheKey($phone.$smscode);
    	$phone_send_code = $ci->cache->redis->get($key_phone_code);
    	if(!empty($phone_send_code) && $phone_send_code == $smscode){
    		$result = true;
    	}
    	return $result;
    }
    /**
     * 发送手机验证码
     * @param unknown $phone
     * @param unknown $smsCode
     * @return number
     */
    static public function sendCode($phone , $smsCode){
    	$errno = 0;
    	$Client = new HttpClient("http://112.74.76.186/service/httpService/httpInterface.do",8030);
    	$params = array(
    	    'username'=>"JSM41415",
    	    'password'=>"0z4x2nt2",
    	    'veryCode'=>'trnaimmu09fl',
    	    'mobile'=>$phone,
    	    'content'=>"@1@=".$smsCode,
    	    'tempid'=>"JSM41415-0001",
    	    'method'=>"sendMsg",
    	    'msgtype'=>2,
    	    'code'=>'utf8'
    	);
    	$url = "http://112.74.76.186:8030/service/httpService/httpInterface.do";
    	$pageContents = $Client->quickPost($url, $params);
//     	if($pageContents){
//     		$errno = 1;
//     	}
    	$errno = 1;
    	return $errno;
    }
}