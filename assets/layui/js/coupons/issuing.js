layui.use(['treeSelector', 'form', 'upload','laydate','layedit','layer'], function(){
	var $ = layui.jquery,
	form = layui.form(),
	laytpl = layui.laytpl,
	layedit = layui.layedit,
	layer = layui.layer,
	laydate = layui.laydate;;
	;
	form.on('radio(use-range)', function(data){
	  if(data.value == 2){
	  	removeProductSelect();
		showCategorySelect();
	  }else if(data.value == 3){
	  	removeCategorySelect();
	  	showProductSelect();
	  }else{
	  	removeCategorySelect();
	  	removeProductSelect()
	  }
	});
	function removeCategorySelect(){
		$("div.category-select").hide();
	  	$("input[name=categroy_id]").val(0);
	  	$("div.category-selected-list").html("");
	  	$("#category-tree").html("");
	}
	function showCategorySelect(){
		layui.treeSelector({
	   		elem:'#category-tree',
	   		type:'checkbox'
	   	});		  	
	  	$("div.category-select").show();	
	}
	function showProductSelect(){
		$("div.product-select").show();	
	}
	function removeProductSelect(){
		$("div.product-select").hide();
		$("input[name=product_id]").val(0);
	  	$("div.product-selected-list").html("");
	}
	$(".add-product").on("click",function(){
		layer.open({
		  type: 2,
		  area: ['1200px', '530px'],
		  fixed: false, //不固定
		  content: '/product/productListMini/'
		});		
	});
	var editIndex = layedit.build('rule_content_editor',{
		tool: [
			  'strong' //加粗
			  ,'italic' //斜体
			  ,'underline' //下划线
			  ,'del' //删除线
			  ,'|' //分割线
			  ,'left' //左对齐
			  ,'center' //居中对齐
			  ,'right' //右对齐
			  ,'link' //超链接
			  ,'unlink' //清除链接
			]
//		uploadImage:{url:'/product/uploadEditImage/'}
	}); //建立编辑器	
	var start = {
		min: laydate.now()
		,max: '2099-06-16 23:59:59'
		,istoday: true
		,choose: function(datas){
			end.min = datas; //开始日选好后，重置结束日的最小日期
			end.start = datas //将结束日的初始值设定为开始日
		}
	};
  
	var end = {
		min: laydate.now()
		,max: '2099-06-16 23:59:59'
		,istoday: true
		,choose: function(datas){
		 	start.max = datas; //结束日选好后，重置开始日的最大日期
		}
	};
  
	document.getElementById('LAY_demorange_s').onclick = function(){
		start.elem = this;
		laydate(start);
	};
	document.getElementById('LAY_demorange_e').onclick = function(){
		end.elem = this
		laydate(end);
	};
	layui.upload({
	    url: '/coupons/uploadCouponIcon/' //上传接口
	    ,ext: 'jpg|png|gif|jpeg'
	    ,success: function(res){ //上传成功后的回调
	      var html = '<img src="' + res.data.img_url + '" width="50" height="50"/>'
			$("#thumbnails").append(html);
			$("#coupon_icon").val(res.data.img_path);
	    }
	});
	form.verify({
		title: function(value){
			if(value.length <= 0){
				return '请填写优惠券标题';
			}
		}
		,categroy:function(value){
			var useRange = $("input[name=use-range]:checked").val();
			if(useRange == 2 && value == 0){
				return '请选择分类';
			}
		}
		,product:function(value){
			var useRange = $("input[name=use-range]:checked").val();
			if(useRange == 3 && value == 0){
				return '请选择产品';
			}
		}
		,facevalue:function(value){
			value = parseFloat(value);
			if(value <= 0 || isNaN(value)){
				return '请填写合适的面值';
			}
		}
		,minimumcharge:function(value){
			value = parseFloat(value);
			if(value < 0 || isNaN(value)){
				return '请填写合适的最低消费金额';
			}
		}
		,exchangeableamount:function(value){
			value = parseFloat(value);
			if(value < 0 || isNaN(value)){
				return '请填写合适的兑换金额';
			}
		}
		,issuenumber:function(value){
			value = parseInt(value);
			if(value < 0 || isNaN(value)){
				return '请填写合适的发放数量';
			}
		}
		,content: function(value){
		  	layedit.sync(editIndex);
		}
		,startdate:function(value){
			if(value == ''){
				return '请选择开始日期';
			}
		}
		,enddate:function(value){
			if(value == ''){
				return '请选择结束日期';
			}
		}
		
	});
	form.on('submit(save)', function(data){
//		  	console.log(data.elem) //被执行事件的元素DOM对象，一般为button对象
//		  	console.log(data.form) //被执行提交的form对象，一般在存在form标签时才会返回
//		  	console.log(data.field) //当前容器的全部表单字段，名值对形式：{name: value}
//		  	return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
	});	
});

