/**
 * Created by fish on 2017/7/20.
 */
layui.use(['treeSelector', 'form','layer','tools'], function(){
    var $ = layui.jquery,
        form = layui.form(),
        layer = layui.layer,
        layerCategoryIndex;
    layui.treeSelector({
        elem:'#category-tree',
        type:'radio',
        categoryType:1,
        callback:function(id){
            var spanHtml = $('.add-category>span').html();
            $('.category-select>span').html(spanHtml);
            $('input[name="search[categroy_select_id]"]').val(id);
            layer.close(layerCategoryIndex);
        }
    });
    $(".category-select").click(function(){
        $(".add-category").click();
        layerCategoryIndex = layer.open({
            type: 1,
            title: false,
            closeBtn: 1,
            area: '516px',
            shadeClose: true,
            content: $('#category-content')
        });
    });
    $(".search-button").click(function(){
        $("form.search-from").submit();
    });
});

function setRealStoreNum(gid, price, obj){
    layui.use(['jquery', 'layer'], function($, layer){
        var oldNum = parseInt($(obj).parent().prev('td').text());
        var operate = $(obj).parent().next('td').find('select').val();
        var num = $(obj).val();
        if(num == 0 || num == '')
            return false;
        var type = 'plus';
        if(num[0] === '-'){
            type = 'minus';
            num = num.substring(1);
            if(oldNum < num) {
                layer.msg('格式错误')
                return false;
            }
        }
        num = parseInt(num);
        $.ajax({
            data: {gid: gid, price: price, num: num, type: type, operate: operate},
            dataType: 'json',
            type: 'get',
            url: '/product/setRealStoreNum',
            success: function(json){
                if(json.code == 0){
                    $(obj).val(0);
                    var result = (type == 'plus')? oldNum + num: oldNum - num;
                    $(obj).parent().prev('td').html(result);
                    layer.msg('OK');
                }
            }
        })

    });

}