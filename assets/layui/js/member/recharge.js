layui.use(['laypage', 'layer','laytpl'], function() {
    var laypage = layui.laypage,
        layer = layui.layer,
        laytpl = layui.laytpl,
        $ = layui.jquery;


	turnToPage(1);
    //翻页
    function turnToPage(page){
        var load = '';
        var data = {page: page , limit: 20}
        $.ajax({
            data: data,
            dataType: 'json',
            type: 'get',
            url: '/api/member/recharge_list',
            error: function(){
                layer.close(load);
                layer.msg('接口错误');
            },
            beforeSend: function(){
                load = layer.load(1, {
                    shade: [0.7,'#000']
                });
            },
            success:function(json){
	            	var getTpl = couponListTemplate.innerHTML;
				laytpl(getTpl).render(json.data, function(html){
				  	$('#couponList').html(html);
				});
			    //分页
			    var pages = Math.ceil(json.data.totalNum /data.limit);
			    laypage({
			        cont: 'page',
			        curr: data.page,
			        pages: pages,//data.total_num,
			        groups: 5, //连续显示分页数
			        jump: function(obj,first){
			        		if(first == undefined){
		        				turnToPage(obj.curr);
			        		}
			        }
			    });
                layer.close(load);
            }
        });
    }

});