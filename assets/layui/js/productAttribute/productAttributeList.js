layui.use(['laypage', 'layer'], function() {
    var laypage = layui.laypage,
        layer = layui.layer,
        $ = layui.jquery;

    //删除
    $(document).on('click', '.del-attribute', function(){
        delAttribute($(this));
    });

    //分页
    laypage({
        cont: 'page',
        pages: $('#page').attr('data-page'),//data.total_num,
        groups: 5, //连续显示分页数
        jump: function(obj){
            turnToPage(obj.curr);
        }
    });

    //翻页
    function turnToPage(page){
        var load = '';
        $.ajax({
            data: {page: page},
            dataType: 'json',
            type: 'get',
            url: '/api/productAttribute/all_attribue',
            error: function(){
                layer.close(load);
                layer.msg('接口错误');
            },
            beforeSend: function(){
                load = layer.load(1, {
                    shade: [0.7,'#000']
                });
            },
            success:function(json){
                var data = json.data;
                var list = data.list;
                var str = '';
                var attributeType = '';
                var formType = '';
                for(var i = 0; i < list.length; i++){
                    switch(list[i].attribute_type){
                        case '1':
                            attributeType = '普通属性';
                            break;
                        case '2':
                            attributeType = '检索属性';
                            break;
                        case '3':
                            attributeType = '规格属性';
                            break;
                    }
                    switch(list[i].form_type){
                        case 'select':
                            formType = '下拉';
                            break;
                        case 'radio':
                            formType = '单选';
                            break;
                        case 'checkbox':
                            formType = '多选';
                            break;
                        case 'text':
                            formType = '文本';
                            break;

                    }
                    str += '<tr>' +
                        '<td>'+ list[i].attribute_name +'</td>' +
                        '<td>'+ list[i].category_name +'</td>' +
                        '<td>'+ attributeType +'</td>' +
                        '<td>'+ formType +'</td>' +
                        '<td>' +
                        '<a  class="layui-btn layui-btn-mini layui-btn-normal" href="/productAttribute/edit?attribute_id='+ list[i].id +'">编辑</a>' +
                        '<button type="button" class="layui-btn layui-btn-mini layui-btn-danger del-attribute" data-id="'+ list[i].id +'" >删除</button>' +
                        '</td>' +
                        '</tr>';
                }
                $('#attribute-list').html(str);
                layer.close(load);
            }
        });
    }

    /**
     * 删除属性值
     * @param id 属性值ID
     */
    function delAttribute(obj){
        var load = '';
        var id = $(obj).attr('data-id');
        var tr = $(obj).parents('tr');
        $.ajax({
            data:{id: id},
            dataType: 'json',
            type: 'post',
            url: '/api/productAttribute/product_attribute_del',
            error: function(){
                layer.close(load);
                layer.msg('接口错误');
            },
            beforeSend: function(){
                load = layer.load(1, {
                    shade: [0.7,'#000']
                });
            },
            success: function(json){
                if(json.success){
                    tr.remove();
                }
                layer.msg(json.errorMessage);
                layer.close(load);
            }
        });

    }
});