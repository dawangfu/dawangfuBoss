layui.use(['form','layer'], function(){
    var $ = layui.jquery,
        form = layui.form(),
        layer = layui.layer;
    var loader = null;

    //秒杀开关
    form.on('switch(seckill)', function(data){
        var that = data.elem;
        var isSeckill = that.checked;
        var gid = $(that).attr('data-gid');
        var pid = $(that).attr('data-pid');
        setSkuSeckill(pid, gid, isSeckill ? 1 : 0);
    });

    $('input.seckill-input').on('change', function(){
        var type = $(this).hasClass('seckill-price')? 1: 0;
        var pid = $(this).attr('data-pid');
        var gid = $(this).attr('data-gid');
        var num = $(this).val() || 0;
        var origin = $(this).attr('data-origin') || 0;//修改库存时候所用
        updateSeckill(pid, gid, num, type, origin);
    });

    $('#pre-seckill').on('click', function(){
        var preSeckill = $('.pre-seckill-item input:checked');
        if(preSeckill.length == 0){
            return false;
        }
        seckillIds = [];
        for(var i = 0; i < preSeckill.length; i++){
            seckillIds[i] = {
                id: $(preSeckill[i]).attr('data-id'),
                pid: $(preSeckill[i]).attr('data-pid'),
                gid: $(preSeckill[i]).attr('data-gid'),
            };
        }
        setPreSeckill(seckillIds);
    });
    $('.offline-button').on('click', function(){
        setSeckillOffline();
    });


    /**
     * 修改秒杀参数
     * num 库存||价格
     */
    function updateSeckill(pid, gid, num, type, origin){
        var resUrl = (type)? 'updateSeckillPrice': 'updateSeckillStore';
        $.ajax({
            data: {gid: gid, pid: pid, num: num, origin:origin},
            dataType: 'json',
            type: 'post',
            url: '/seckill/' + resUrl,
            beforeSend: function(){
                loader = layer.msg('加载中', {
                    icon: 16,
                    shade: 0.01
                });
            },
            success: function(e){
                layer.close(loader);
                if(!type){
                    if(!e.success){
                        layer.msg('库存设置不能大于商品实际库存');
                    }
                }
                document.location.reload();
            }
        });
    }

    /**
     * 设置秒杀状态
     */
    function setSkuSeckill(pid, gid, isSeckill){
        $.ajax({
            data: {gid: gid, pid: pid, isSeckill: isSeckill},
            dataType: 'json',
            type: 'post',
            // async: false,
            url: '/seckill/setSkuSeckill',
            beforeSend: function(){
                loader = layer.msg('加载中', {
                    icon: 16,
                    shade: 0.01
                });
            },
            success: function(){
                layer.close(loader);
                /*if (isSeckill == 1) {
                    $('.sku_' + gid).find('input.seckill-input').prop('disabled', '');
                } else {
                    $('.sku_' + gid).find('input.seckill-input').prop('disabled', 'disabled');
                }*/
                document.location.reload();
            }
        });
    }

    /**
     * 预秒杀一键上线
     * @param id json
     */
    function setPreSeckill(ids){
        $.ajax({
            data: {ids: ids},
            dataType: 'json',
            type: 'post',
            url: '/seckill/setSkuPreSeckill',
            beforeSend: function(){
                loader = layer.msg('加载中', {
                    icon: 16,
                    shade: 0.01
                });
            },
            success: function(){
                layer.close(loader);
                document.location.reload();
            }
        });
    }

    /**
     * 秒杀一键下线
     */
    function setSeckillOffline(){
        $.ajax({
            dataType: 'json',
            type: 'post',
            url: '/seckill/setSeckillOffLine',
            success: function(){
                document.location.reload();
            }
        });
    }
});