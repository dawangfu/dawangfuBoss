layui.use(['treeSelector', 'form', 'laytpl','layer','tools'], function(){
        var $ = layui.jquery,
        form = layui.form(),
        laytpl = layui.laytpl,
        layedit = layui.layedit,
        layer = layui.layer,
        layerCategoryIndex
       	;
       	layui.treeSelector({
       		elem:'#category-tree',
       		type:'radio',
       		categoryType:1,
       		callback:function(id){
       			var spanHtml = $('.add-category>span').html();
       			$('.category-select>span').html(spanHtml);
       			$('input[name="search[categroy_select_id]"]').val(id);
       			layer.close(layerCategoryIndex);
       		}
       	});
       	$(".category-select").click(function(){
       		$(".add-category").click();
			 layerCategoryIndex = layer.open({
			  type: 1,
			  title: false,
			  closeBtn: 1,
			  area: '516px',
			  shadeClose: true,
			  content: $('#category-content')
			});      		
       	});
       	$(".search-button").click(function(){
       		$("form.search-from").submit();
       	});
		$('.save-button').on('click', function(){
			var index = parent.layer.getFrameIndex(window.name); //获取窗口索引
			var $idObj = $('input:checkbox[name="id[]"]:checked'),gid=[];
			var productNameHtml = "";
			if($idObj.length > 0)
			{
				$idObj.each(function(){
					gid.push($(this).val());
					var productName = $(this).parent().siblings("td.product-name").text();
					productNameHtml += "<span dataId='" + $(this).val() + "'> " + productName +  " </span>"
				});
				
			}
			else
			{
				alert('请选商品!');
				return false;
			}
			parent.document.getElementById("productId").value = gid.join(",");
			parent.document.getElementById("productSelectedList").innerHTML = productNameHtml;
		    parent.layer.close(index);
		});
		
});
