/**
 * Created by fish on 16/10/26.
 * 保存 新增&编辑 分类
 */
layui.use(['jquery', 'form', 'categoryList', 'upload'], function($){
    var form = layui.form();
    var tools = layui.tools;

    //首页显示
    var index_show = $('input[name="index_show"]').val();
    form.on('switch(index_show)', function(data){
        index_show = data.elem.checked?1:0;
    });


    $(document).on('click', '.confirm-checkbox2', function(){
        var checkboxs = '';
        $('#category-tree2 i.layui-tree-check.checked').each(function(){
            var id = $(this).next('a').attr('data-id');
            checkboxs += id + ',';
        });
        checkboxs = tools.removeLastStr(checkboxs);
        $('.category-list-2').addClass('layui-hide');
        getProductList(checkboxs, false);
    });

    //自定义分类关联产品
    $('.add-product').on('click', function(){
        $('.category-list-2').removeClass('layui-hide');
        if(!$('.product-list').hasClass('layui-hide')){
            $('.product-list').addClass('layui-hide')
        }
        layui.categoryList({
            type: 1,
            elem: '#category-tree2',
            renderType: 'checkbox'
        });
    });

    var iconLoad = '';
    layui.upload({
        url: '/index/uploadCategoryImage/', //上传接口
        ext: 'jpg|png|gif|jpeg',
        before: function(){
            iconLoad = layer.load(1, {
                shade: [0.1,'#fff']
            });
        },
        success: function(res){ //上传成功后的回调
            uploadPicCallback(res.data);
            layer.close(iconLoad);
            layer.msg('上传成功');
        }
    });

    //选择产品
    $(document).on('click', '.category-product-list li', function(){
       if($(this).hasClass('on')){
           $(this).find('i').html('&#xe626;');
           $(this).removeClass('on');
       }else{
           $(this).find('i').html('&#xe627;');
           $(this).addClass('on');
       }
    });

    //确认所选产品
    $(document).on('click', '.confirm-select-product', function(){
        var pids = '';
        $(this).prev('.category-product-list').find('li').each(function(){
            if($(this).hasClass('on')) {
                pids += $(this).attr('data-id') + ',';
            }
        });
        $('input[name="product_ids"]').val(tools.removeLastStr(pids));
        if($(this).parents('div.layui-layer').length > 0){
            $(this).parents('div.layui-layer').find('a.layui-layer-close').click();
        }
        layer.msg('OK');
    });

    //查看已选中产品
    $('.selected-product').on('click', function(){
        getProductList($('input[name="id"]').val(), true);
    });

    //添加&编辑 保存数据
    $('#save').on('click', function(){
        var category_name = $('input[name="category_name"]').val();
        if(category_name == ''){
            layer.msg('分类名称不能为空');
            return false;
        }
        var category_type = $('input[name="category_type"]').val();
        var category_id = $('input[name="categroy_id"]').val();
        var product_ids = $('input[name="product_ids"]').val();
        var category_icon = $('input[name="category-icon"]').val();
        var f_order = $('input[name="f_order"]').val();
        if(f_order == ''){
            layer.msg('排序不能为空');
            return false;
        }
        var title = $('input[name="title"]').val();
        var keywords = $('input[name="keywords"]').val();
        var description = $('textarea[name="description"]').val();
        var id = $('input[name="id"]').val();
        var data = {
            category_name: category_name,
            category_type: category_type,
            parent_id: category_id,
            product_ids: product_ids,
            category_icon: category_icon,
            index_show: index_show,
            f_order: f_order,
            title: title,
            keywords: keywords,
            description:  description
        }
        if(id > 0){
            data.id = id;
        }
        $.ajax({
            data: data,
            dataType: 'json',
            type: 'get',
            url: '/api/category/category_item_add/',
            error: function(json){
                layer.msg(json.errorMessage);
            },
            success: function(json){
                if(json.success){
                    if(id > 0){
                        layer.msg('编辑成功');
                    }else{
                        layer.msg('添加成功');
                    }
                    setTimeout(function(){
                        document.location.reload();
                    },1000);
                }else{
                    layer.msg(json.errorMessage);
                }
            }
        });
    });

    //获取所属分类下产品列表
    function getProductList(categoryIds, view){
        var load = '';//loading
        $.ajax({
            data: {cids: categoryIds},
            dataType: 'json',
            type: 'get',
            url: '/api/category/product_list_from_category/',
            beforeSend: function(){
                load = layer.load(1, {
                    shade: [0.7,'#000']
                });
            },
            error: function(){
                layer.close(load);
                layer.msg('分类未选中');
            },
            success: function(json){
                var data = json.data;
                var list = data.list;
                if(json.success){
                    var str = '';
                    for(var i = 0; i < data.total_num; i++){
                        var on = '';
                        var onStr = '&#xe626;';
                        if(view){
                            on = ' on';
                            onStr = '&#xe627;';
                        }
                        str += '<li class="category-product-list-li'+ on +'" data-id="'+ list[i].id +'">' +
                                    '<i class="layui-icon">'+ onStr +'</i>&nbsp;' + list[i].product_name + '</li>';
                    }
                    if(!view) {
                        $('.category-product-list').html(str);
                        if ($('.product-list').hasClass('layui-hide')) {
                            $('.product-list').removeClass('layui-hide')
                        }
                    }else{
                        str = '<div class="product-list-selected site-block">' +
                            '<ul class="category-product-list">' + str + '</ul>' +
                            '<button type="button" class="layui-btn layui-btn-warm confirm-select-product" style="margin-top:10px;">确认</button>' +
                            '</div>';
                        layer.open({
                            type: 1,
                            title: '已选产品',
                            closeBtn: 1,
                            shadeClose: true,
                            skin: 'yourclass',
                            content: str
                        });
                    }
                }
                layer.close(load);
            }
        });
    }

    //图片上传回调
    function uploadPicCallback(data){
        console.log(data);
        $('.category-icon').val(data.img_path);
        $('#category_icon').attr('src',data.img_url);
    }
});