/**
 * Created by fish on 16/10/26.
 * 分类 删除&置顶
 */
layui.use('tree', function() {
    $ = layui.jquery;
    $.ajax({
        dataType: 'json',
        type: 'get',
        url: '/api/category/category_list/',
        error: function(){
            layer.msg('系统错误');
        },
        success: function(json){
            var list = json.data.list;
            if(list == null) {
                $('#category-tree').html('暂无记录');
                return false;
            }
            layui.tree({
                elem: '#category-tree',
                nodes: list,
                opera: 1,
            });
        }
    });


    $(document).on('click', '.category_edit', function(e){
        layui.stope(e);
        var _this = $(this);
        var type = _this.attr('data-type');
        var target_id = _this.parent('li').find('a').attr('data-id');
        switch(type){
            case 'show':
                var show_status = _this.attr('data-show');
                showIndex(target_id, show_status, _this);
                break;
            case 'del':
                delCategory(target_id, _this);
                break;
            case 'edit':
                document.location.href = '/index/category_edit/?category_id=' + target_id;
                break;
            case 'add'://自定义商品分类
                addProductForCategory();
                break;
        }
    });
});

/**
 *  首页显示
 */
function showIndex(id, status, obj){
    if(id == '')
        return false;
    var status = parseInt(status)?0:1;
    $.ajax({
        data: {id:id, status: status},
        dataType: 'json',
        type: 'get',
        url: '/api/category/category_item_show/',
        error: function(){
            layer.msg('系统错误');
        },
        success: function(json){
            var list = json.data.list;
            if(json.success){
                obj.attr('data-show', list.index_show);
                obj.attr('class', 'layui-btn layui-btn-mini category_edit');
                if(list.index_show == '1'){
                    obj.addClass('layui-btn-normal').html('显　示');
                }else{
                    obj.addClass('layui-btn-primary').html('不显示');
                }
                layer.msg(json.errorMessage);
            }else{
                layer.msg(json.errorMessage);
            }
        }
    });
}
/**
 *  删除分类
 */
function delCategory(id, obj){
    if(id == '')
        return false;
    $.ajax({
        data: {id:id},
        dataType: 'json',
        type: 'get',
        url: '/api/category/category_item_del/',
        error: function(){
            layer.msg('系统错误');
        },
        success: function(json){
            var list = json.data.list;
            if(json.success){
                obj.parents('li').remove();
            }
            if($('#category-tree').find('li').length == 0){
                $('#category-tree').parent('div.site-block').html('暂无记录');
            }
            layer.msg(json.errorMessage);
        }
    });
}
