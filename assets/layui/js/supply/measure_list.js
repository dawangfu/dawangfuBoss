/**
 * Created by fish on 2017/6/23.
 */
layui.use(['treeSelector', 'form', 'laytpl','layer','tools'], function(){
    var $ = layui.jquery,
        form = layui.form(),
        laytpl = layui.laytpl,
        layer = layui.layer;
    var cid = '';
    layui.treeSelector({
        elem:'#category-tree',
        type:'checkbox',
        categoryType:1,
        btnEvent: function(categoryIDs){
            cid = categoryIDs;
            $.ajax({
                data: {cid: categoryIDs, type: 'fetch'},
                dataType: 'json',
                type: 'get',
                url: '/api/Supply/product_measure',
                beforeSend: function(){
                    loading = layer.load(2, {
                        shade: [0.1,'#fff']
                    });
                },
                success: function(data){
                    var getTpl = document.getElementById('product-list').innerHTML;
                    var data = data.data.list;
                    laytpl(getTpl).render(data, function(html){
                        document.getElementById('supply-list-block').innerHTML = html;
                        layer.close(loading);
                    });
                }
            })
        }
    });

    $('.measure-data-export').on('click', function(){
        console.log(cid);
        if(cid == '')
            return false;

        var urlParam = $.param({cid: cid, type: 'export'});
        window.location.href = '/api/Supply/product_measure?' + urlParam;
    });
});