/**
 * Created by fish on 2017/6/14.
 */
layui.use(['form','laydate','laytpl','layer','tools'], function(){
    var $ = layui.jquery;
    var form = layui.form();
    var laytpl = layui.laytpl;
    var tools = layui.tools;

    var managerID = [];
    var begainDate = '';
    var endDate = '';
    var deliverDate = '';
    var status = 0;
    var type = 'fetch';

    //负责人
    form.on('checkbox(manager)', function(data){
        var mid = data.value;
        if(data.elem.checked){
            managerID.push(mid);
        }else{
            tools.removeArrayValue(managerID, mid);
        }
    });
    form.on('checkbox(manager-all)', function(data){
        var mid = data.value;
        if(data.elem.checked){
            managerID = [];
            managerID.push(mid);
            $('input[name=manager]:checkbox').attr('checked', false).attr('disabled', true);
        }else{
            tools.removeArrayValue(managerID, mid);
            $('input[name=manager]:checkbox').attr('disabled', false);
        }
        form.render('checkbox');
    });
    //状态
    form.on('select(status)', function(data){
        status = data.value;
    });

    /* 下单日期&配送日期 begain */
    document.getElementById('supply-begain').onclick = function(){
        laydate({
            elem: this,
            istime: false,
            format: 'YYYY-MM-DD',
            choose: function(dates){
                begainDate = dates;
            }
        });
    }
    document.getElementById('supply-end').onclick = function(){
        laydate({
            elem: this,
            istime: false,
            format: 'YYYY-MM-DD',
            choose: function(dates){
                endDate = dates;
            }
        })
    }
    document.getElementById('deliver-date').onclick = function(){
        laydate({
            elem: this,
            istime: false,
            format: 'YYYY-MM-DD',
            choose: function(dates){
                deliverDate = dates;
            }
        })
    }
    /* 下单日期&配送日期 end */

    //确认
    $('.supply-option-confirm').on('click', function(){
        checkSupplyCondition();
        var loading = '';
        $.ajax({
            data: {mid: managerID, begainDate: begainDate, endDate: endDate, deliverDate: deliverDate, type: type, status: status},
            dataType: 'json',
            type: 'get',
            url: '/api/Supply/supply_list',
            beforeSend: function(){
                loading = layer.load(2, {
                    shade: [0.1,'#fff']
                });
            },
            success: function(data){
                var getTpl = document.getElementById('supply-list').innerHTML;
                var data = data.data.list;
                laytpl(getTpl).render(data, function(html){
                    document.getElementById('supply-list-block').innerHTML = html;
                    layer.close(loading);
                });
            }
        });
    });

    //导出
    $('.supply-data-export').on('click', function(){
        checkSupplyCondition();
        var urlParam = $.param({mid: managerID, begainDate: begainDate, endDate: endDate, deliverDate: deliverDate, type: 'export', status: status});
        window.location.href = '/api/Supply/supply_list?' + urlParam;
    });

    //验证筛选条件
    function checkSupplyCondition(){
        if(managerID.length == 0){
            layer.msg('负责人不能为空');
            return false;
        }
        if(begainDate == ''){
            layer.msg('开始日期不能为空');
            return false;
        }
        if(endDate == ''){
            layer.msg('结束日期不能为空');
            return false;
        }
    }
});