layui.define(['layer','tools'], function(exports){
    var $ = layui.jquery;
    var tools = layui.tools;

    categoryList = function(options){
        this.options = options;
    }

    categoryList.prototype.init = function(){
        $('.confirm-checkbox').unbind('click');
        var _this = this;
        $.ajax({
            data: {type:_this.options.type},
            dataType: 'json',
            type: 'get',
            url: '/api/category/category_list/',
            error: function(){
                layer.msg('系统错误');
            },
            success: function(json){
                var list = json.data.list;
                var elem = _this.options.elem || '#category-tree';
                $(elem).empty();
                if(!tools.isEmpty(list) && list.length > 0) {
                    layui.tree({
                        elem: elem,
                        check: _this.options.renderType || 'radio',
                        nodes: list,
                        btnEvent: 1
                    });
                }else{
                    $(elem).parent('.category-list').addClass('layui-hide');
                }
            }
        });
    }

    exports('categoryList', function(options){
        var TS = new categoryList(options = options || {});
        TS.init();
    });
});