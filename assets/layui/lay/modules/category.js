layui.define(['layer', 'laytpl', 'jquery', 'tools', 'form'], function(exports){
    var $ =  layui.jquery;
    var layer = layui.layer;
    var laytpl = layui.laytpl;
    var form = layui.form();
    var tools = layui.tools;

    var obj = {
        template : '',
        getData: function(){
            /*$.ajax({
                type: 'get',
                dataType: 'json',
                data: {},
                url: '/api/category/category_list/',
                success: function(json){
                    console.log(json);
                }
            });*/
            var dd =  [
                {
                    id: 1,
                    pid: 0,
                    index: 0,
                    name: '肉禽蛋类',
                    children: [
                        {
                            id: 2,
                            pid: 1,
                            index: 1,
                            name: '生态猪肉'
                        },
                        {
                            id: 3,
                            pid: 1,
                            index: 1,
                            name: '普通猪肉'
                        }
                    ]
                },
                {
                    id: 4,
                    pid: 0,
                    index: 0,
                    name: '酒水',
                    children: [{
                        id: 5,
                        pid: 4,
                        index: 1,
                        name: '酒',
                        children: [
                            {
                                id: 6,
                                pid: 5,
                                index: 2,
                                name: '红酒'
                            },{
                                id: 7,
                                pid: 5,
                                index: 2,
                                name: '白酒'
                            }
                        ]
                    },
                        {
                            id: 8,
                            name: '饮料',
                            pid: 4,
                            index: 1,
                            children: [
                                {
                                    id: 9,
                                    pid: 8,
                                    index: 2,
                                    name: '运动饮料',
                                },
                                {
                                    id: 10,
                                    pid: 8,
                                    index: 2,
                                    name: '碳酸饮料'
                                }
                            ]
                        }]
                }
            ];
            return dd;
        },
        list: function(){
            this.spread();
            this.initTemplate(this.getData());
            $('#tree').html(this.template);
            form.render('checkbox');
        },
        initTemplate: function(data){
            var _this = this;
            layui.each(data, function(k, v){
                _this.template += '';
                if(v.pid != 0) {
                    _this.template += '<tr data-pid="' + v.pid + '" data-id="'+ v.id +'" class="layui-hided">';
                }else{
                    _this.template += '<tr data-pid="' + v.pid + '" data-id="'+ v.id +'">';
                }
                _this.template += '<td>'+ v.pid +'</td>' +
                        '<td>';
                if(!tools.isEmpty(v.children)) {
                    _this.template += '<i class="layui-icon" style="margin-left:' + v.index * 20 + 'px;">&#xe623;</i>';
                }else{
                    _this.template += '<i class="layui-icon" style="margin-left:' + v.index * 20 + 'px;">&#xe621;</i>';
                }
                _this.template += '' +
                            '<a href="javascript:;;" class="spread" data-pid="'+ v.pid +'"><cite style="padding:0 6px;">'+ v.name +'</cite></a>' +
                        '</td>' +
                        '<td>' +
                            '<input type="checkbox" layui-skin="switch" name="index_show">' +
                        '</td>' +
                        '<td>' +
                            '<button class="layui-btn layui-btn-mini layui-btn-warm" type="button">修改</button>' +
                            '<button class="layui-btn layui-btn-mini layui-btn-danger" type="button">删除</button>' +
                        '</td>' +
                    '</tr>';
                if(!tools.isEmpty(v.children)){
                    _this.initTemplate(v.children);
                }
            });
        },
        spread: function(){
            var _this = this;
            $(document).on('click', '.spread', function () {
               var pid = $(this).attr('data-pid');
                console.log(pid);
               var spreadEle = $('tr[data-id="'+ pid +'"]');
               if(spreadEle.hasClass('layui-hide')){
                   spreadEle.removeClass('layui-hide');
               }else{
                   spreadEle.addClass('layui-hide');
               }
               // _this.eachSpread(pid);
            });
        },
        eachSpread: function(pid){
            var spreadEle = $('tr[data-id="'+ pid +'"]');
            //console.log(spreadEle.html());
            layui.each(spreadEle, function(){
                var childPid = spreadEle.find('a').attr('data-pid');
                if(spreadEle.hasClass('layui-hide')){
                    $('tr[data-pid="'+ childPid +'"]').removeClass('layui-hide');
                }else{
                    $('tr[data-pid="'+ childPid +'"]').addClass('layui-hide');
                }
            })
        }
    };

    var dd = function(){
        setTimeout(function(){
            obj.layer.msg('哈哈');
        },3000);
    }

    exports('category', obj);
});