layui.define('jquery',function(exports){
	var $ =  layui.jquery;
    var obj = {
        //对象是否为空
        isEmpty: function(o){
            if(o === undefined || o === '' || o === null ) {
                return true;
            }
            return false;
        },
        //获取url参数
        GetQueryString: function(name){
            var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
            var r = window.location.search.substr(1).match(reg);
            if(r!=null)return  unescape(decodeURI(r[2])); return null;
        }
        ,
       	encodeJSON: function(content){
			content = JSON.stringify(content);
			return content.replace(/"/g,'\\"');
		}
		,
		parseJSON: function(content){
			switch($.type(content))
			{
				case "object":
				case "array":
				{
					return content;
				}
				break;
		
				case "string":
				{
					if($.trim(content))
					{
						return $.parseJSON(content);
					}
				}
				break;
			}
			return null;
		},
		splitStr: function(str, symbol){
			return str.split(symbol);
		},
		removeLastStr: function(str){
			str = str.substring(0, str.length-1);
			return str;
		},
        removeArrayValue: function(arr, val){
            for(var i = 0; i < arr.length; i++) {
                if(arr[i] == val) {
                    arr.splice(i, 1);
                    break;
                }
            }
		}
	
    };
    exports('tools', obj);
});