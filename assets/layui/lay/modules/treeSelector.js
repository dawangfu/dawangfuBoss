/**
 * Created by fish on 16/10/27.
 */

layui.define(['layer', 'tree'], function(exports){
    var $ = layui.jquery;

    treeSelector = function(options){
        this.options = options;
    }

    treeSelector.prototype.init = function(){
        var _this = this;
        $.ajax({
            data: {type: _this.options.categoryType},
            dataType: 'json',
            type: 'get',
            url: '/api/category/category_list/',
            error: function(){
                layer.msg('系统错误');
            },
            success: function(json){
                layui.tree({
                    elem: _this.options.elem, //'#category-tree',
                    check: _this.options.type,
                    nodes: json.data.list,
                    focusId: _this.options.focusId,
                    only: _this.options.only ? _this.options.only :false ,
                    btnEvent: _this.options.btnEvent,
                    click: function(node){
                        if(_this.options.type == 'radio'){
                            $('input[name="categroy_id"]').val(node.id);
                            $('.add-category>span').html(node.category_name);
                            $('.category-list').addClass('layui-hide');
                            layer.msg(node.category_name);
                            if(typeof _this.options.callback === 'function'){
                                _this.options.callback(node.id);
                            }
                        }
                    }
                });
                _this.event();
            }
        });
    }
    treeSelector.prototype.event = function(){
        //展开分类
        $('.add-category').on('click', function(){
            if($(this).hasClass('layui-btn-disabled'))
                return false;
            $('.category-list').removeClass('layui-hide');
        });
    }
    /*var obj = {
        setTree: function(dom){
            var _this = this;
            $.ajax({
                dataType: 'json',
                type: 'get',
                url: '/api/category/category_list/',
                error: function(){
                    layer.msg('系统错误');
                },
                success: function(json){
                    layui.tree({
                        elem: dom, //'#category-tree',
                        check: 'radio',
                        nodes: json.data.list,
                        click: function(node){
                            $('input[name="categroy_id"]').val(node.id);
                            $('.add-category>span').html(node.category_name);
                            layer.msg(node.category_name);
                            $('.category-list').addClass('layui-hide');
                        }
                    });
                    _this.spreadNode();
                }
            });
        },
        spreadNode: function(){
            //展开分类
            $('.add-category').on('click', function(){
                $('.category-list').removeClass('layui-hide');
            });
        }
    }*/
    exports('treeSelector', function(options){
        var TS = new treeSelector(options = options || {});
        TS.init();
    });
});