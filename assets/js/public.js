$(function(){
    $(document).on("tap",".tap",function(e){
        e.preventDefault();
        e.stopPropagation();
        var url = $(this).attr("data-href") || $(this).attr("href");
        if (url != "") {
            document.location.href = url;
        }
    });
});
//显示加载动画
function showFishLoading(){
    var str = '<div id="loadingToast" class="weui_loading_toast"><div class="weui_mask_transparent"></div><div class="weui_toast"><div class="weui_loading"><div class="weui_loading_leaf weui_loading_leaf_0"></div><div class="weui_loading_leaf weui_loading_leaf_1"></div><div class="weui_loading_leaf weui_loading_leaf_2"></div><div class="weui_loading_leaf weui_loading_leaf_3"></div><div class="weui_loading_leaf weui_loading_leaf_4"></div><div class="weui_loading_leaf weui_loading_leaf_5"></div><div class="weui_loading_leaf weui_loading_leaf_6"></div><div class="weui_loading_leaf weui_loading_leaf_7"></div><div class="weui_loading_leaf weui_loading_leaf_8"></div><div class="weui_loading_leaf weui_loading_leaf_9"></div><div class="weui_loading_leaf weui_loading_leaf_10"></div><div class="weui_loading_leaf weui_loading_leaf_11"></div></div><p class="weui_toast_content">数据加载中</p></div></div>';
    $(str).appendTo("body");
}
//关闭加载动画
function hideFishLoading(){
    if($("#loadingToast")){
        $("#loadingToast").remove();
    }
}
//显示提示信息
intervalId=0;//定时任务ID
function showError(msg){
    self.clearInterval(intervalId);
    var divMsg = '<div id="message_info_div"><span class="alert alert-danger">'+ msg +'</span></div>';
    $('#message_info').html(divMsg.toString());
    $("input").blur();
    intervalId=self.setInterval("hideMessage()",2000);
}
//关闭提示信息
function hideMessage(){
    $('#message_info').html('');
}
//判断对象为空
function isEmpty(o){
    if(o === undefined || o === '' || o === null ) {
        return true;
    }
    return false;
}