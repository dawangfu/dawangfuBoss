$(document).ready(function(){
	$("#updateAreaInfo").click(function(){
		$.get('/area/update_area',{},function(data){
			if(data.errno == 1){
				layer.msg('更新成功!');
				window.location.reload();
			}
		},'json');
	});
	$("#setAreaManager").click(function(){
		var area_id = $("#currentId").val();
		var manager_id = $("input[name=manager]:checked").val();
		if(manager_id==undefined || manager_id=="undefined"){
			layer.msg('请选择分管经理');
			return false;
		}
		var data = {area_id:area_id,manager_id:manager_id};
		$.post('/area/set_area_manager',data,function(data){
			if(data.errno == 1){
				layer.msg(data.msg, {time:2000,icon: 1});
			}else{
				layer.msg(data.msg, {time:2000,icon: 2});
			}
		},'json');
	});
	$("#setManagerArea").click(function(){
			var $checkedArea = $("input[name=area]:checked:not(:disabled)");
			if($checkedArea.length <= 0){
				layer.msg('不分配区域吗？', {
				  time: 0 //不自动关闭
				  ,btn: ['分配', '不分配了']
				  ,yes: function(index){
				     layer.close(index);
				     return false;
				  }
				  ,btn2:function(index){
				  	setManangerArea($checkedArea);
				  }
				});				
			}else{
				setManangerArea($checkedArea);
			}
	});	
});
function setManangerArea($checkedArea){
	var $checkedAreaId =[];
	$.each($checkedArea , function(i,e){
		$checkedAreaId[i] = $(this).val();
	});
	var manager_id = $("#managerId").val();
	var data = {area_ids:$checkedAreaId , manager_id:manager_id};
	$.post('/area/set_manager_area',data,function(data){
		if(data.errno == 1){
			layer.msg(data.msg, {time:2000,icon: 1});
		}else{
			layer.msg(data.msg, {time:2000,icon: 2});
		}
	},'json');	
}
